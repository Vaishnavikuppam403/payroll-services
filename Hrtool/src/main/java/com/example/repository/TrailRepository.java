/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.repository;

import com.example.model.Trail;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Burgeonits
 */
@Repository
public interface TrailRepository extends MongoRepository<Trail,Integer>{

    public Object findByEmail(String email);

    public Trail findByUser(String user);
    
}
