/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.repository;

import com.example.model.Registrationmodel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Burgeonits
 */
@Repository
public interface RegistrationRepository extends MongoRepository<Registrationmodel,Integer>{

    public Object findByEmail(String email);
     public Registrationmodel findByUser(String user);

   // public Object findByNumber(String number);

  
    
}
