/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.login;

import com.example.db.TwoStepAuthDB;
import com.example.email.SentEmail;
import com.example.model.Registrationmodel;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
/**
 *
 * @author Burgeonits
 */
@Controller
public class LoginController {
    
    @Autowired
    public LoginService logservice;
    
 
     @RequestMapping(value="/login")
    public String login(){
        System.out.println("hiii1");
        return "login";
    }
    
     @RequestMapping(value="login1")
         public String login1(HttpServletRequest req,HttpServletResponse res,String username,String pass,Model model) throws IOException{
             HttpSession session = req.getSession();
        session.setAttribute("username", username);
        
        System.out.println("user name"+session.getAttribute("username"));
         
             System.out.println("1234555"+pass);
            
     Registrationmodel i1=logservice.login1(username, pass);
     System.out.println("flag1:"+i1.getFlag1());
            System.out.println("entered");
        
        
        if(i1!=null)
        {
            System.out.println("success page"+i1.getFirstname());
           
           /*if(i1.getFlag1().equals("first")){
               System.out.println("iffffffffff");
           model.addAttribute("firstname", i1.getFirstname());
           model.addAttribute("lastname", i1.getLastname());
           model.addAttribute("email", i1.getEmail());
           model.addAttribute("phonenumber",i1.getNumber());
           
           
            return "addemployee";
        }
           else{
               System.out.println("elseeeeeeeee");
               return "loginsucces";
           }*/
           
          
            // Using random method 
            Random otp = new Random(); 

            StringBuilder builder=new StringBuilder();

            for (int i = 0; i < 4; i++) 
            { 
               
                builder.append(otp.nextInt(4));
                 
            } 
            
            System.out.println("otp "+builder.toString());
           
           String sub = "OTP";
           String msg = builder.toString();
           String mail = i1.getEmail();
           
           SentEmail se = new SentEmail();
           se.sendActMail(sub, msg, mail);
           
           TwoStepAuthDB tdb = new TwoStepAuthDB();
           
           tdb.write(msg, mail);
           
           return "loginsucces";
        }
        else{
            return "login";
        }
      
    }
         
         
    @RequestMapping(value="/verif")
    public String verification(Model model, String otp){
        
        System.out.println("hiii1"+otp);
        
        TwoStepAuthDB tdb = new TwoStepAuthDB();
        String num = tdb.DBReadName(otp);
        
        if(num != ""){
            
            return "start";
        }
        
        else{
            
            model.addAttribute("msg", "Your OTP has expired..");
            return "login";
        }
        
    }
}