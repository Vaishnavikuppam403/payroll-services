/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.db;

import com.example.dao.MongoDAO;
import com.example.model.Tasks;
import com.example.payroll.Components;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Manasa.Janam
 */
public class ComponentsDB {
    
    MongoDAO dao = new MongoDAO();
    DB db = dao.mongoConnect();
    
    DBCollection collection = db.getCollection("components");
    
    public List<Components> DBRead() {

        BasicDBObject basicDBObject = new BasicDBObject();
        //basicDBObject.put("username", username);
        DBCursor cursor = collection.find(basicDBObject);
        List<Components> comp = new ArrayList<Components>();

        SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
        
       while (cursor.hasNext()) {
           
           Components t=new Components();
           DBObject obj = cursor.next();

           t.setName((String) obj.get("name"));
           
           t.setDate((Date) obj.get("date"));
           
           String format1 = myFormat.format(t.getDate());
           t.setDate1(format1);
           
           comp.add(t);
           
        }

        return comp;
    }
    
    public List<Components> DBReadName(String tname) {
        
        System.out.println("Read template name "+tname);
        
        List<Components> comp = new ArrayList<Components>();
        
        BasicDBObject basicDBObject = new BasicDBObject();
        basicDBObject.put("name", tname);
        DBCursor cursor = collection.find(basicDBObject);
        System.out.println("cursor "+cursor);
        
        while (cursor.hasNext()) {
            Components j = new Components();
            System.out.println("hello");
            DBObject obj = cursor.next();

            j.setName((String) obj.get("code"));
            j.setCategory((String) obj.get("category"));
            j.setBasevalue((String) obj.get("basevalue"));
            j.setRatetype((String) obj.get("ratetype"));
            j.setValue((String) obj.get("value"));

            System.out.println("ppppp "+j.getName());
             
            comp.add(j);
        }
        
        return comp;
    }
}
