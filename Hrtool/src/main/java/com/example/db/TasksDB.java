/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.db;

import com.example.dao.MongoDAO;
import com.example.model.Tasks;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Manasa.Janam
 */
public class TasksDB {
    
    MongoDAO dao = new MongoDAO();
    DB db = dao.mongoConnect();
    
    DBCollection collection = db.getCollection("CreatedTasks");
    
    public void write(Tasks t){

        Date d = new Date();
        
        DBObject basicDBObject = new BasicDBObject();

        basicDBObject.put("uname", t.getUname());
        basicDBObject.put("tname", t.getTname());
        basicDBObject.put("date", d);

        collection.insert(basicDBObject);
    }
    
    public List<Tasks> DBRead() {

        BasicDBObject basicDBObject = new BasicDBObject();
        //basicDBObject.put("username", username);
        DBCursor cursor = collection.find(basicDBObject);
        List<Tasks> b = new ArrayList<Tasks>();

       while (cursor.hasNext()) {
           
           Tasks t=new Tasks();
           DBObject obj = cursor.next();

           t.setUname((String) obj.get("uname"));
           t.setTname((String) obj.get("tname"));
           t.setDate((Date) obj.get("date"));
           b.add(t);
           
        }

        return b;
    }
}
