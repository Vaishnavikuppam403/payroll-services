/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.db;

import com.example.dao.MongoDAO;
import com.example.model.Tasks;
import com.example.payroll.Components;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Manasa.Janam
 */
public class TwoStepAuthDB {
    
    MongoDAO dao = new MongoDAO();
    DB db = dao.mongoConnect();
    
    DBCollection collection = db.getCollection("log_events");
    
    public void write(String otp, String mail){

        Date d = new Date();
        
        DBObject basicDBObject = new BasicDBObject();

        basicDBObject.put("createdAt", d);
        basicDBObject.put("otp", otp);
        basicDBObject.put("email", mail);
        
        collection.insert(basicDBObject);
    }
    
    
    public String DBReadName(String otp) {
        
        System.out.println("Read template name "+otp);
        
        BasicDBObject basicDBObject = new BasicDBObject();
        basicDBObject.put("otp", otp);
        DBCursor cursor = collection.find(basicDBObject);
        System.out.println("cursor "+cursor);
       
            DBObject obj = cursor.next();

            String otp1 = (String) obj.get("otp");
            
            System.out.println("ppppp "+otp1);
             
        return otp1;
    }
}
