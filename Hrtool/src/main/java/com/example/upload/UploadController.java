/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.upload;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Iterator;
import java.util.List;
import javax.websocket.Encoder;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.bson.types.Binary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Burgeonits
 */
@Controller
public class UploadController {
      @Autowired
      public UploadService upservice;
      
       @Value("${upload.path}")
    private String fileLocation;
       
          @RequestMapping(value = "/loginsucces")
        public String uploadfile(Model model,MultipartFile fileupload) throws IOException{
         System.out.println("entered file"+fileupload);
         List<Upload> tempStudentList = new ArrayList<Upload>();
           XSSFWorkbook wb = null;
          FileInputStream fileIn = null;
           FileOutputStream fileOut = null;
           
    InputStream in = fileupload.getInputStream();
    File currDir = new File(".");
    String path = currDir.getAbsolutePath();
    fileLocation = path.substring(0, path.length() - 1) + fileupload.getOriginalFilename();
    System.out.println(fileLocation);
    FileOutputStream f = new FileOutputStream(fileLocation);
   
    int ch = 0;
    while ((ch = in.read()) != -1) {
        f.write(ch);
    }
    f.flush();
    f.close();
    model.addAttribute("message", "File: " + fileupload.getOriginalFilename() 
      + " has been uploaded successfully!");
       
    
        fileIn=new FileInputStream(fileLocation);
        wb = new XSSFWorkbook(fileIn);
             Sheet datatypeSheet = wb.getSheetAt(0);
           
       
              for(int i=1;i<datatypeSheet.getPhysicalNumberOfRows() ;i++) {
            Upload tempStudent = new Upload();
         XSSFRow row = (XSSFRow) datatypeSheet.getRow(i);
          System.out.println(row.getCell(0).getStringCellValue());
          
         tempStudent.setId(row.getCell(0).getStringCellValue());
         tempStudent.setName1(row.getCell(1).getStringCellValue());
         tempStudent.setAddress(row.getCell(2).getStringCellValue());
         tempStudent.setDesignation(row.getCell(3).getStringCellValue());
         tempStudent.setCompany(row.getCell(4).getStringCellValue());
          tempStudentList.add(tempStudent);
         //
          
                  System.out.println(tempStudentList.size());
                  model.addAttribute("tempStudentList",tempStudentList);
        
//           if(str1==1){
//             System.out.println("Saved successfully");
//                  return "success";
//            }else{
//                     
//              }

//}
               
        }
              Integer str1=upservice.uploadfile(tempStudentList);
              if(str1==1){
                    System.out.println("Saved successfully");
                return "success12";

              }
     return "loginsucces";         
}
        @RequestMapping(value="/imageupload")
    public String image(){
        return "image";
}

        
    @RequestMapping(value = "/imageupload1")
    public String imageupload(Model model,MultipartFile fileupload1) throws IOException{
         byte[] b=fileupload1.getBytes();
         String encodeToString = Base64.getEncoder().encodeToString(b);
         System.out.println("data"+encodeToString);
          Binary data=new Binary(b);
         System.out.println(b);
         Upload up1=new Upload();
      up1.setFileupload(data);
        System.out.println("upload"+up1.getFileupload());
      
          File currDir = new File(".");
         String path = currDir.getAbsolutePath();
    fileLocation = path.substring(0, path.length() - 1) + fileupload1.getOriginalFilename();
    System.out.println(fileLocation);
    
         Integer str1=upservice.imageupload(up1);
         if(str1==1){
             model.addAttribute("image",encodeToString);
             return "imagedisplay";
         }
         else{
           return "image";
         }
        
}
}
        


            
    



