/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.upload;

import org.bson.types.Binary;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.annotation.Id;

/**
 *
 * @author Burgeonits
 */
@Document(collection = "User1")
public class Upload {
    @Id
//    private Double Id;
//
//    public Double getId() {
//        return Id;
//    }
//
//    public void setId(Double Id) {
//        this.Id = Id;
//    }
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

   
    private String name1;
    private String address;
    private String designation;
    private String company;

  
    private Binary fileupload;

    public Binary getFileupload() {
        return fileupload;
    }

    public void setFileupload(Binary fileupload) {
        this.fileupload = fileupload;
    }

    

   
  
   
    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }
   
}
