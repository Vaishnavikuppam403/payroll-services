/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.Addemployee;

import com.example.repository.AddemployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Burgeonits
 */
@Service()
public class AddemployeeImpl implements AddemployeeService{
     @Autowired
    public AddemployeeRepository addrep;

    @Override
    public Integer addemployee(Addemployee add) {
        System.out.println("saving");
       addrep.save(add);
       return 1;
    }
}
