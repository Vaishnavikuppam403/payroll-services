/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.Addemployee;

import com.example.model.Registrationmodel;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
/**
 *
 * @author Burgeonits
 */
@Controller
public class AddemployeeController {
    @Autowired 
    public AddemployeeService addservice;
    
    @RequestMapping(value="basic")
    public String basicemployee(){
     
        System.out.println("entered basic details");
       
        return "addemployee";
        
    }
    @RequestMapping(value="basic1")
    public String addemployee(Model model,Addemployee add){
        System.out.println("submitted values");
        Integer str1=addservice.addemployee(add);
        if(str1==1){
            System.out.println("saved successfully");
            return "success";
        }
        
        return "addemployee";
    }
}
        
    
