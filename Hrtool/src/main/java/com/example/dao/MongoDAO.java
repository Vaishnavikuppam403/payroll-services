/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.dao;

import com.mongodb.DB;
import com.mongodb.MongoClient;

/**
 *
 * @author Manasa.Janam
 */
public class MongoDAO {
    
    public DB mongoConnect(){
        
        MongoClient mongoClient = new MongoClient("127.0.0.1", 27017);
    
        DB database = mongoClient.getDB("Hrtool");
        
        /*Set<String> collectionNames = database.getCollectionNames();
        
        for(String s : collectionNames){
            
            System.out.println("Collections.."+s);
        }
        */
        
        return database;
    }
}
