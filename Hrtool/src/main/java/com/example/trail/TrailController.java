/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.trail;

import com.example.model.Registrationmodel;
import com.example.model.Sendemail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.example.model.Trail;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.ui.Model;
/**
 *
 * @author Burgeonits
 */
@Controller
public class TrailController {
 @Autowired
    public TrailService trservice;
   
    @RequestMapping(value="/")
    public String home(){
        System.out.println("entered home page");
        return "home";
    }
    @RequestMapping(value="/registertry")
    public String registertry()
    {
        System.out.println("Entered to freetail page");
        return "registertry";
    }
    @RequestMapping(value="/pricing")
    public String pricing(){
        System.out.println("Entered pricing page");
        return "pricing";
    }
    @RequestMapping(value="/registerbuy")
    public String registerbuy(){
        System.out.println("entered registerd buy");
        return "registerbuy";
        
    }
    
     @RequestMapping(value="/trylogin")
    public String login1(){
        System.out.println("hiii1");
        return "login1";
    }
    @RequestMapping(value="/tryregister1")
    public String register1(Trail tr,Model model,HttpServletRequest req,HttpServletResponse res){
        System.out.println("entered");
         HttpSession session = req.getSession();
           String str=tr.getEmail();
            String[] word;
             String delimiter="@";
             word=str.split(delimiter);
             System.out.println(word[0]);
             String temp=word[1].toString();
           System.out.println(temp);
           String first=(tr.getFname()).substring(0,3);
           String last=(tr.getLname()).substring(0,4);
            int randompin1= (int)(Math.random()*900)+100;
     
        StringBuilder userid=new StringBuilder(first);
         userid.append(last);
             userid.append(randompin1);
              System.out.println("userid:"+userid);
             String username=userid.toString();
             int randompin   =(int)(Math.random()*9000)+100000;
            String otp  = String.valueOf(randompin);
            tr.setUser(username);
             session.setAttribute("user", tr.getUser());
             tr.setFlag1("A");
         Integer str1=trservice.register1(tr);
               if(str1==1){
                 tr.setFlag("try");
                 tr.setFlag2("first");
               String link="http://localhost:8080/resetpass1?user="+(tr.getUser());
             String html="Details of your login credintials of Adriot Consultancy Services \n username:"+tr.getUser() +"\n activation link: <a href='"+link+"'>Activationlink</a>";
            Sendemail email1=new Sendemail();
            email1.sendactmail(html,tr.getEmail());
            model.addAttribute("Firstname",tr.getFname());
            model.addAttribute("Temp",temp);
           
              return "registersuccess";
               }
               else{
                   return "existregister";
               }
    }
    @RequestMapping(value="/buyregister")
    public String register2(Trail tr,Model model,HttpServletRequest req,HttpServletResponse res){
         System.out.println("entered");
         HttpSession session = req.getSession();
           String str=tr.getEmail();
            String[] word;
             String delimiter="@";
             word=str.split(delimiter);
             System.out.println(word[0]);
             String temp=word[1].toString();
           System.out.println(temp);
           String first=(tr.getFname()).substring(0,3);
           String last=(tr.getLname()).substring(0,4);
            int randompin1= (int)(Math.random()*900)+100;
     
        StringBuilder userid=new StringBuilder(first);
         userid.append(last);
             userid.append(randompin1);
              System.out.println("userid:"+userid);
             String username=userid.toString();
             int randompin   =(int)(Math.random()*9000)+100000;
            String otp  = String.valueOf(randompin);
            tr.setUser(username);
             session.setAttribute("user", tr.getUser());
             tr.setFlag1("A");
             Integer str2=trservice.register2(tr);
             if(str2==1){
                 tr.setFlag("buy");
                  String link="http://localhost:8080/resetpass?user="+(tr.getUser());
             String html="Details of your login credintials of Adriot Consultancy Services \n username:"+tr.getUser() +"\n activation link: <a href='"+link+"'>Activationlink</a>";
            Sendemail email1=new Sendemail();
            email1.sendactmail(html,tr.getEmail());
            model.addAttribute("Firstname",tr.getFname());
            model.addAttribute("Temp",temp);
           
              return "registersuccess";
               }
             else{
                  return "existregister";
             }
             
    }
     @RequestMapping(value="logintry")
         public String login1(HttpServletRequest req,HttpServletResponse res,String username,String pass,Model model) throws ParseException{
             System.out.println("entered login1 try");
             HttpSession session = req.getSession();
        session.setAttribute("username", username);
        
        System.out.println("user name"+session.getAttribute("username"));
         
             System.out.println("1234555"+pass);
            System.out.println("usernamezcgfsdhf"+username);
    Trail i1=trservice.login12(username, pass);
     System.out.println("flag1:"+i1.getFlag1());
            System.out.println("entered");
        
        
        if(i1!=null)
        {
            System.out.println("success page"+i1.getFname());
            System.out.println("Date "+i1.getDate());
            SimpleDateFormat myFormat = new SimpleDateFormat("dd MM yyyy");
            
            String format = myFormat.format(i1.getDate());
            
            Date d = new Date();
            String format1 = myFormat.format(d);
            
            Date dateBefore = myFormat.parse(format);
            Date dateAfter = myFormat.parse(format1);
            
            long difference = dateAfter.getTime() - dateBefore.getTime();
            float daysBetween = (difference / (1000*60*60*24));
            
            int days = (int) daysBetween;
            
            int tday = 15;
            int rday = tday - days;
            
            System.out.println("Number of Days between dates: "+(tday - daysBetween));
            
            if(days != 15)
                
            
            
            model.addAttribute("msg", rday);
            
               return "trailsuccess";
           
        }
        else{
            return "login";
        }
    }  

}

