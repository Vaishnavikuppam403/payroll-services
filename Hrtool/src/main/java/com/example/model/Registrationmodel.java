/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.model;

import java.io.File;
import java.io.InputStream;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "User")
public class Registrationmodel {




private String flag1;

   public String getFlag1() {
       return flag1;
   }

   public void setFlag1(String flag1) {
       this.flag1 = flag1;
   }
   private String name;
   private String role;

   public String getRole() {
       return role;
   }



   public void setRole(String role) {
       this.role = role;
   }

   public String getName() {
       return name;
   }

   public void setName(String name) {
       this.name = name;
   }
   private File file;

   public File getFile() {
       return file;
   }

   public void setFile(File file) {
       this.file = file;
   }
   private String firstname;
   private String lastname;
   private String email;
   private String number;

   @Id
   private String user;
   private String flag;
   private String pass;
   private String confirmpass;

   public String getPass() {
       return pass;
   }

   public void setPass(String pass) {
       this.pass = pass;
   }

   public String getConfirmpass() {
       return confirmpass;
   }

   public void setConfirmpass(String confirmpass) {
       this.confirmpass = confirmpass;
   }

   public String getUser() {
       return user;
   }

   public void setUser(String user) {
       this.user = user;
   }

   public String getFlag() {
       return flag;
   }

   public void setFlag(String flag) {
       this.flag = flag;
   }

   public String getFirstname() {
       return firstname;
   }

   public void setFirstname(String firstname) {
       this.firstname = firstname;
   }

   public String getLastname() {
       return lastname;
   }

   public void setLastname(String lastname) {
       this.lastname = lastname;
   }

   public String getEmail() {
       return email;
   }

   public void setEmail(String email) {
       this.email = email;
   }

   public String getNumber() {
       return number;
   }

   public void setNumber(String number) {
       this.number = number;
   }
}