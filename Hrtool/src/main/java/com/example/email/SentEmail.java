/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.email;

import java.io.IOException;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author Manasa.Janam
 */
public class SentEmail {
    
    public void sendActMail(String sub, String msg, String mail) throws IOException {    
        
        String host="mail.adriott.com";  
        final String user="test1@adriott.com";//change accordingly  
        final String password="adriot@123";//change accordingly  

        String to=mail;//change accordingly  
       
         //Get the session object  
         Properties props = new Properties();  
         props.put("mail.smtp.host",host);  
         props.put("mail.smtp.auth", "true");  

         Session session = Session.getInstance(props,  
          new javax.mail.Authenticator() {  
              
                protected PasswordAuthentication getPasswordAuthentication() {  
                return new PasswordAuthentication(user,password);  
            }  
          });  

         //Compose the message  
          try {  
              
                MimeMessage message = new MimeMessage(session);  
                
                message.setFrom(new InternetAddress(user));  
                message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));  
                message.setSubject(sub);  
                //message.setText(msg);  
                message.setContent(msg, "text/html");
               //send the message  
                Transport.send(message);  

                System.out.println("message sent successfully...");  

           } 
          catch (MessagingException e) {
              e.printStackTrace();
           }  
   
   }
}
