/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.payroll;

import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author "Naveen Chindam"
 */


public class SalComponents {
    
    private String[] label;
    
    private String[] category;
    
    private String[] percentage;
    
    private double[] value;
    
    private double[] basevalue;
    
    

    public double[] getValue() {
        return value;
    }

    public void setValue(double[] value) {
        this.value = value;
    }

    public double[] getBasevalue() {
        return basevalue;
    }

    public void setBasevalue(double[] basevalue) {
        this.basevalue = basevalue;
    }

  
    
    public String[] getLabel() {
        return label;
    }

    public void setLabel(String[] label) {
        this.label = label;
    }

    public String[] getCategory() {
        return category;
    }

    public void setCategory(String[] category) {
        this.category = category;
    }

    public String[] getPercentage() {
        return percentage;
    }

    public void setPercentage(String[] percentage) {
        this.percentage = percentage;
    }

   
}
