/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.payroll;


import com.example.db.ComponentsDB;
import com.example.repository.SalCompRepository;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author "Naveen Chindam"
 */

@Controller
public class SalComponentController {
    
    @Autowired
    SalCompRepository rep;
    
    
     @RequestMapping(value="comp1")
    public String com1(){
        
       
        return "payitems";
     
    }
    
    @RequestMapping(value = "/payroll")
    public String payRoll(Model model){
        
        System.out.println("Create Tasks");
        
         ComponentsDB cdb = new ComponentsDB();
        List<Components> c = cdb.DBRead();
        System.out.println("ddd "+c.size());
        model.addAttribute("c",c);
        
        return "payroll";
    }
    
    
    
    @RequestMapping(value="comp")
    public String com(Model model, HttpServletRequest req,HttpServletResponse res){
        
       // req.getParameterValues("label");
        System.out.println("ssssssssss");
        
        String name = req.getParameter("name");
        System.out.println("name"+name);
        String[] arr=req.getParameterValues("label");
        System.out.println("label"+req.getParameterValues("label"));
        String[] arr1=req.getParameterValues("category");
        System.out.println("category"+req.getParameterValues("category"));
        String[] arr2=req.getParameterValues("ratetype");
        System.out.println("ratetype"+req.getParameterValues("ratetype"));
        String[] arr3=req.getParameterValues("value");
        System.out.println("value"+req.getParameterValues("value"));
        String[] arr4=req.getParameterValues("basevalue");
        System.out.println("basevalue"+req.getParameterValues("basevalue"));
        
        List<Components> component=new ArrayList<Components>();
        
        Date d = new Date();
        
       int numberOfItems = arr.length;
        for (int i=0; i<numberOfItems; i++)
        {
            Components comp=new Components();
            
           comp.setLabel(arr[i]);
           comp.setCategory(arr1[i]);
           comp.setRatetype(arr2[i]);
           comp.setValue(arr3[i]);
           comp.setBasevalue(arr4[i]);
           comp.setName(name);
           comp.setDate(d);
            System.out.println("Hello " + arr[i]);
            
            component.add(comp);
        }
         
          for(Components com : component){
              
             System.out.println("Label "+com.getLabel());
             
              System.out.println("category "+com.getCategory());
              
               System.out.println("percentage "+com.getRatetype());
               
               System.out.println("value "+com.getValue());
               
               System.out.println("base "+com.getBasevalue());
               
             rep.save(com);
          }
       
         
        return "index";
     
    }
    
    @RequestMapping(value="salcalc")
    public String earnings(){
        
        List<Components> lst=rep.findAll();
        
        double sum = 0;
        
        double ctc=100000;
        
        double basic=(50/100)*ctc;
        
        for(Components com:lst){
            
            Map<String,String> hmap=new HashMap<String, String>();
            
            double value=Double.valueOf(com.getValue());
            
            double name = (value/100)*basic;
            
            String str=String.valueOf(value);
            
            hmap.put(com.getLabel(), str);
            
           for(String str1:hmap.keySet()){
               
               System.out.println("key value"+str1);
           }
           
             for(String str1:hmap.values()){
               
               System.out.println("value"+str1);
               
               sum+=Double.valueOf(str1);
               
               System.out.println("Sum"+sum);
           }
             
             double specialallowance=ctc-sum;
          
        }
     
        return "index";
        
    }
    
    @RequestMapping(value="general")
    public String salcal(){
        
        double ctc=55000;
        
        double basic=0;
        
         basic=ctc*50/100;
         
         double lta=0;
        
         lta=basic/10;
         
         double pf=0;
        
         pf=basic*(0.12);
         
         double esi=0;
        
         esi=basic*(4.75/100);
        
        double medicalallowance=1250;
        
        double conveyance=1600;
        
        double earn1=basic+lta+pf+esi+medicalallowance+conveyance;
        
        double specialallowance=ctc-earn1;
        
        double gross=earn1+specialallowance;
        
        System.out.println("basic  "+basic+"\n"+"lta  "+lta+"\n"+"pf  "+pf+"\n"+"esi  "+esi+"\n"+"medicalallowance  "+medicalallowance+"\n"+"conveyance  "+conveyance+"\n"+"specialallowance  "+specialallowance+"\n"+"gross  "+gross+"\n");
        
        double epf=basic*(13.25/100);
        
        double esic=gross*(1.75/100);
        
        double pt;
        
        if(basic>=15000){
            pt=200;
            
        }else{
            
            pt=150;
        }
        
        double deductions=epf+esic+pt;
        
        double takehome=gross-deductions;
        
        System.out.println("epf "+epf+"\n"+"esic "+esic+"\n"+"pt "+pt+"\n"+"dedeuctions "+deductions+"\n"+"takehome "+takehome);
        
        return "index";
        
        
    }
  
}
