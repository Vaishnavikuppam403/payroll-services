/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.tasks;

import com.example.db.TasksDB;
import com.example.model.Tasks;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author Manasa.Janam
 */

@Controller
public class TasksController {
    
    @Autowired
    TasksService tasksService;
    
    @RequestMapping(value = "/createtask")
    public String createTasks(Model model){
        
        System.out.println("Create Tasks");
        
        return "createtask";
    }
    
    @RequestMapping(value = "/createtask1")
    public String createTasks1(Model model, Tasks t){
        
        System.out.println("Created"+t.getTname());
        
        tasksService.createTask(t);
        
        model.addAttribute("msg", "Task Created Successfully");
        
        return "createtask";
    }
    
    @RequestMapping(value = "/listtask")
    public String listTasks(Model model){
        
        System.out.println("List Tasks");
        
        TasksDB tdb = new TasksDB();
        List<Tasks> l = tdb.DBRead();
        int size = l.size();
        System.out.println(size);
            
        for(Tasks t : l){
            
            System.out.println("name "+t.getTname());
        }
        
        model.addAttribute("list", l);
        
        return "listtask";
    }
}
