/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.tasks;

import com.example.db.TasksDB;
import com.example.model.Tasks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Manasa.Janam
 */

@Service()
public class TasksServiceimpl implements TasksService{

    
    @Override
    public void createTask(Tasks t) {
        
        TasksDB tdb = new TasksDB();
        
        tdb.write(t);
    }
    
}
