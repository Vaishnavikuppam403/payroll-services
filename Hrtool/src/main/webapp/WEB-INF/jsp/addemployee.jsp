<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

<title>HR</title>
<!-- Favicon-->
<link  rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
<!-- Morris Chart Css-->
<link rel="stylesheet" href="plugins/morrisjs/morris.css" />
<!-- Colorpicker Css -->
<link rel="stylesheet" href="plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css" />
<!-- Multi Select Css -->
<link rel="stylesheet" href="plugins/multi-select/css/multi-select.css">
<!-- Bootstrap Spinner Css -->
<link rel="stylesheet" href="plugins/jquery-spinner/css/bootstrap-spinner.css">
<!-- Bootstrap Tagsinput Css -->
<link rel="stylesheet" href="plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<!-- Bootstrap Select Css -->
<link rel="stylesheet" href="plugins/bootstrap-select/css/bootstrap-select.css" />
<!-- noUISlider Css -->
<link rel="stylesheet" href="plugins/nouislider/nouislider.min.css" />
<!-- Custom Css -->
<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="css/color_skins.css">
</head>
<style>
.stepwizard-step p {
    margin-top: 0px;
    color:#666;
}
.stepwizard-row {
    display: table-row;
}
.stepwizard {
    display: table;
    width: 100%;
    position: relative;
}
.stepwizard-step button[disabled] {
    /*opacity: 1 !important;
    filter: alpha(opacity=100) !important;*/
}
.stepwizard .btn.disabled, .stepwizard .btn[disabled], .stepwizard fieldset[disabled] .btn {
    opacity:1 !important;
    color:#bbb;
}
.stepwizard-row:before {
    top: 20px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 80%;
    height: 2px;
    background-color: #21282c;
    z-index: 0;
    left: 120px;
}
.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}
.btn-circle {
    width: 30px;
    height: 30px;
    text-align: center;
    padding: 6px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 15px;
}
.copy-fields {
    display: none;
}
.remove{
    margin: 0!important;
}
.cta {
    position: relative;
    margin: auto;
    padding: 19px 22px;
    transition: all 0.2s ease;
}
.cta:before {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    display: block;
    border-radius: 28px;
    background: rgba(255, 171, 157, 0.5);
    width: 56px;
    height: 56px;
    transition: all 0.3s ease;
}
.cta span {
    position: relative;
    font-size: 16px;
    line-height: 18px;
    font-weight: 900;
    letter-spacing: 0.25em;
    text-transform: uppercase;
    vertical-align: middle;
}
.cta svg {
    position: relative;
    top: 0;
    margin-left: 10px;
    fill: none;
    stroke-linecap: round;
    stroke-linejoin: round;
    stroke: #111;
    stroke-width: 2;
    transform: translateX(-5px);
    transition: all 0.3s ease;
}
.cta span {
    position: relative;
    font-size: 16px;
    line-height: 18px;
    font-weight: 900;
    letter-spacing: 0.25em;
    text-transform: uppercase;
    vertical-align: middle;
}
.cta:hover:before {
    width: 100%;
    background: #ffab9d;
}

</style>
<body class="theme-black">
<!-- Page Loader -->
<!-- <div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img src="assets/images/logo.svg" width="48" height="48" alt="Alpino"></div>
        <p>Please wait...</p>        
    </div>
</div> -->
<div class="overlay_menu">
    <button class="btn btn-primary btn-icon btn-icon-mini btn-round"><i class="zmdi zmdi-close"></i></button>
    <div class="container">        
        <div class="row clearfix">
            <div class="card">
                <div class="body">
                    <div class="input-group mb-0">
                        <input type="text" class="form-control" placeholder="Search...">
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="zmdi zmdi-search"></i></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card links">
                <div class="body">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-sm-12">
                            <h6>App</h6>
                            <ul class="list-unstyled">
                                <li><a href="">Inbox</a></li>
                                <li><a href="">Chat</a></li>
                                <li><a href="">Calendar</a></li>
                                <li><a href="">File Manager</a></li>
                                <li><a href="contact.html">Contact list</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12">
                            <h6>User Interface (UI)</h6>
                            <ul class="list-unstyled">
                                <li><a href="">UI KIT</a></li>
                                <li><a href="">Tabs</a></li>
                                <li><a href="">Range Sliders</a></li>
                                <li><a href="">Modals</a></li>
                                <li><a href="">Alerts</a></li>
                                <li><a href="">Dialogs</a></li>
                                <li><a href="">Collapse</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12">
                            <h6>Sample Pages</h6>
                            <ul class="list-unstyled">                            
                                <li><a href="">Image Gallery</a></li>
                                <li><a href="">Profile</a></li>
                                <li><a href="">Timeline</a></li>
                                <li><a href="">Pricing</a></li>
                                <li><a href="">Invoices</a></li>
                                <li><a href="">FAQs</a></li>
                                <li><a href="">Search Results</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12">
                            <h6>About</h6>
                            <ul class="list-unstyled">
                                <li><a href="" target="_blank">About</a></li>
                                <li><a href="" target="_blank">Contact Us</a></li>
                                <li><a href="" target="_blank">Admin Templates</a></li>
                                <li><a href="" target="_blank">Services</a></li>
                                <li><a href="" target="_blank">Portfolio</a></li>                            
                            </ul>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12">
        
            </div>
        </div>
    </div>
</div>
<div class="overlay"></div><!-- Overlay For Sidebars -->

<!-- Left Sidebar -->
<aside id="minileftbar" class="minileftbar">
    <ul class="menu_list">
        <li>
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" href="index.html"><img src="images/logo.svg" alt="Alpino"></a>
        </li>
        <li><a href="javascript:void(0);" class="btn_overlay hidden-sm-down"><i class="zmdi zmdi-search"></i></a></li>        
       <!--  <li><a href="javascript:void(0);" class="menu-sm"><i class="zmdi zmdi-swap"></i></a></li>      -->   
        <li class="menuapp-btn"><a href=""><i class="zmdi zmdi-apps"></i></a></li>
        <li class="notifications badgebit">
            <a href="javascript:void(0);">
                <i class="zmdi zmdi-notifications"></i>
                <div class="notify">
                    <span class="heartbit"></span>
                    <span class="point"></span>
                </div>
            </a>
        </li>
        <li class="task badgebit">
            <a href="javascript:void(0);">
                <i class="zmdi zmdi-account-box-phone"></i>
                <div class="notify">
                    <span class="heartbit"></span>
                    <span class="point"></span>
                </div>
            </a>
        </li>
        <li><a href="events.html" title="Events"><i class="zmdi zmdi-calendar"></i></a></li>
        <!-- <li><a href="mail-inbox.html" title="Inbox"><i class="zmdi zmdi-email"></i></a></li> -->
        <li><a href="" title="Contact List"><i class="zmdi zmdi-account-box-phone"></i></a></li>        
        <li><a href=""><i class="zmdi zmdi-comments"></i></a></li>        
        <li><a href="javascript:void(0);" class="fullscreen" data-provide="fullscreen"><i class="zmdi zmdi-fullscreen"></i></a></li>
        <li class="power">
            <a href="javascript:void(0);" class="js-right-sidebar"><i class="zmdi zmdi-settings zmdi-hc-spin"></i></a>            
            <a href="sign-in.html" class="mega-menu"><i class="zmdi zmdi-power"></i></a>
        </li>
    </ul>    
</aside>

<aside class="right_menu">
    <div class="menu-app">
        <div class="slim_scroll">
            <div class="card">
                <div class="header">
                    <h2><strong>App</strong> Menu</h2>
                </div>
                <div class="body">
                    <ul class="list-unstyled menu">
                        <li><a href=""><i class="zmdi zmdi-calendar-note"></i><span>Calendar</span></a></li>
                        <li><a href=""><i class="zmdi zmdi-file-text"></i><span>File Manager</span></a></li>
                        <li><a href=""><i class="zmdi zmdi-blogger"></i><span>Blog</span></a></li>
                        <li><a href="javascript:void(0)"><i class="zmdi zmdi-arrows"></i><span>Notes</span></a></li>
                        <li><a href="javascript:void(0)"><i class="zmdi zmdi-view-column"></i><span>Taskboard</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="notif-menu">
        <div class="slim_scroll">
            <div class="card">
                <div class="header">
                    <h2><strong>Messages</strong></h2>
                </div>
                <div class="body">
                    <ul class="messages list-unstyled">
                        <li>
                            <a href="javascript:void(0);">
                                <div class="media">
                                    <img class="media-object rounded-circle" src="images/xs/avatar1.jpg" alt="">
                                    <div class="media-body">
                                        <span class="name">Alexander <small class="time">35min ago</small></span>
                                        <p class="message">New tasks needs to be done</p>                                        
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <div class="media">
                                    <img class="media-object rounded-circle" src="images/xs/avatar2.jpg" alt="">
                                    <div class="media-body">
                                        <span class="name">Grayson <small class="time">1hr ago</small></span>
                                        <p class="message">New tasks needs to be done</p>                                        
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <div class="media">
                                    <img class="media-object rounded-circle" src="images/xs/avatar3.jpg" alt="">
                                    <div class="media-body">
                                        <span class="name">Sophia <small class="time">31min ago</small></span>
                                        <p class="message">New tasks needs to be done</p>                                        
                                    </div>
                                </div>
                            </a>
                        </li>        
                    </ul>  
                </div>
            </div>
            <div class="card">
                <div class="header">
                    <h2><strong>Notifications</strong></h2>
                </div>
                <div class="body">
                    <ul class="notification list-unstyled">
                        <li>
                            <i class="zmdi zmdi-balance-wallet text-success"></i>
                            <strong>+$30 New sale</strong>
                            <p><a href="javascript:void(0)">Admin Template</a></p>
                            <small class="text-muted">7 min ago</small>
                        </li>
                        <li>
                            <i class="zmdi zmdi-edit text-info"></i>
                            <strong>You Edited file</strong>
                            <p><a href="javascript:void(0)">Docs.doc</a></p>
                            <small class="text-muted">15 min ago</small>
                        </li>
                        <li>
                            <i class="zmdi zmdi-delete text-danger"></i>
                            <strong>Project removed</strong>
                            <p><a href="javascript:void(0)">AdminX BS4</a></p>
                            <small class="text-muted">1 hours ago</small>
                        </li>
                        <li>
                            <i class="zmdi zmdi-account text-success"></i>
                            <strong>New user</strong>
                            <p><a href="javascript:void(0)">UI Designer</a></p>
                            <small class="text-muted">1 hours ago</small>
                        </li>
                        <li>
                            <i class="zmdi zmdi-flag text-warning"></i>
                            <strong>Alpino v1.0.0 is available</strong>
                            <p><a href="javascript:void(0)">Update now</a></p>
                            <small class="text-muted">5 hours ago</small>
                        </li>
                    </ul>  
                </div>
            </div>
        </div>
    </div>
    <div class="task-menu">


               <div class="slim_scroll">
     <div class="card">
        <h2><strong>Employees</strong></h2>
                <div class="header">
                    <h2><strong>Main</strong></h2>
                </div>
                <div class="body">
                    <ul class="notification list-unstyled">
                        <li>
                             <i class="zmdi zmdi-balance-wallet text-success"></i>                           
                            <p><a href="">Overwiew</a></p>
                            
                        </li>

                        <li>
                            <i class="zmdi zmdi-account text-success"></i>                           
                            <p><a href="">Employee Directory</a></p>
                            
                        </li>

                    </ul>  
                </div>
            </div>
            <div class="card">
                <div class="header">
                    <h2><strong>Information</strong></h2>
                </div>
                <div class="body">
                    <ul class="notification list-unstyled">
                           <li>
                             <i class="zmdi zmdi-balance-wallet text-success"></i>                           
                            <p><a href="">Employee Profile</a></p>
                            
                        </li>
                           <li>
                             <i class="zmdi zmdi-balance-wallet text-success"></i>                           
                            <p><a href="">Bank/PF/ESI</a></p>
                            
                        </li>
                           <li>
                             <i class="zmdi zmdi-balance-wallet text-success"></i>                           
                            <p><a href="">Position History</a></p>
                            
                        </li>
                           <li>
                             <i class="zmdi zmdi-balance-wallet text-success"></i>                           
                            <p><a href="">Employee Documents</a></p>
                            
                        </li>
                           <li>
                             <i class="zmdi zmdi-balance-wallet text-success"></i>                           
                            <p><a href="">Employee Salary</a></p>
                            
                        </li>

                 
                    </ul>  
                </div>
            </div>
                        <div class="card">
                <div class="header">
                    <h2><strong>Setup</strong></h2>
                </div>
                <div class="body">
                    <ul class="notification list-unstyled">
                        <li>
                             <i class="zmdi zmdi-balance-wallet text-success"></i>                           
                            <p><a href="">HR Forms</a></p>
                            
                        </li>
                                             <li>
                             <i class="zmdi zmdi-balance-wallet text-success"></i>                           
                            <p><a href="">Letter Template</a></p>
                            
                        </li>
                    </ul>  
                </div>
            </div>
        </div>
        <div class="slim_scroll">
            <div class="card tasks">
                <div class="header">
                    <h2><strong>Project</strong> Status</h2>
                </div>
                
                                <ul>
                                        <li class="mid-level open">
                                            <a href="/ngapp/employee/main">
                                                <span class="menu_text">Main</span>
                                                <i class="icon-chevron-down"></i>
                                            </a>
                                                <ul>
                                                            <li class="leaf-level open">
                                                                <a href="/ngapp/employee/main/overview">
                                                                    <span class="menu_text">Overview</span>
                                                                </a>
                                                            </li>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/main/emp-directory">
                                                                    <span class="menu_text">Employee Directory</span>
                                                                </a>
                                                            </li>

                                                </ul>
                                        </li>
                                        <li class="mid-level">
                                            <a href="/ngapp/employee/info">
                                                <span class="menu_text">Information</span>
                                                <i class="icon-chevron-down"></i>
                                            </a>
                                                <ul>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/info/profile">
                                                                    <span class="menu_text">Employee Profile</span>
                                                                </a>
                                                            </li>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/info/accounts">
                                                                    <span class="menu_text">Bank/PF/ESI</span>
                                                                </a>
                                                            </li>

                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/info/assets">
                                                                    <span class="menu_text">Assets</span>
                                                                </a>
                                                            </li>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/info/career_history">
                                                                    <span class="menu_text">Position History</span>
                                                                </a>
                                                            </li>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/info/previousemployeement">
                                                                    <span class="menu_text">Previous Employment</span>
                                                                </a>
                                                            </li>

                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/info/employee-docs">
                                                                    <span class="menu_text">Employee Documents</span>
                                                                </a>
                                                            </li>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/info/employee-contract">
                                                                    <span class="menu_text">Employee Contracts</span>
                                                                </a>
                                                            </li>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/info/employeesalary">
                                                                    <span class="menu_text">Employee Salary</span>
                                                                </a>
                                                            </li>
                                                </ul>
                                        </li>

                                        <li class="mid-level">
                                            <a href="/ngapp/employee/setup">
                                                <span class="menu_text">Setup</span>
                                                <i class="icon-chevron-down"></i>
                                            </a>
                                                <ul>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/setup/letter-template">
                                                                    <span class="menu_text">Letter Template</span>
                                                                </a>
                                                            </li>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/setup/hr-forms">
                                                                    <span class="menu_text">HR Forms</span>
                                                                </a>
                                                            </li>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/setup/employeesegment">
                                                                    <span class="menu_text">Employee Segment</span>
                                                                </a>
                                                            </li>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/setup/v2/qb/employee-filter">
                                                                    <span class="menu_text">Employee Filter</span>
                                                                </a>
                                                            </li>
                                                </ul>
                                        </li>

                            </ul>
            </div>
        </div>



    </div>
    <div id="rightsidebar" class="right-sidebar">
        <ul class="nav nav-tabs">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#setting">Setting</a></li>        
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#activity">Activity</a></li>
        </ul>
        <div class="tab-content slim_scroll">
            <div class="tab-pane slideRight active" id="setting">
                <div class="card">
                    <div class="header">
                        <h2><strong>Colors</strong> Skins</h2>
                    </div>
                    <div class="body">
                        <ul class="choose-skin list-unstyled m-b-0">
                            <li data-theme="black" class="active">
                                <div class="black"></div>
                            </li>
                            <li data-theme="purple">
                                <div class="purple"></div>
                            </li>                   
                            <li data-theme="blue">
                                <div class="blue"></div>
                            </li>
                            <li data-theme="cyan">
                                <div class="cyan"></div>                    
                            </li>
                            <li data-theme="green">
                                <div class="green"></div>
                            </li>
                            <li data-theme="orange">
                                <div class="orange"></div>
                            </li>
                            <li data-theme="blush">
                                <div class="blush"></div>                    
                            </li>
                        </ul>
                    </div>
                </div>                
                <div class="card">
                    <div class="header">
                        <h2><strong>General</strong> Settings</h2>
                    </div>
                    <div class="body">
                        <ul class="setting-list list-unstyled m-b-0">
                            <li>
                                <div class="checkbox">
                                    <input id="checkbox1" type="checkbox">
                                    <label for="checkbox1">Report Panel Usage</label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox">
                                    <input id="checkbox2" type="checkbox" checked="">
                                    <label for="checkbox2">Email Redirect</label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox">
                                    <input id="checkbox3" type="checkbox">
                                    <label for="checkbox3">Notifications</label>
                                </div>                        
                            </li>
                            <li>
                                <div class="checkbox">
                                    <input id="checkbox4" type="checkbox">
                                    <label for="checkbox4">Auto Updates</label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox">
                                    <input id="checkbox5" type="checkbox" checked="">
                                    <label for="checkbox5">Offline</label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox m-b-0">
                                    <input id="checkbox6" type="checkbox">
                                    <label for="checkbox6">Location Permission</label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="card">
                    <div class="header">
                        <h2><strong>Left</strong> Menu</h2>
                    </div>
                    <div class="body theme-light-dark">
                        <button class="t-dark btn btn-primary btn-round btn-block">Dark</button>
                    </div>
                </div>               
            </div>
            <div class="tab-pane slideLeft" id="activity">
                <div class="card activities">
                    <div class="header">
                        <h2><strong>Recent</strong> Activity Feed</h2>
                    </div>
                    <div class="body">
                        <div class="streamline b-accent">
                            <div class="sl-item">
                                <div class="sl-content">
                                    <div class="text-muted">Just now</div>
                                    <p>Finished task <a href="#" class="text-info">#features 4</a>.</p>
                                </div>
                            </div>
                            <div class="sl-item b-info">
                                <div class="sl-content">
                                    <div class="text-muted">10:30</div>
                                    <p><a href="#">@Jessi</a> retwit your post</p>
                                </div>
                            </div>
                            <div class="sl-item b-primary">
                                <div class="sl-content">
                                    <div class="text-muted">12:30</div>
                                    <p>Call to customer <a href="#" class="text-info">Jacob</a> and discuss the detail.</p>
                                </div>
                            </div>
                            <div class="sl-item b-warning">
                                <div class="sl-content">
                                    <div class="text-muted">1 days ago</div>
                                    <p><a href="#" class="text-info">Jessi</a> commented your post.</p>
                                </div>
                            </div>
                            <div class="sl-item b-primary">
                                <div class="sl-content">
                                    <div class="text-muted">2 days ago</div>
                                    <p>Call to customer <a href="#" class="text-info">Jacob</a> and discuss the detail.</p>
                                </div>
                            </div>
                            <div class="sl-item b-primary">
                                <div class="sl-content">
                                    <div class="text-muted">3 days ago</div>
                                    <p>Call to customer <a href="#" class="text-info">Jacob</a> and discuss the detail.</p>
                                </div>
                            </div>
                            <div class="sl-item b-warning">
                                <div class="sl-content">
                                    <div class="text-muted">4 Week ago</div>
                                    <p><a href="#" class="text-info">Jessi</a> commented your post.</p>
                                </div>
                            </div>
                            <div class="sl-item b-warning">
                                <div class="sl-content">
                                    <div class="text-muted">5 days ago</div>
                                    <p><a href="#" class="text-info">Jessi</a> commented your post.</p>
                                </div>
                            </div>
                            <div class="sl-item b-primary">
                                <div class="sl-content">
                                    <div class="text-muted">5 Week ago</div>
                                    <p>Call to customer <a href="#" class="text-info">Jacob</a> and discuss the detail.</p>
                                </div>
                            </div>
                            <div class="sl-item b-primary">
                                <div class="sl-content">
                                    <div class="text-muted">3 Week ago</div>
                                    <p>Call to customer <a href="#" class="text-info">Jacob</a> and discuss the detail.</p>
                                </div>
                            </div>
                            <div class="sl-item b-warning">
                                <div class="sl-content">
                                    <div class="text-muted">1 Month ago</div>
                                    <p><a href="#" class="text-info">Jessi</a> commented your post.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</aside>
<!-- Main Content -->
<section class="content home">
    <div class="container-fluid">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Dropdown
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#">Disabled</a>
      </li>
    </ul>
<form class="form-inline md-form form-sm">
    <i class="fa fa-search" aria-hidden="true"></i>
    <input class="form-control form-control-sm ml-3 w-75" type="text" placeholder="Search" aria-label="Search">
</form>
  </div>
</nav>
    </div>
</section>
<section class="content">   
    <div class="container-fluid">
        <div class="block-header">
            <div class="row">
        <div class="col-lg-5 col-md-5 col-sm-12">
                         <ul class="breadcrumb padding-0">
                        <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">Employess</a></li>
                        <li class="breadcrumb-item active">Add employess</li>
                    </ul>
                </div> 

            </div>
        </div>

        <!-- Masked Input -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="body">
                        <div class="container">
    <div class="stepwizard">
        <div class="stepwizard-row setup-panel">
            <div class="stepwizard-step col-xs-3"> 
                <a href="#step-1"  class="btn btn-success btn-circle">1</a>
                <p><small>Basic Information</small></p>
            </div>
            <div class="stepwizard-step col-xs-3"> 
                <a href="#step-2" class="btn btn-default btn-circle" disabled="disabled">2</a>
                <p><small>Personal Details</small></p>
            </div>
            <div class="stepwizard-step col-xs-3"> 
                <a href="#step-3"  class="btn btn-default btn-circle" disabled="disabled">3</a>
                <p><small>Organization details</small></p>
            </div>
            <div class="stepwizard-step col-xs-3"> 
                <a href="#step-4"  class="btn btn-default btn-circle" disabled="disabled">4</a>
                <p><small>Payroll</small></p>
            </div>
                        <div class="stepwizard-step col-xs-3"> 
                <a href="#step-5"  class="btn btn-default btn-circle" disabled="disabled">4</a>
            <p><small>Documentation</small></p>
            </div>
        </div>
    </div>
    
                            <form id="form_validation" action="basic1" method="post">
       <div class="panel panel-primary setup-content" id="step-1" style="    width: 60%;    margin: 0 auto;">
            <div class="panel-heading">
                 <h3 class="panel-title text-center">Basic Information</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                <div class="col-sm-12">
               
                            <div class="form-group form-float">
                                <input type="text" class="form-control" placeholder="First Name" name="fname" required="" aria-required="true">
                            </div>
                            <div class="form-group form-float">
                                <input type="text" class="form-control" placeholder="Last Name" name="lname" required="" aria-required="true">
                            </div>
                            <div class="form-group form-float">
                                <input type="text" class="form-control" placeholder="Date of Birth" name="dob" required="" aria-required="true">
                            </div>
                            <div class="form-group">
                                <div class="radio inlineblock m-r-20">
                                    <input type="radio" name="gender" id="male" class="with-gap" value="male">
                                    <label for="male">Male</label>
                                </div>                                
                                <div class="radio inlineblock">
                                    <input type="radio" name="gender" id="Female" class="with-gap" value="female" checked="">
                                    <label for="Female">Female</label>
                                </div>
                            </div>
                  
         </div>


          </div>

                <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
                <!--<input type="submit" value="Submit">-->
            </div><!--panel body closed-->
        </div><!--panel closed-->


        
        <div class="panel panel-primary setup-content" id="step-2">
            <div class="panel-heading">
                 <h3 class="panel-title text-center">Personal Information</h3>
            </div>
            <div class="panel-body">
                <div class="row">                   
                    <div class="col-sm-4"><h6>Communication</h6>
                        <form id="form_validation">
                           <div class="form-group form-float">
                                <input type="text" class="form-control" placeholder="email Id" name="email" required="" aria-required="true">
                            </div>
                            <div class="form-group form-float">
                                <input type="text" class="form-control" placeholder="Phone Number" name="number" required="" aria-required="true">
                            </div>
                            <div class="form-group form-float">
                                <input type="text" class="form-control" placeholder="Country" name="con" required="" aria-required="true">
                            </div>
                                 <div class="form-group form-float">
                                <input type="text" class="form-control" placeholder="Address" name="add1" required="" aria-required="true">
                            </div>
                       
                    </div>

                    <div class="col-lg-4"><h6>Identification</h6>
                        
                            <div class="form-group form-float">
                                <input type="text" class="form-control" placeholder="PAN" name="pan" required="" aria-required="true">
                            </div>
                            <div class="form-group form-float">
                                <input type="text" class="form-control" placeholder="ESI" name="esi" required="" aria-required="true">
                            </div>
                            <div class="form-group form-float">
                                <input type="text" class="form-control" placeholder="DL" name="dl" required="" aria-required="true">
                            </div>
                                 <div class="form-group form-float">
                                <input type="text" class="form-control" placeholder="Aadhar" name="aadhar" required="" aria-required="true">
                            </div>
                        
                    </div>

                    <div class="col-lg-4"><h6>Social Links</h6>
                    
                            <div class="form-group form-float">
                                <input type="text" class="form-control" placeholder="LinkedIn" name="link" required="" aria-required="true">
                            </div>
                            <div class="form-group form-float">
                                <input type="text" class="form-control" placeholder="Facebook" name="fb" required="" aria-required="true">
                            </div>
                            <div class="form-group form-float">
                                <input type="text" class="form-control" placeholder="Twitter" name="twi" required="" aria-required="true">
                            </div>
                                <div class="input-group control-group after-add-more"><div class="input-group-btn"> 
                        <button class="btn btn-success add-more"  type="button"><i class="glyphicon glyphicon-plus"></i> Add</button>
                      </div>
              </div>
                                      <!-- Copy Fields-These are the fields which we get through jquery and then add after the above input,-->
        <div class="copy-fields hide">
          <div class="control-group input-group" style="margin-top:10px">
            <input type="text"  class="form-control" placeholder="Enter Name Here">
            <div class="input-group-btn"> 
              <button class="btn btn-danger remove" type="button"><i class="glyphicon glyphicon-remove"></i> x</button>
            </div>
          </div>
        </div>
                      
                    </div>
                    <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
                   

            </div>
        </div><!--panel body closed-->
    </div><!--pannel closed-->
   
        <div class="panel panel-primary setup-content" id="step-3">
            <div class="panel-heading">
                 <h3 class="panel-title text-center">Organization Details</h3>
            </div>
                 <div class="panel-body">
                <div class="row">                   
                    <div class="col-lg-6">
<!--                        <form id="form_validation">-->
                           <div class="form-group form-float">
                                <input type="text" class="form-control" placeholder="Employee ID" name="empid" required="" aria-required="true">
                            </div>
                            <div class="form-group form-float">
                                <input type="text" class="form-control" placeholder="Employment Type" name="emptype" required="" aria-required="true">
                            </div>
                            <div class="form-group form-float">
                                <input type="text" class="form-control" placeholder=" Job Title" name="jobtitle" required="" aria-required="true">
                            </div>
                                 <div class="form-group form-float">
                                <input type="text" class="form-control" placeholder="Team" name="team" required="" aria-required="true">
                          </div>
                       <!-- </form>-->
                    </div>

                    <div class="col-lg-6">
<!--                        <form id="form_validation">-->
                            <div class="form-group form-float">
                                <input type="text" class="form-control" placeholder="Department " name="dep" required="" aria-required="true">
                            </div>
                            <div class="form-group form-float">
                                <input type="text" class="form-control" placeholder="HR Manager" name="hr" required="" aria-required="true">
                            </div>
                            <div class="form-group form-float">
                                <input type="text" class="form-control" placeholder="Start Date" name="start" required="" aria-required="true">
                            </div>
                                 <div class="form-group form-float">
                                <input type="text" class="form-control" placeholder="Others" name="other" required="" aria-required="true">
                        </div>
<!--                        </form>-->-->
                    </div>

                    <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
                     <!--<input type="submit" value="Submit">-->
            </div>
        </div><!--panel body closed-->
        </div><!--panel closed-->
        
        
                        
        <div class="panel panel-primary setup-content" id="step-4">
            <div class="panel-heading">
                 <h3 class="panel-title text-center">Payroll Details</h3>
            </div>
                      <div class="panel-body">
                <div class="row">                   
                    <div class="col-lg-6">
<!--                        <form id="form_validation">-->
                           <div class="form-group form-float">
                                <input type="text" class="form-control" placeholder="Payrun Type" name="payrun" required="" aria-required="true">
                            </div>
                            <div class="form-group form-float">
                                <input type="text" class="form-control" placeholder="PayItems" name="payitem" required="" aria-required="true">
                            </div>
                            <div class="form-group form-float">
                                <input type="email" class="form-control" placeholder="End Date" name="eod" required="" aria-required="true">
                            </div>
                                 <div class="form-group form-float">
                                <input type="email" class="form-control" placeholder="Others" name="oth" required="" aria-required="true">
                            </div>
<!--                        </form>-->
                    </div>

                    <div class="col-lg-6">
<!--                        <form id="form_validation">-->
                            <div class="form-group form-float">
                                <input type="text" class="form-control" placeholder=""  aria-required="true">
                            </div>
                            <div class="form-group form-float">
                                <input type="text" class="form-control" placeholder=""  aria-required="true">
                            </div>
                            <div class="form-group form-float">
                                <input type="email" class="form-control" placeholder=""  aria-required="true">
                            </div>
                                 <div class="form-group form-float">
                                <input type="email" class="form-control" placeholder=""  aria-required="true">
                            </div>
<!--                        </form>-->
                    </div>

                    <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
                    <input type="submit" value="Submit">
            </div>
        </div><!--panel body closed-->
        </div><!--panel closed-->
 </form>
<!--        <div class="panel panel-primary setup-content" id="step-5">
            <div class="panel-heading">
                 <h3 class="panel-title text-center">Documentation</h3>
            </div>
                 <div class="panel-body">
                <div class="row">                   
                    <div class="col-lg-6">
                        <form id="form_validation">
                           <div class="form-group form-float">
                                <input type="text" class="form-control" placeholder="email Id" name="Fname" required="" aria-required="true">
                            </div>
                            <div class="form-group form-float">
                                <input type="text" class="form-control" placeholder="Phone Number" name="Lname" required="" aria-required="true">
                            </div>
                            <div class="form-group form-float">
                                <input type="email" class="form-control" placeholder="Country" name="bob" required="" aria-required="true">
                            </div>
                                 <div class="form-group form-float">
                                <input type="email" class="form-control" placeholder="Address" name="bob" required="" aria-required="true">
                            </div>
                        </form>
                    </div>

                    <div class="col-lg-6">
                        <form id="form_validation">
                            <div class="form-group form-float">
                                <input type="text" class="form-control" placeholder="PAN" name="Fname" required="" aria-required="true">
                            </div>
                            <div class="form-group form-float">
                                <input type="text" class="form-control" placeholder="ESI" name="Lname" required="" aria-required="true">
                            </div>
                            <div class="form-group form-float">
                                <input type="email" class="form-control" placeholder="DL" name="bob" required="" aria-required="true">
                            </div>
                                 <div class="form-group form-float">
                                <input type="email" class="form-control" placeholder="Aadhar" name="bob" required="" aria-required="true">
                            </div>
                        </form>
                    </div>

                    <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
            </div>
        </div>panel body closed
        </div>panel closed-->
    </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Masked Input -->        
        <!-- Multi Select -->

        <!-- #END# Multi Select -->


    </section>
<!-- Jquery Core Js --> 
<script src="bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
<script src="bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 

<script src="plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> <!-- Bootstrap Colorpicker Js --> 
<script src="plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script> <!-- Input Mask Plugin Js --> 
<script src="plugins/multi-select/js/jquery.multi-select.js"></script> <!-- Multi Select Plugin Js --> 
<script src="plugins/jquery-spinner/js/jquery.spinner.js"></script> <!-- Jquery Spinner Plugin Js --> 
<script src="plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script> <!-- Bootstrap Tags Input Plugin Js --> 
<script src="plugins/nouislider/nouislider.js"></script> <!-- noUISlider Plugin Js --> 

<script src="bundles/mainscripts.bundle.js"></script><!-- Custom Js --> 
<script src="js/pages/forms/advanced-form-elements.js"></script> 
<script>
$(document).ready(function () {

    var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-success').addClass('btn-default');
            $item.addClass('btn-success');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for (var i = 0; i < curInputs.length; i++) {
            if (!curInputs[i].validity.valid) {
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid) nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-success').trigger('click');
});
</script>

<script>

 
    $(document).ready(function() {
 
    //here first get the contents of the div with name class copy-fields and add it to after "after-add-more" div class.
      $(".add-more").click(function(){ 
          var html = $(".copy-fields").html();
          $(".after-add-more").after(html);
      });
//here it will remove the current value of the remove button which has been pressed
      $("body").on("click",".remove",function(){ 
          $(this).parents(".control-group").remove();
      });
 
    });
 
</script>
<script>
$(".add-more").click(function(){
    $("copy-fields").show(1000);
});
</script>

</body>

</html>