<!doctype html>
<html class="no-js " lang="en">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<title>Home</title>
<!-- <link rel="icon" href="favicon.ico" type="image/x-icon"> --> <!-- Favicon-->
<link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="plugins/morrisjs/morris.css" />
<link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-2.0.3.min.css"/>
<!-- Custom Css -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>

<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="css/color_skins.css">
<link rel="stylesheet" href="plugins/toastr/toastr.min.css">
<style>

/* Nav Meu Container */
#nav-menu-container {
  float: right;
  margin: 0;
}

@media (max-width: 768px) {
  #nav-menu-container {
    display: none;
  }
}

/* Nav Meu Styling */
.nav-menu a {
  padding: 10px 8px;
  text-decoration: none;
  display: inline-block;
  color: #fbfdfc;
  font-family: "Raleway", sans-serif;
  font-weight: 700;
  font-size: 14px;
  outline: none;
}

.nav-menu li:hover > a, .nav-menu > .menu-active > a {
  color: #50d8af;
}

.nav-menu > li {
  margin-left: 10px;
}

.nav-menu ul {
  margin: 4px 0 0 0;
  padding: 10px;
  box-shadow: 0px 0px 30px rgba(127, 137, 161, 0.25);
  background: #fff;
}

.nav-menu ul li {
  transition: 0.3s;
}

.nav-menu ul li a {
  padding: 10px;
  color: #333;
  transition: 0.3s;
  display: block;
  font-size: 13px;
  text-transform: none;
}

.nav-menu ul li:hover > a {
  color: #50d8af;
}

.nav-menu ul ul {
  margin: 0;
}

/* Mobile Nav Toggle */
#mobile-nav-toggle {
  position: fixed;
  right: 0;
  top: 0;
  z-index: 999;
  margin: 20px 20px 0 0;
  border: 0;
  background: none;
  font-size: 24px;
  display: none;
  transition: all 0.4s;
  outline: none;
  cursor: pointer;
}

#mobile-nav-toggle i {
  color: #555;
}

@media (max-width: 768px) {
  #mobile-nav-toggle {
    display: inline;
  }
}

/* Mobile Nav Styling */
#mobile-nav {
  position: fixed;
  top: 0;
  padding-top: 18px;
  bottom: 0;
  z-index: 998;
  background: rgba(52, 59, 64, 0.9);
  left: -260px;
  width: 260px;
  overflow-y: auto;
  transition: 0.4s;
}

#mobile-nav ul {
  padding: 0;
  margin: 0;
  list-style: none;
}

#mobile-nav ul li {
  position: relative;
}

#mobile-nav ul li a {
  color: #fff;
  font-size: 16px;
  overflow: hidden;
  padding: 10px 22px 10px 15px;
  position: relative;
  text-decoration: none;
  width: 100%;
  display: block;
  outline: none;
}

#mobile-nav ul li a:hover {
  color: #fff;
}

#mobile-nav ul li li {
  padding-left: 30px;
}

#mobile-nav ul .menu-has-children i {
  position: absolute;
  right: 0;
  z-index: 99;
  padding: 15px;
  cursor: pointer;
  color: #fff;
}

#mobile-nav ul .menu-has-children i.fa-chevron-up {
  color: #50d8af;
}

#mobile-nav ul .menu-item-active {
  color: #50d8af;
}

#mobile-body-overly {
  width: 100%;
  height: 100%;
  z-index: 997;
  top: 0;
  left: 0;
  position: fixed;
  background: rgba(52, 59, 64, 0.9);
  display: none;
}

/*--------------------------------------------------------------
# Navigation Menu
--------------------------------------------------------------*/
/* Nav Menu Essentials */
.nav-menu, .nav-menu * {
  margin: 0;
  padding: 0;
  list-style: none;
}

.nav-menu ul {
  position: absolute;
  display: none;
  top: 100%;
  left: 0;
  z-index: 99;
}

.nav-menu li {
  position: relative;
  white-space: nowrap;
}

.nav-menu > li {
  float: left;
}

.nav-menu li:hover > ul,
.nav-menu li.sfHover > ul {
  display: block;
}

.nav-menu ul ul {
  top: 0;
  left: 100%;
}

.nav-menu ul li {
  min-width: 180px;
}

/* Nav Menu Arrows */
.sf-arrows .sf-with-ul {
  padding-right: 22px;
}

.sf-arrows .sf-with-ul:after {
  content: "\f107";
  position: absolute;
  right: 8px;
  font-family: FontAwesome;
  font-style: normal;
  font-weight: normal;
}

.sf-arrows ul .sf-with-ul:after {
  content: "\f105";
}

/* Nav Meu Container */
#nav-menu-container {
  float: right;
  margin: 0;
}

@media (max-width: 768px) {
  #nav-menu-container {
    display: none;
  }
}
/* Mobile Nav body classes */
body.mobile-nav-active {
  overflow: hidden;
}

body.mobile-nav-active #mobile-nav {
  left: 0;
}

body.mobile-nav-active #mobile-nav-toggle {
  color: #fff;
}
/*--------------------------------------------------------------
# Intro Section
--------------------------------------------------------------*/
#intro {
  width: 100%;
  height: 60vh;
  position: relative;
  /* background: url("../img/intro-carousel/1.jpg") no-repeat; */
  background-size: cover;
}

#intro .intro-content {
  position: absolute;
  bottom: 0;
  top: 0;
  left: 0;
  right: 0;
  z-index: 10;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  text-align: center;
}

#intro .intro-content h2 {
  color: #0c2e8a;
  margin-bottom: 30px;
  font-size: 64px;
  font-weight: 700;
}

#intro .intro-content h2 span {
  color: #50d8af;
  text-decoration: underline;
}

@media (max-width: 767px) {
  #intro .intro-content h2 {
    font-size: 34px;
  }
}

#intro .intro-content .btn-get-started, #intro .intro-content .btn-projects {
  font-family: "Raleway", sans-serif;
  font-size: 15px;
  font-weight: bold;
  letter-spacing: 1px;
  display: inline-block;
  padding: 10px 32px;
  border-radius: 2px;
  transition: 0.5s;
  margin: 10px;
  color: #fff;
}

#intro .intro-content .btn-get-started {
  background: #0c2e8a;
  border: 2px solid #0c2e8a;
}

#intro .intro-content .btn-get-started:hover {
  background: none;
  color: #0c2e8a;
}

#intro .intro-content .btn-projects {
  background: #50d8af;
  border: 2px solid #50d8af;
}

#intro .intro-content .btn-projects:hover {
  background: none;
  color: #50d8af;
}

#intro #intro-carousel {
  z-index: 8;
}

#intro #intro-carousel::before {
  content: '';
  background-color: rgba(255, 255, 255, 0.7);
  position: absolute;
  height: 100%;
  width: 100%;
  top: 0;
  right: 0;
  left: 0;
  bottom: 0;
  z-index: 7;
}

#intro #intro-carousel .item {
  width: 100%;
  height: 60vh;
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  -webkit-transition-property: opacity;
  transition-property: opacity;
}
.pricingTable{
    text-align: center;
    border: 1px solid #dbdbdb;
    box-shadow: 0 0 10px rgba(0, 0, 0, 0.14);
    border-radius: 10px;
}
.pricingTable:hover .price-value{
    transform: rotate(360deg);
    transition:0.6s ease;
}
.pricingTable > .pricingTable-header{
    color:#fff;
}
.pricingTable-header > .heading{
    background: #e67e22;
    display: block;
    padding: 7px 10px;
    border-radius: 10px 10px 0 0;
}
.heading > h3{
    font-weight:bold;
    margin: 0;
    text-transform: uppercase;
}
.heading > .subtitle{
    font-size: 13px;
    margin-top: 3px;
    display: block;
}
.pricingTable-header > .price-value{
    width: 120px;
    height: 120px;
    border-radius: 50%;
    border:2px solid #474747;
    display: block;
    margin: 0 auto;
    color:#474747;
    font-size: 25px;
    font-weight: 800;
    margin-top: 10px;
    padding: 20px 10px 0 10px;
    line-height: 35px;
}
.price-value > span{
    font-size: 40px;
}
.price-value > .mo{
    display: inline-block;
    line-height: 0;
    padding-top: 13px;
    border-top: 1px solid #474747;
    text-transform: uppercase;
    letter-spacing: 2px;
    font-size: 13px;
    margin-top: -20px;
}
.pricingTable > .pricingContent{
    margin: 10px 0 0 0;
}
.pricingContent > ul{
    padding: 0;
    list-style: none;
    margin-bottom: 0;
}
.pricingContent > ul > li{
    border-top: 1px solid #dbdbdb;
    padding: 10px 0;
    text-align: center;
    transition:0.4s ease-in-out;
}
.pricingContent > ul > li:before{
    content: "\f101";
    font-family: "Font Awesome 5 Free"; font-weight: 900;
    color:#e67e22;
    margin-right: 10px;
}
.pricingContent > ul > li:hover{
    padding-left: 15px;
    transition:0.4s ease-in-out;
}
.pricingContent > ul > li:last-child{
    border-bottom: 1px solid #dbdbdb;
}
.pricingTable > .pricingTable-sign-up{
    padding: 25px 0;
}
.btn-block{
    width: 50%;
    margin: 0 auto;
    background: #e67e22;
    border:0px none;
    padding: 10px 5px;
    color:#fff;
    text-transform: capitalize;
    transition:0.3s ease;
    border-radius: 0px;
}
.btn-block:after{
    content: "\f090";
    font-family: "Font Awesome 5 Free"; font-weight: 900;
    padding-left: 10px;
    font-size: 15px;
}
.btn-block:hover{
    border-radius: 10px;
    background: #e67e22;
    color:#fff;
}
@media screen and (max-width: 990px){
    .pricingTable{
        margin-bottom: 20px;
    }
}

</style>
</head>
<body>


<section id="intro">

<div class="container">
    <div class="row">
        <div class="col-md-4 col-sm-6">
            <div class="pricingTable">
                <div class="pricingTable-header">
                    <span class="heading">
                        <h3>Starter</h3>
                        <span class="subtitle">Lorem ipsum dolor</span>
                    </span>
                    <span class="price-value"><i class="fa fa-usd"></i><span>25</span><span class="mo">month</span></span>
                </div>
 
                <div class="pricingContent">
                    <ul>
                        <li><b>50GB</b> Disk Space</li>
                        <li><b>50</b> Email Accounts</li>
                        <li><b>50GB</b> Monthly Bandwidth</li>
                        <li><b>50</b> Domains</li>
                        <li><b>Unlimited</b> Subdomains</li>
                    </ul>
                </div><!-- /  CONTENT BOX-->
 
                <div class="pricingTable-sign-up">
                    <a href="#" class="btn btn-block btn-default">sign up</a>
                </div><!-- BUTTON BOX-->
            </div>
        </div>

                <div class="col-md-4 col-sm-6">
            <div class="pricingTable">
                <div class="pricingTable-header">
                    <span class="heading">
                        <h3>Standard</h3>
                        <span class="subtitle">Lorem ipsum dolor</span>
                    </span>
                    <span class="price-value"><i class="fa fa-usd"></i><span>50</span><span class="mo">month</span></span>
                </div>
 
                <div class="pricingContent">
                    <ul>
                        <li><b>50GB</b> Disk Space</li>
                        <li><b>50</b> Email Accounts</li>
                        <li><b>50GB</b> Monthly Bandwidth</li>
                        <li><b>50</b> Domains</li>
                        <li><b>Unlimited</b> Subdomains</li>
                    </ul>
                </div><!-- /  CONTENT BOX-->
 
                <div class="pricingTable-sign-up">
                    <a href="#" class="btn btn-block btn-default">sign up</a>
                </div><!-- BUTTON BOX-->
            </div>
        </div>
                <div class="col-md-4 col-sm-6">
            <div class="pricingTable">
                <div class="pricingTable-header">
                    <span class="heading">
                        <h3>Premium</h3>
                        <span class="subtitle">Lorem ipsum dolor</span>
                    </span>
                    <span class="price-value"><i class="fa fa-usd"></i><span>70</span><span class="mo">month</span></span>
                </div>
 
                <div class="pricingContent">
                    <ul>
                        <li><b>50GB</b> Disk Space</li>
                        <li><b>50</b> Email Accounts</li>
                        <li><b>50GB</b> Monthly Bandwidth</li>
                        <li><b>50</b> Domains</li>
                        <li><b>Unlimited</b> Subdomains</li>
                    </ul>
                </div><!-- /  CONTENT BOX-->
 
                <div class="pricingTable-sign-up">
                    <a href="tregister" class="btn btn-block btn-default">sign up</a>
                </div><!-- BUTTON BOX-->
            </div>
        </div>

    </div>
</div>
  </section><!-- #intro -->
<!-- Jquery Core Js -->
<script src="bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js ( jquery.v3.2.1, Bootstrap4 js) -->
<script src="bundles/vendorscripts.bundle.js"></script> <!-- slimscroll, waves Scripts Plugin Js -->

<script src="bundles/knob.bundle.js"></script> <!-- Jquery Knob-->
<script src="bundles/jvectormap.bundle.js"></script> <!-- JVectorMap Plugin Js -->
<script src="bundles/morrisscripts.bundle.js"></script> <!-- Morris Plugin Js --> 
<script src="bundles/sparkline.bundle.js"></script> <!-- sparkline Plugin Js --> 
<script src="bundles/doughnut.bundle.js"></script>

<script src="bundles/mainscripts.bundle.js"></script>
<script src="js/pages/index.js"></script>
<script src="plugins/toastr/toastr.js"></script>

</body>

</html>