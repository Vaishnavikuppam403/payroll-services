<%-- 
    Document   : login
    Created on : 3 Aug, 2018, 4:27:06 PM
    Author     : Burgeonits
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
       <title>HR Tool</title><meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="css/bootstrap.min.css" />
<!-- 		<link rel="stylesheet" href="css/bootstrap-responsive.min.css" /> -->
        <link rel="stylesheet" href="css/maruti-login.css" />
    </head>
    <body>
        <style>
   .x-header {
    background-image: none;
}
.x-header {
    background: #00a3d3;
}
.x-header {
    color: #fff;
    display: block;
    background-color: #0590c3;
 
    background: -webkit-gradient(linear,50% 0,50% 100%,color-stop(0%,#1696c5),color-stop(100%,#048bbe));
    background: -moz-linear-gradient(#1696c5,#048bbe);
    background: -webkit-linear-gradient(#1696c5,#048bbe);
    background: linear-gradient(#1696c5,#048bbe);
}

        </style>
        
        <div class="x-header">
            <div class="inner">
                    <h1><span>HR Tool</span></h1>
            </div>
        </div>
        
        <h4>${msg}</h4>
        
        <div align="right">
          <div id="google_translate_element"></div>
        </div>
        <div class="container login-container">
            <div class="row">
                <div class="col-xs-12 col-sm-3 Login">
                 <h2>Login</h2>
                 <p>Enter detials to login</p>
                </div>
                <div class="col-sm-1 hidden-xs divider">
                <div class="horizontal-divider"></div>
                </div>
                <div class="col-xs-12 col-sm-8 login-fields">
                                    <h2>Login in to continue</h2>
                                    
                                    <form action="login1" method="post">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="your username" required="required" name="username">
                                        </div>
                                        <div class="form-group">
                                            <input type="password" class="form-control" placeholder="******" required="required" name="pass">
                                        </div>
                                             <div class="form-group"><a href="" tabindex="5" class="forgot-password">Forgot Password?</a>
                                                 </div>
                                        <div class="form-group">
                                            <div class="col-sm-10">
                                              <div class="checkbox">
                                                <label>
                                                  <input type="checkbox"> Remember me
                                                </label>
                                              </div>
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <div class="col-sm-10">
                                              <input type="submit" class="btn btn-default lgn-btn" value="Sign in"/>
                                            </div>
                                          </div>
                                      </form>
                                    </div>
                                   


                    </div>                    
            </div>
        </div>

        <div class="container login-container2">
            <div class="row">
                <div class="col-xs-12 col-sm-3 Login">
                 <h2>Authenty</h2>
                 <p>your account</p>
                </div>
                <div class="col-sm-1 hidden-xs divider">
                <div class="horizontal-divider"></div>
                </div>
                <div class="col-xs-12 col-sm-8 login-fields">
                                    <h2>Forgotten your password?</h2>
                                    <p>To reset your password, enter the email address you use to login to Cadena. A link will be emailed to this address which will let you reset your password.</p>
                                    
                                    <form>
                                        <div class="x-warning">
        <p>The email address entered was not valid.<br> Please enter your email address and the password will be emailed to you.</p>
    </div>
                                        <div class="form-group">
                                            <input type="email" class="form-control" placeholder="Email" required="required">
                                        </div>
 
                                          <div class="form-group">
                                            <div class="col-sm-10">
                                              <button type="submit" class="btn btn-default lgn-btn">Send link</button>&nbsp or 
                                              <a href="login2.html">&nbsp cancel</a>
                                            </div>
                                          </div>

                                    </div>
                                    </form>

                                    
                    </div>                    
            </div>
      
<script>
    function googleTranslateElementInit() {
        new google.translate.TranslateElement(
            {pageLanguage: 'en'},
            'google_translate_element'
        );

        /*
            To remove the "powered by google",
            uncomment one of the following code blocks.
            NB: This breaks Google's Attribution Requirements:
            https://developers.google.com/translate/v2/attribution#attribution-and-logos
        */

        // Native (but only works in browsers that support query selector)
        //if(typeof(document.querySelector) == 'function') {
        //    document.querySelector('.goog-logo-link').setAttribute('style', 'display: none');
        //    document.querySelector('.goog-te-gadget').setAttribute('style', 'font-size: 0');
        //}

        // If you have jQuery - works cross-browser - uncomment this
        //jQuery('.goog-logo-link').css('display', 'none');
        //jQuery('.goog-te-gadget').css('font-size', '0');
    }
</script>
<script src="http://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
        </div>
        <script src="js/jquery.min.js"></script>  
        <script src="js/maruti.login.js"></script> 
    </body>

  
    
</html>
