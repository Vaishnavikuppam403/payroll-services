<%-- 
    Document   : Success
    Created on : 3 Aug, 2018, 5:52:17 PM
    Author     : Burgeonits
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="css/bootstrap1.min.css" />
<link rel="stylesheet" href="css/bootstrap-responsive.min.css" />
<link rel="stylesheet" href="css/fullcalendar.css" />
<link rel="stylesheet" href="css/maruti-style.css" />
<link rel="stylesheet" href="css/font-awesome.min.css" />
<link rel="stylesheet" href="css/font-awesome.css" />
<link rel="stylesheet" href="css/maruti-media.css" class="skin-color" />
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">


</head>
<style>
.user-profile {
    position: relative;
    padding: 20px 25px;
    background-color: #f4f4f4;
    margin-bottom: 15px;
    border-radius: .25rem .25rem 0 0;
}
.user-avatar {
    display: -webkit-inline-flex;
    display: -ms-inline-flexbox;
    display: inline-flex;
    -webkit-flex-direction: row;
    -ms-flex-direction: row;
    flex-direction: row;
    -webkit-flex-wrap: nowrap;
    -ms-flex-wrap: nowrap;
    flex-wrap: nowrap;
    position: relative;
    overflow: hidden;
    -webkit-justify-content: center;
    justify-content: center;
    -webkit-align-items: center;
    align-items: center;
    -webkit-flex-shrink: 0;
    flex-shrink: 0;
    border-radius: 50%;
    -moz-user-select: none;
    -webkit-user-select: none;
    -ms-user-select: none;
    border: 1px solid #ced4da;
}
.user-profile .user-detail {
    -webkit-flex: 1;
    flex: 1;
    -ms-flex-align: center;
    -webkit-align-self: center;
    align-self: center;
}
.user-detail .user-name {
    margin-bottom: 2px;
    font-weight: 400;
    text-transform: capitalize;
}


.user-detail .user-name {
    cursor: pointer;
}
.ml-2, .mx-2 {
    margin-left: .5rem!important;
}
.size-40 {
    height: 40px!important;
    width: 40px!important;
    line-height: 40px;
}
.user-profile {
    display: -webkit-flex;
    display: -moz-flex;
    display: -ms-flex;
    display: -o-flex;
    /* display: flex; */
    /* -webkit-flex-direction: row; */
    -ms-flex-direction: row;
    /* flex-direction: row; */
    /* -webkit-flex-wrap: wrap; */
    -ms-flex-wrap: wrap;
    /* flex-wrap: wrap; */
    /* -webkit-align-items: flex-start; */
    align-items: flex-start;
}
.user-ul li{
    float: left;
    display: block;
    padding: 14px;
}
.user-ul li a{
margin-left: 5px;
}
</style>
<body>

<!--Header-part-->
<div id="header">

</div>
<!--close-Header-part--> 



<!--top-Header-menu-->

<div id="user-nav" class="navbar navbar-inverse">
  <ul class="nav">

        <li class="dropdown" id="profile-info"><a href="#" data-toggle="dropdown" data-target="#profile-info" class="dropdown-toggle"><i class="icon icon-user"></i> <span class="text">Profile</span> <b class="caret"></b></a>
      <ul class="dropdown-menu" style="   width: 300px;">
  
        <div class="user-profile"><img class="user-avatar border-0 size-40" src="images/gallery/domnic-harris.jpg" alt="User"><div class="user-detail ml-2"><h4 class="user-name mb-0">Chris Harris</h4><small>Administrator</small></div></div>
        <ul class="user-ul">
        <li><i class="fa fa-user" aria-hidden="true"></i><a class="sInbox" title="" href="#">Profile</a></li>
        <li><i class="fa fa-cog" aria-hidden="true"></i><a class="sOutbox" title="" href="#">Account</a></li>
        <li><i class="fa fa-power-off" aria-hidden="true"></i><a class="sTrash" title="" href="#">Logout</a></li>
        </ul>
      </ul>
    </li>
    <li class=" dropdown" id="menu-messages"><a href="#" data-toggle="dropdown" data-target="#menu-messages" class="dropdown-toggle"><i class="icon icon-envelope"></i> <span class="text">Messages</span><b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li><a class="sAdd" title="" href="#">new message</a></li>
        <li><a class="sInbox" title="" href="#">inbox</a></li>
        <li><a class="sOutbox" title="" href="#">outbox</a></li>
        <li><a class="sTrash" title="" href="#">trash</a></li>
      </ul>
    </li>
    <li class=""><a title="" href="#"><i class="icon icon-cog"></i> <span class="text">Settings</span></a></li>
    <li class=""><a title="" href="login2.html"><i class="icon icon-share-alt"></i> <span class="text">Logout</span></a></li>
  </ul>
</div>

<div id="search">
  <input type="text" placeholder="Search here..."/>
  <button type="submit" class="tip-left" title="Search"><i class="icon-search icon-white"></i></button>

</div>

<!--close-top-Header-menu-->

<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>

  <ul>
    <li class="active"><a href="index.html"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>

    <li class="submenu"> <a href="#"><i class="icon icon-th-list"></i> <span>Projects</span> <span class="label label-important">3</span></a>
      <ul>
        <li><a href="">All Projects</a></li>
        <li><a href="">Completed</a></li>
        <li><a href="">Others</a></li>
      </ul>
    </li>

    <li class="submenu"> <a href="#"><i class="icon icon-file"></i> <span>Reports</span> <span class="label label-important">4</span></a>
      <ul>
        <li><a href="index2.html">Dashboard2</a></li>
        <li><a href="gallery.html">Gallery</a></li>
        <li><a href="calendar.html">Calendar</a></li>
        <li><a href="chat.html">Chat option</a></li>
      </ul>
    </li>
  </ul>
</div>
<div id="content">
  <div id="content-header">
    <div id="breadcrumb"> <a href="index.html" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>

    </div>

  </div>
  <div class="container">
    <span class="xui-text-placeholder pull-right">Your last login: <a class="xui-text-placeholder" data-automationid="lastLogin-label" style="text-decoration: underline;">17 hours ago</a> &nbsp from India</span>
    <div class="quick-actions_homepage">
    <ul class="quick-actions">
          <li> <a href="#"> <i class="icon-dashboard"></i> My Dashboard </a> </li>
          <li> <a href="#"> <i class="icon-shopping-bag"></i> Accounts</a> </li>
          <li> <a href="#"> <i class="icon-web"></i> Contacts </a> </li>
          <li> <a href="#"> <i class="icon-people"></i> Manage Users </a> </li>
          <li> <a href="#"> <i class="icon-calendar"></i> Settings </a> </li>
          
        </ul>
        <h1>${message}</h1>
        <h1>${File}</h1>
        
   </div>
 
<div class="container-fluid">
              <div class="row-fluid">
          <div class="span4">
<div class="widget-box">
              <div class="widget-title">
                <span class="icon"><i class="icon-time"></i></span>
                <h5>To Do List</h5>
                
              </div>
              <div class="widget-content nopadding">
                <table class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Description</th>
                                                <th>Status</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="taskDesc"><i class="icon-info-sign"></i> Created Account</td>
                                                <td class="taskStatus"><span class="in-progress">in progress</span></td>
                                                
                                            </tr>
                                            <tr>
                                                <td class="taskDesc"><i class="icon-plus-sign"></i> Permissions</td>
                                                <td class="taskStatus"><span class="pending">pending</span></td>
                                                
                                            </tr>
                                            <tr>
                                                <td class="taskDesc"><i class="icon-ok-sign"></i>Scheduling</td>
                                                <td class="taskStatus"><span class="done">done</span></td>
                                               
                                            </tr>
                                                                                        <tr>
                                                <td class="taskDesc"><i class="icon-ok-sign"></i>Meetings</td>
                                                <td class="taskStatus"><span class="done">Pending</span></td>
                                               
                                            </tr>
                                                                                        <tr>
                                                <td class="taskDesc"><i class="icon-ok-sign"></i> Reports</td>
                                                <td class="taskStatus"><span class="done">Pending</span></td>
                                                
                                            </tr>
                                        </tbody>
                                    </table>
              </div>
            </div>
          </div>
          <div class="span4">
<div class="widget-box">
                                <div class="widget-title">
                                    <span class="icon"><i class="icon-repeat"></i></span>
                                    <h5>Recent Activity</h5>
                                </div>
                                <div class="widget-content nopadding">
                                    <ul class="activity-list">
                                        <li><a href="#">
                                            <i class="icon-user"></i>
                                            <strong>HR</strong>Logged <strong>1 user</strong> <span>2 hours ago</span>
                                        </a></li>
                                        <li><a href="#">
                                            <i class="icon-file"></i>
                                            <strong>Got mail</strong> Theme <strong>to user 1</strong> <span>2months ago</span>
                                        </a></li>
                                        <li><a href="#">
                                            <i class="icon-envelope"></i>
                                            <strong>Added</strong> added<strong> User 1 Account</strong> <span>2 days ago</span>
                                        </a></li>
                                        <li><a href="#">
                                            <i class="icon-user"></i>
                                            <strong>Admin</strong> changed <strong>password</strong> <span>week ago</span>
                                        </a></li>
                                        <li><a href="#">
                                            <i class="icon-user"></i>
                                            <strong>Admin</strong> request for <strong>change password</strong> <span>week ago</span>
                                        </a></li>

                                         <li><a href="#">
                                            <i class="icon-user"></i>
                                            <strong>Others</strong> <span>week ago</span>
                                        </a></li>
                                    </ul>
                                </div>
                            </div>
          </div>
          <div class="span4">
<div class="widget-box">
                  <div class="widget-title"> <span class="icon"> <i class="icon-refresh"></i> </span>
                    <h5>News updates</h5>
                  </div>
                  <div class="widget-content nopadding updates">
                    <div class="new-update clearfix"><i class="icon-ok-sign"></i>
                      <div class="update-done"><a title="" href="#"><strong>update 1</strong></a> <span>dolor sit amet, consectetur adipiscing eli</span> </div>
                      <div class="update-date"><span class="update-day">20</span>jan</div>
                    </div>
                    <div class="new-update clearfix"> <i class="icon-gift"></i> <span class="update-notice"> <a title="" href="#"><strong>Update 2 </strong></a> <span>dolor sit amet, consectetur adipiscing eli</span> </span> <span class="update-date"><span class="update-day">11</span>jan</span> </div>
                    <div class="new-update clearfix"> <i class="icon-move"></i> <span class="update-alert"> <a title="" href="#"><strong>Update 3</strong></a> <span>everything was solved</span> </span> <span class="update-date"><span class="update-day">07</span>Jan</span> </div>
                    <div class="new-update clearfix"> <i class="icon-move"></i> <span class="update-alert"> <a title="" href="#"><strong>Update 4</strong></a> <span>everything was solved</span> </span> <span class="update-date"><span class="update-day">07</span>Jan</span> </div>
                 
                  </div>
                </div>
          </div>
        </div><hr>
 
                <div class="row-fluid">
        <div class="span12">
            
   
            <div class="widget-box">
                            <div class="widget-title">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#tab1">Tab1</a></li>
                                    <li><a data-toggle="tab" href="#tab2">Tab2</a></li>
                                    <li><a data-toggle="tab" href="#tab3">Tab3</a></li>
                                </ul>
                            </div>
                            <div class="widget-content tab-content">
                                <div id="tab1" class="tab-pane active">
                                  <p>And is full of waffle to It has multiple paragraphs and is full of waffle to pad out the comment. Usually, you just wish these sorts of comments would come to an end.multiple paragraphs and is full of waffle to pad out the comment. Usually, you just wish these sorts of comments would come to an end.multiple paragraphs and is full of waffle to pad out the comment. Usually, you just wish these sorts of comments would come to an end. </p>
                            
                                </div>
                                <div id="tab2" class="tab-pane">
                                <p> waffle to pad out the comment. Usually, you just wish these sorts of comments would come to an end.multiple paragraphs and is full of waffle to pad out the comment. Usually, you just wish these sorts of comments would come to an end. </p>
                                
                                </div>
                                <div id="tab3" class="tab-pane">
                                  <p>full of waffle to pad out the comment. Usually, you just wish these sorts of comments would come to an end.multiple paragraphs and is full of waffle to pad out the comment. Usually, you just wish these sorts of comments would come to an end. </p>
                 
                                </div>
                            </div>                            
                        </div>
     
          </div>
         </div>
      </div>

  
  </div>
</div>
</div>
</div>

<div class="row-fluid">
  <div id="footer" class="span12"> All rights reserved <a href="">Cadena Tech</a> </div>
</div>
<script src="js/excanvas.min.js"></script> 
<script src="js/jquery.min.js"></script> 
<script src="js/jquery.ui.custom.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/jquery.flot.min.js"></script> 
<script src="js/jquery.flot.resize.min.js"></script> 
<script src="js/jquery.peity.min.js"></script> 
<script src="js/fullcalendar.min.js"></script> 
<script src="js/maruti.js"></script> 
<script src="js/maruti.dashboard.js"></script> 
<script src="js/maruti.chat.js"></script> 
 

<script type="text/javascript">
  // This function is called from the pop-up menus to transfer to
  // a different page. Ignore if the value returned is a null string:
  function goPage (newURL) {

      // if url is empty, skip the menu dividers and reset the menu selection to default
      if (newURL != "") {
      
          // if url is "-", it is this page -- reset the menu:
          if (newURL == "-" ) {
              resetMenu();            
          } 
          // else, send page to designated URL            
          else {  
            document.location.href = newURL;
          }
      }
  }

// resets the menu selection upon entry to this page:
function resetMenu() {
   document.gomenu.selector.selectedIndex = 2;
}
</script>
</body>
</html>
