<%-- 
    Document   : register
    Created on : 3 Aug, 2018, 4:26:56 PM
    Author     : Burgeonits
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
         <title>HR Tool</title><meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="css/bootstrap.min.css" />
<!-- 		<link rel="stylesheet" href="css/bootstrap-responsive.min.css" /> -->
        <link rel="stylesheet" href="css/maruti-login.css" />
    </head>
    <body>
        <style>
   .x-header {
    background-image: none;
}
.x-header {
    background: #00a3d3;
}
.x-header {
    color: #fff;
    display: block;
    background-color: #0590c3;
 
    background: -webkit-gradient(linear,50% 0,50% 100%,color-stop(0%,#1696c5),color-stop(100%,#048bbe));
    background: -moz-linear-gradient(#1696c5,#048bbe);
    background: -webkit-linear-gradient(#1696c5,#048bbe);
    background: linear-gradient(#1696c5,#048bbe);
}

        </style>
        <div class="x-header">
            <div class="inner">
                    <h1><span>HR Tool</span></h1>
            </div>
        </div>
        <div class="container login-container">
            <div class="row">
                <div class="col-xs-12 col-sm-3 Login">
                 <h2>Register</h2>
                 <p>Enter detials to register</p>
                </div>
                <div class="col-sm-1 hidden-xs divider">
                <div class="horizontal-divider"></div>
                </div>
                <div class="col-xs-12 col-sm-8 login-fields">
                                    <h2>Register</h2>
                                    
                                    <form action="register1" method="post">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="First Name" required="required" name="firstname">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Last Name" required="required" name="lastname">
                                        </div>
                                         <div class="form-group">
                                            <input type="email" class="form-control" placeholder="Email" required="required" name="email">
                                        </div>
                                           <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Phone Number" required="required" name="number">
                                        </div>
                                        <div class="form-group">
                                            <select id="authoirty" name="role">
                                                <option value="admin">Admin</option>
                                                <option value="employee">Employee</option>
                                                <option value="accounts">Accounts</option>
                                           </select>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-10">
                                              <div class="checkbox">
                                           <label class="form-label text">
                        <input type="checkbox" name="TermsAccepted" value="true" required="">
                            I have read and I agree to the
                            <a href="" target="_blank">terms of use</a>,
                            <a href="" target="_blank">privacy notice</a>
                            and
                            <a href="" target="_blank">offer details</a>
                    </label>
                                              </div>
                                            </div>
                                          </div>
                                          <div class="form-group">
                                            <div class="col-sm-10">
                                              <button type="submit" class="btn btn-default lgn-btn">Get Started</button>
                                            </div>
                                          </div>
                                       </form>

                                    </div>
                                    

                    </div>                    
            </div>
        </div>


        <script src="js/jquery.min.js"></script>  
        <script src="js/maruti.login.js"></script> 
    </body>

        
        
    </body>
</html>