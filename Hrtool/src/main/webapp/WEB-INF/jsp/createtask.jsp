<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!doctype html>

<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

<title>HR</title>
<!-- Favicon-->
<link  rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
<!-- Morris Chart Css-->
<link rel="stylesheet" href="plugins/morrisjs/morris.css" />
<!-- Colorpicker Css -->
<link rel="stylesheet" href="plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css" />
<!-- Multi Select Css -->
<link rel="stylesheet" href="plugins/multi-select/css/multi-select.css">
<!-- Bootstrap Spinner Css -->
<link rel="stylesheet" href="plugins/jquery-spinner/css/bootstrap-spinner.css">
<!-- Bootstrap Tagsinput Css -->
<link rel="stylesheet" href="plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<!-- Bootstrap Select Css -->
<link rel="stylesheet" href="plugins/bootstrap-select/css/bootstrap-select.css" />
<!-- noUISlider Css -->
<link rel="stylesheet" href="plugins/nouislider/nouislider.min.css" />
<!-- Custom Css -->
<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="css/color_skins.css">
</head>
<style>


body{
    margin-top:20px;
    background:#f5f5f5;
}
/**
 * Panels
 */
/*** General styles ***/

</style>
<body class="theme-black">

<div class="overlay_menu">
    <button class="btn btn-primary btn-icon btn-icon-mini btn-round"><i class="zmdi zmdi-close"></i></button>
    <div class="container">        
        <div class="row clearfix">
            <div class="card">
                <div class="body">
                    <div class="input-group mb-0">
                        <input type="text" class="form-control" placeholder="Search...">
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="zmdi zmdi-search"></i></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card links">
                <div class="body">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-sm-12">
                            <h6>App</h6>
                            <ul class="list-unstyled">
                                <li><a href="">Inbox</a></li>
                                <li><a href="">Chat</a></li>
                                <li><a href="">Calendar</a></li>
                                <li><a href="">File Manager</a></li>
                                <li><a href="contact.html">Contact list</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12">
                            <h6>User Interface (UI)</h6>
                            <ul class="list-unstyled">
                                <li><a href="">UI KIT</a></li>
                                <li><a href="">Tabs</a></li>
                                <li><a href="">Range Sliders</a></li>
                                <li><a href="">Modals</a></li>
                                <li><a href="">Alerts</a></li>
                                <li><a href="">Dialogs</a></li>
                                <li><a href="">Collapse</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12">
                            <h6>Sample Pages</h6>
                            <ul class="list-unstyled">                            
                                <li><a href="">Image Gallery</a></li>
                                <li><a href="">Profile</a></li>
                                <li><a href="">Timeline</a></li>
                                <li><a href="">Pricing</a></li>
                                <li><a href="">Invoices</a></li>
                                <li><a href="">FAQs</a></li>
                                <li><a href="">Search Results</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12">
                            <h6>About</h6>
                            <ul class="list-unstyled">
                                <li><a href="" target="_blank">About</a></li>
                                <li><a href="" target="_blank">Contact Us</a></li>
                                <li><a href="" target="_blank">Admin Templates</a></li>
                                <li><a href="" target="_blank">Services</a></li>
                                <li><a href="" target="_blank">Portfolio</a></li>                            
                            </ul>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12">
        
            </div>
        </div>
    </div>
</div>
<div class="overlay"></div><!-- Overlay For Sidebars -->

<!-- Left Sidebar -->
<aside id="minileftbar" class="minileftbar">
    <ul class="menu_list">
        <li>
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" href="index.html"><img src="images/logo.svg" alt="Alpino"></a>
        </li>
        <li><a href="javascript:void(0);" class="btn_overlay hidden-sm-down"><i class="zmdi zmdi-search"></i></a></li>        
       <!--  <li><a href="javascript:void(0);" class="menu-sm"><i class="zmdi zmdi-swap"></i></a></li>      -->   
        <li class="menuapp-btn"><a href=""><i class="zmdi zmdi-apps"></i></a></li>
        <li class="notifications badgebit">
            <a href="javascript:void(0);">
                <i class="zmdi zmdi-notifications"></i>
                <div class="notify">
                    <span class="heartbit"></span>
                    <span class="point"></span>
                </div>
            </a>
        </li>
        <li class="task badgebit">
            <a href="javascript:void(0);">
                <i class="zmdi zmdi-account-box-phone"></i>
                <div class="notify">
                    <span class="heartbit"></span>
                    <span class="point"></span>
                </div>
            </a>
        </li>
        <li><a href="events.html" title="Events"><i class="zmdi zmdi-calendar"></i></a></li>
        <!-- <li><a href="mail-inbox.html" title="Inbox"><i class="zmdi zmdi-email"></i></a></li> -->
        <li><a href="" title="Contact List"><i class="zmdi zmdi-account-box-phone"></i></a></li>        
        <li><a href=""><i class="zmdi zmdi-comments"></i></a></li>        
        <li><a href="javascript:void(0);" class="fullscreen" data-provide="fullscreen"><i class="zmdi zmdi-fullscreen"></i></a></li>
        <li class="power">
            <a href="javascript:void(0);" class="js-right-sidebar"><i class="zmdi zmdi-settings zmdi-hc-spin"></i></a>            
            <a href="sign-in.html" class="mega-menu"><i class="zmdi zmdi-power"></i></a>
        </li>
    </ul>    
</aside>

<aside class="right_menu">
    <div class="menu-app">
        <div class="slim_scroll">
            <div class="card">
                <div class="header">
                    <h2><strong>App</strong> Menu</h2>
                </div>
                <div class="body">
                    <ul class="list-unstyled menu">
                        <li><a href=""><i class="zmdi zmdi-calendar-note"></i><span>Calendar</span></a></li>
                        <li><a href=""><i class="zmdi zmdi-file-text"></i><span>File Manager</span></a></li>
                        <li><a href=""><i class="zmdi zmdi-blogger"></i><span>Blog</span></a></li>
                        <li><a href="javascript:void(0)"><i class="zmdi zmdi-arrows"></i><span>Notes</span></a></li>
                        <li><a href="javascript:void(0)"><i class="zmdi zmdi-view-column"></i><span>Taskboard</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="notif-menu">
        <div class="slim_scroll">
            <div class="card">
                <div class="header">
                    <h2><strong>Messages</strong></h2>
                </div>
                <div class="body">
                    <ul class="messages list-unstyled">
                        <li>
                            <a href="javascript:void(0);">
                                <div class="media">
                                    <img class="media-object rounded-circle" src="images/xs/avatar1.jpg" alt="">
                                    <div class="media-body">
                                        <span class="name">Alexander <small class="time">35min ago</small></span>
                                        <p class="message">New tasks needs to be done</p>                                        
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <div class="media">
                                    <img class="media-object rounded-circle" src="images/xs/avatar2.jpg" alt="">
                                    <div class="media-body">
                                        <span class="name">Grayson <small class="time">1hr ago</small></span>
                                        <p class="message">New tasks needs to be done</p>                                        
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <div class="media">
                                    <img class="media-object rounded-circle" src="images/xs/avatar3.jpg" alt="">
                                    <div class="media-body">
                                        <span class="name">Sophia <small class="time">31min ago</small></span>
                                        <p class="message">New tasks needs to be done</p>                                        
                                    </div>
                                </div>
                            </a>
                        </li>        
                    </ul>  
                </div>
            </div>
            <div class="card">
                <div class="header">
                    <h2><strong>Notifications</strong></h2>
                </div>
                <div class="body">
                    <ul class="notification list-unstyled">
                        <li>
                            <i class="zmdi zmdi-balance-wallet text-success"></i>
                            <strong>+$30 New sale</strong>
                            <p><a href="javascript:void(0)">Admin Template</a></p>
                            <small class="text-muted">7 min ago</small>
                        </li>
                        <li>
                            <i class="zmdi zmdi-edit text-info"></i>
                            <strong>You Edited file</strong>
                            <p><a href="javascript:void(0)">Docs.doc</a></p>
                            <small class="text-muted">15 min ago</small>
                        </li>
                        <li>
                            <i class="zmdi zmdi-delete text-danger"></i>
                            <strong>Project removed</strong>
                            <p><a href="javascript:void(0)">AdminX BS4</a></p>
                            <small class="text-muted">1 hours ago</small>
                        </li>
                        <li>
                            <i class="zmdi zmdi-account text-success"></i>
                            <strong>New user</strong>
                            <p><a href="javascript:void(0)">UI Designer</a></p>
                            <small class="text-muted">1 hours ago</small>
                        </li>
                        <li>
                            <i class="zmdi zmdi-flag text-warning"></i>
                            <strong>Alpino v1.0.0 is available</strong>
                            <p><a href="javascript:void(0)">Update now</a></p>
                            <small class="text-muted">5 hours ago</small>
                        </li>
                    </ul>  
                </div>
            </div>
        </div>
    </div>
    <div class="task-menu">


               <div class="slim_scroll">
     <div class="card">
        <h2><strong>Employees</strong></h2>
                <div class="header">
                    <h2><strong>Main</strong></h2>
                </div>
                <div class="body">
                    <ul class="notification list-unstyled">
                        <li>
                             <i class="zmdi zmdi-balance-wallet text-success"></i>                           
                            <p><a href="">Overwiew</a></p>
                            
                        </li>

                        <li>
                            <i class="zmdi zmdi-account text-success"></i>                           
                            <p><a href="">Employee Directory</a></p>
                            
                        </li>

                    </ul>  
                </div>
            </div>
            <div class="card">
                <div class="header">
                    <h2><strong>Information</strong></h2>
                </div>
                <div class="body">
                    <ul class="notification list-unstyled">
                           <li>
                             <i class="zmdi zmdi-balance-wallet text-success"></i>                           
                            <p><a href="">Employee Profile</a></p>
                            
                        </li>
                           <li>
                             <i class="zmdi zmdi-balance-wallet text-success"></i>                           
                            <p><a href="">Bank/PF/ESI</a></p>
                            
                        </li>
                           <li>
                             <i class="zmdi zmdi-balance-wallet text-success"></i>                           
                            <p><a href="">Position History</a></p>
                            
                        </li>
                           <li>
                             <i class="zmdi zmdi-balance-wallet text-success"></i>                           
                            <p><a href="">Employee Documents</a></p>
                            
                        </li>
                           <li>
                             <i class="zmdi zmdi-balance-wallet text-success"></i>                           
                            <p><a href="">Employee Salary</a></p>
                            
                        </li>

                 
                    </ul>  
                </div>
            </div>
                        <div class="card">
                <div class="header">
                    <h2><strong>Setup</strong></h2>
                </div>
                <div class="body">
                    <ul class="notification list-unstyled">
                        <li>
                             <i class="zmdi zmdi-balance-wallet text-success"></i>                           
                            <p><a href="">HR Forms</a></p>
                            
                        </li>
                                             <li>
                             <i class="zmdi zmdi-balance-wallet text-success"></i>                           
                            <p><a href="">Letter Template</a></p>
                            
                        </li>
                    </ul>  
                </div>
            </div>
        </div>
        <div class="slim_scroll">
            <div class="card tasks">
                <div class="header">
                    <h2><strong>Project</strong> Status</h2>
                </div>
                
                                <ul>
                                        <li class="mid-level open">
                                            <a href="/ngapp/employee/main">
                                                <span class="menu_text">Main</span>
                                                <i class="icon-chevron-down"></i>
                                            </a>
                                                <ul>
                                                            <li class="leaf-level open">
                                                                <a href="/ngapp/employee/main/overview">
                                                                    <span class="menu_text">Overview</span>
                                                                </a>
                                                            </li>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/main/emp-directory">
                                                                    <span class="menu_text">Employee Directory</span>
                                                                </a>
                                                            </li>

                                                </ul>
                                        </li>
                                        <li class="mid-level">
                                            <a href="/ngapp/employee/info">
                                                <span class="menu_text">Information</span>
                                                <i class="icon-chevron-down"></i>
                                            </a>
                                                <ul>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/info/profile">
                                                                    <span class="menu_text">Employee Profile</span>
                                                                </a>
                                                            </li>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/info/accounts">
                                                                    <span class="menu_text">Bank/PF/ESI</span>
                                                                </a>
                                                            </li>

                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/info/assets">
                                                                    <span class="menu_text">Assets</span>
                                                                </a>
                                                            </li>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/info/career_history">
                                                                    <span class="menu_text">Position History</span>
                                                                </a>
                                                            </li>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/info/previousemployeement">
                                                                    <span class="menu_text">Previous Employment</span>
                                                                </a>
                                                            </li>

                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/info/employee-docs">
                                                                    <span class="menu_text">Employee Documents</span>
                                                                </a>
                                                            </li>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/info/employee-contract">
                                                                    <span class="menu_text">Employee Contracts</span>
                                                                </a>
                                                            </li>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/info/employeesalary">
                                                                    <span class="menu_text">Employee Salary</span>
                                                                </a>
                                                            </li>
                                                </ul>
                                        </li>

                                        <li class="mid-level">
                                            <a href="/ngapp/employee/setup">
                                                <span class="menu_text">Setup</span>
                                                <i class="icon-chevron-down"></i>
                                            </a>
                                                <ul>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/setup/letter-template">
                                                                    <span class="menu_text">Letter Template</span>
                                                                </a>
                                                            </li>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/setup/hr-forms">
                                                                    <span class="menu_text">HR Forms</span>
                                                                </a>
                                                            </li>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/setup/employeesegment">
                                                                    <span class="menu_text">Employee Segment</span>
                                                                </a>
                                                            </li>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/setup/v2/qb/employee-filter">
                                                                    <span class="menu_text">Employee Filter</span>
                                                                </a>
                                                            </li>
                                                </ul>
                                        </li>

                            </ul>
            </div>
        </div>



    </div>
    <div id="rightsidebar" class="right-sidebar">
        <ul class="nav nav-tabs">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#setting">Setting</a></li>        
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#activity">Activity</a></li>
        </ul>
        <div class="tab-content slim_scroll">
            <div class="tab-pane slideRight active" id="setting">
                <div class="card">
                    <div class="header">
                        <h2><strong>Colors</strong> Skins</h2>
                    </div>
                    <div class="body">
                        <ul class="choose-skin list-unstyled m-b-0">
                            <li data-theme="black" class="active">
                                <div class="black"></div>
                            </li>
                            <li data-theme="purple">
                                <div class="purple"></div>
                            </li>                   
                            <li data-theme="blue">
                                <div class="blue"></div>
                            </li>
                            <li data-theme="cyan">
                                <div class="cyan"></div>                    
                            </li>
                            <li data-theme="green">
                                <div class="green"></div>
                            </li>
                            <li data-theme="orange">
                                <div class="orange"></div>
                            </li>
                            <li data-theme="blush">
                                <div class="blush"></div>                    
                            </li>
                        </ul>
                    </div>
                </div>                
                <div class="card">
                    <div class="header">
                        <h2><strong>General</strong> Settings</h2>
                    </div>
                    <div class="body">
                        <ul class="setting-list list-unstyled m-b-0">
                            <li>
                                <div class="checkbox">
                                    <input id="checkbox1" type="checkbox">
                                    <label for="checkbox1">Report Panel Usage</label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox">
                                    <input id="checkbox2" type="checkbox" checked="">
                                    <label for="checkbox2">Email Redirect</label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox">
                                    <input id="checkbox3" type="checkbox">
                                    <label for="checkbox3">Notifications</label>
                                </div>                        
                            </li>
                            <li>
                                <div class="checkbox">
                                    <input id="checkbox4" type="checkbox">
                                    <label for="checkbox4">Auto Updates</label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox">
                                    <input id="checkbox5" type="checkbox" checked="">
                                    <label for="checkbox5">Offline</label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox m-b-0">
                                    <input id="checkbox6" type="checkbox">
                                    <label for="checkbox6">Location Permission</label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
              
            </div>
      
        </div>
    </div>

</aside>
<!-- Main Content -->

<section class="content">   
    <div class="container-fluid">
       <div class="ks-column ks-page">
        <div class="ks-page-header">
            <section class="ks-title">
                <h3>Payroll settings</h3>
            </section>
        </div>
        <div class="ks-page-content">
            <div class="ks-page-content-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xl-3">
                            <div class="card ks-crm-contact-user-column">
                                <div class="ks-crm-contact-user-column-main-info">
                                    <section>
                                        <span class="ks-crm-contact-user-send-message">
                                            <span class="ks-icon la la-envelope"></span>
                                        </span>
                                        <img src="images/logo.png" width="100" height="100" class="ks-crm-contact-user-avatar rounded-circle">
                                        <span class="ks-crm-contact-user-controls">
                                            <div class="dropdown ks-control">
                                                <a class="btn btn-link ks-no-text" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <span class="ks-icon la la-ellipsis-h"></span>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1" x-placement="bottom-end" style="position: absolute; transform: translate3d(58px, 38px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                    <a class="dropdown-item" href="#">Edit</a>
                                                    <a class="dropdown-item" href="#">Delete</a>
                                                </div>
                                            </div>
                                        </span>
                                    </section>
                                    <section>
                                        <div class="ks-crm-contact-user-name">Adriot CadenaTech</div>
                                        <div class="ks-crm-contact-user-location">Hyderabad, India</div>
                                        <div class="ks-crm-contact-user-rating">
                                            <i class="la la-star ks-star" aria-hidden="true"></i>
                                            <i class="la la-star ks-star" aria-hidden="true"></i>
                                            <i class="la la-star ks-star" aria-hidden="true"></i>
                                            <i class="la la-star ks-star" aria-hidden="true"></i>
                                            <i class="la la-star-half-o ks-star" aria-hidden="true"></i>
                                        </div>
                                    </section>
                                </div>
                                <div class="ks-crm-contact-user-column-custom-info">
                                    <h6 class="ks-custom-info-header">Contact Information</h6>
                                    <div class="ks-custom-info-item">
                                        <div class="ks-custom-info-item-name">Email</div>
                                        <div class="ks-custom-info-item-content">adriott@gmail.com</div>
                                    </div>
                                    <div class="ks-custom-info-item">
                                        <div class="ks-custom-info-item-name">Phone Number</div>
                                        <div class="ks-custom-info-item-content">657-662-0825</div>
                                    </div>
                                    <div class="ks-custom-info-item">
                                        <div class="ks-custom-info-item-name">Address</div>
                                        <div class="ks-custom-info-item-content">BURGEON IT SERVICES PVT LTD
Plot #:253, 2nd Floor, Srinivas Nagar, Kapra, Hyderabad, TS, INDIA- 500062</div>
                                    </div>
                                </div>
                                <div class="ks-crm-contact-user-column-custom-info">
                                    <h6 class="ks-custom-info-header">Custom Information</h6>
                                    <div class="ks-custom-info-item">
                                        <div class="ks-custom-info-item-name">Facebook</div>
                                        <div class="ks-custom-info-item-content"><a href="#">@adriott</a></div>
                                    </div>
                                    <div class="ks-custom-info-item">
                                        <div class="ks-custom-info-item-name">Personal Phone Number</div>
                                        <div class="ks-custom-info-item-content">480-947-8193</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-9">
                            <div class="ks-crm-contact-user-tabs-column">
                                <div class="ks-tabs-container ks-tabs-default ks-tabs-no-separator">
                                    <ul class="nav ks-nav-tabs">
                                        <li class="nav-item">
                                            <a class="nav-link active show" href="#" data-toggle="tab" data-target="#crm-tab-activity" aria-expanded="true">Pay items</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#" data-toggle="tab" data-target="#crm-tab-notes" aria-expanded="false">Calendars</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#" data-toggle="tab" data-target="#crm-tab-events" aria-expanded="false">Cycles</a>
                                        </li>

                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane ks-scrollable active show jspScrollable" id="crm-tab-activity" role="tabpanel" aria-expanded="true" data-height="700" style="height: 586px; overflow: hidden; padding: 0px;">
                                          <div class="container-fluid">
  
            <div class="col-lg-12 col-md-12 col-sm-12">

<div class="row">
    <div class="col-lg-3 notemp">
        <div class="card border-primary mb-3" style="max-width: 18rem;">
  <div class="card-header">templates</div>

  <div class="card-body text-primary">
        <h5 class="card-title">No templates</h5>
    <button class="add btn btn-primary font-weight-bold" id="createtemp">Create template</button>

  </div>
</div>

</div>
</div>
                
                <h4>${msg}</h4>


                <form name="task" action="createtask1" method="get">
                    
                    <div class="form-group showtemp">

                      <label for="usr">To:</label>
                      <input type="text" class="form-control" id="uname" name="uname">

                      <label for="usr">Name:</label>
                      <input type="text" class="form-control" id="tname" name="tname">
                      <button class="createbtn btn btn-primary font-weight-bold" type="submit">Create</button>

                    </div>
                </form>
                
            <div class="panel-body temp">
                <div class="row">
       <h6 class="tem-name" style="font-size: 15pt; color: #888; text-align: center;">Template</h6>
          <div class="table">

                    <div class="add-items d-flex">
                                     <button class="add btn btn-primary font-weight-bold" id="add-task" data-toggle="modal" data-target="#exampleModal">Add</button>
                                    </div>
            <table class="todo-list">
            <tr>
                <th>Label</th>
                <th>Category</th>
                <th>Rate</th>
                <th><div class='form-check'><label class='form-check-label'><input class='checkbox' type='checkbox'/></th>
                <th></th>
            </tr>

        </table>
  <button class="btn btn-primary nextBtn pull-right todo-list-add-btn" type="button">Save</button>
          </div>
         
        </div><!--panel body closed-->
        </div><!--panel closed-->
        
          
            
                </div>
            </div>

        <!-- #END# Masked Input -->        
        <!-- Multi Select -->

        <!-- #END# Multi Select -->

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add pay item</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
                  <div class="form-group form-float">
                    <label>Label Name</label>
                              <input type="text" class="form-control todo-list-input inputme"  placeholder="What do you need to do today?">
                            </div>
    
                                    <label>Category</label>
                                 <div class="form-group form-float">



                                    <select class="type">
                                            <option>Earnings</option>
                                            <option>Deductions</option>
                                            <option>Reimbursements</option>
                                        </select>

                                    </div>

                                 <label>Rate type</label>
                                 <div class="form-group form-float">
                              <select name="type" class="amt" id="type" style="margin-left:57px; width:153px;">
                                    <option name="l_letter" value="percentage">Percentage(%)</option>
                                    <option name="parcel" value="amount">Amount</option>
                                </select>
                            </div>
                            <div class="row" id="row_dim1">
                                <input type="text" name="height" class="dimension form-control" placeholder="Enter percentage">
                            </div>

                            <div class="row" id="row_dim">
                                <input type="text" name="height" class="dimension form-control" placeholder="enter ammount">
                            </div> 
                              <div class="form-check">
                              <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                              <label class="form-check-label" for="defaultCheck1">
                                include in Gross
                              </label>
                            </div>
                        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary todo-list-add-btn">Add</button>
      </div>
    </div>
  </div>
</div>

                                        </div><!--frst tab closed-->
      
                                 
                                        <div class="tab-pane" id="crm-tab-notes" role="tabpanel" aria-expanded="false" data-height="735" style="height: 775px;">
                                            Content 2
                                        </div>
                                        <div class="tab-pane" id="crm-tab-events" role="tabpanel" aria-expanded="false" data-height="735" style="height: 775px;">
                                            Content 3
                                        </div>
                                        <div class="tab-pane" id="crm-tab-tasks" role="tabpanel" aria-expanded="false" data-height="735" style="height: 775px;">
                                            Content 3
                                        </div>
                                        <div class="tab-pane" id="crm-tab-deals" role="tabpanel" aria-expanded="false" data-height="735" style="height: 775px;">
                                            Content 3
                                        </div>
                                        <div class="tab-pane" id="crm-tab-campaigns" role="tabpanel" aria-expanded="false" data-height="735" style="height: 775px;">
                                            Content 3
                                        </div>
                                        <div class="tab-pane" id="crm-tab-documents" role="tabpanel" aria-expanded="false" data-height="735" style="height: 775px;">
                                            Content 3
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!--row end-->

                </div>
            </div>
        </div>
    </div>
 
    </div>
</section>
<!-- Jquery Core Js --> 
<script src="bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
<script src="bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 

<script src="plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> <!-- Bootstrap Colorpicker Js --> 
<script src="plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script> <!-- Input Mask Plugin Js --> 
<script src="plugins/multi-select/js/jquery.multi-select.js"></script> <!-- Multi Select Plugin Js --> 
<script src="plugins/jquery-spinner/js/jquery.spinner.js"></script> <!-- Jquery Spinner Plugin Js --> 
<script src="plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script> <!-- Bootstrap Tags Input Plugin Js --> 
<script src="plugins/nouislider/nouislider.js"></script> <!-- noUISlider Plugin Js --> 

<script src="bundles/mainscripts.bundle.js"></script><!-- Custom Js --> 
<script src="js/pages/forms/advanced-form-elements.js"></script> 

<script>

 $(function () {
     $('#row_dim').hide();
       $('#row_dim1').hide();
     $('#type').change(function () {
         $('#row_dim').hide();
           
         if (this.options[this.selectedIndex].value == 'percentage') {
             $('#row_dim1').show();
               $('#row_dim').hide();
         }
             if (this.options[this.selectedIndex].value == 'amount') {
             $('#row_dim').show();
                $('#row_dim1').hide();
         }
     });
 });


    var todoListItem = $('.todo-list');
 var todoListInput = $('.todo-list-input');
 var todoListInput = $('.inputme');

 $('#exampleModal').on('click', '.btn-primary', function(){
   event.preventDefault();
  var item = $('.inputme').val();

    var selectedCountry = $(".type option:selected").val();
    var selectedamt = $(".amt option:selected").val();

    todoListItem.append("<tr><td>"+ item +"</td><td>" + selectedCountry + "</td><td>"+ selectedamt +"</td><td><div class='form-check'><label class='form-check-label'><input class='checkbox' type='checkbox'/></td><td> <button id='but1' class='botaoadd'> x </button> </td></tr>");
         todoListInput.val("");
    $('#exampleModal').modal('hide');

});

  $("body").on("click","#but1",function(){ 

  $(this).closest('tr').remove();

  return false;
});


  $("#createtemp").click(function(){
    $(".showtemp").toggle(300);
    $(".notemp").hide();
});

    /*$(".createbtn").click(function(){
    $(".temp").toggle(300);
        $(".showtemp").hide();
          $("#createtemp").hide();
        

});*/
</script>
</script>
</body>

</html>