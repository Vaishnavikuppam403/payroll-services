<%-- 
    Document   : resetpassword
    Created on : 9 Aug, 2018, 11:11:39 AM
    Author     : Burgeonits
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>HR Tool</title><meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="css/bootstrap.min.css" />
<!--    <link rel="stylesheet" href="css/bootstrap-responsive.min.css" /> -->
        <link rel="stylesheet" href="css/maruti-login.css" />
    </head>
    <body style="background-color: #F6F9FC;">
        <style>
   .x-header {
    background-image: none;
}
.x-header {
    background: #00a3d3;
}
.x-header {
    color: #fff;
    display: block;
    background-color: #0590c3;
 
    background: -webkit-gradient(linear,50% 0,50% 100%,color-stop(0%,#1696c5),color-stop(100%,#048bbe));
    background: -moz-linear-gradient(#1696c5,#048bbe);
    background: -webkit-linear-gradient(#1696c5,#048bbe);
    background: linear-gradient(#1696c5,#048bbe);
}

        </style>
        <div class="x-header2">
            <div class="inner">
                    <h1><span>HR Tool</span></h1>
            </div>
        </div>
        <div class="container login-container">

            <div class="col-sm-12 password-box">
                <div class="create-password">
                     <h2 class="mobile-heading">Create a password</h2>
                    <p class="password-help-text">It'll need to contain at least 8 characters, including one or more numbers</p>
                   
                    <form action="reset1" method="post">
<!--                         <div class="form-group">
                           <input type="text" class="form-control" placeholder="enter userid" required="required" name="username"/>
                           </div>-->
                          <div class="form-group">
                           <input type="password" class="form-control" placeholder="enter password" required="required" name="pass"/>
                           </div>
                           
                          <div class="form-group">
                           <input type="password" class="form-control" placeholder="confirm password" required="required" name="confirmpass"/>
                           </div>
                           <div class="text-center">
        
                             <input type="submit" class="btn btn-primary lgn-btn3" value="Activate your account"/>                                  
                                            </div>
                        </form>
                </div>
                 
             </div> 


 <!--          <div id="signup" class="content" style="display: block;">
            <div class="left x-boxed">
                
                
                    <h2 class="mobile-heading">Create a password</h2>
                    <p class="password-help-text">It'll need to contain at least 8 characters, including one or more numbers</p>   
                

                <input id="SignUpRequestLink" name="SignUpRequestLink" type="hidden" value="866b24a8-88dc-4a3d-915e-b45c4a05f5ac">
                

                <input id="EmailAddress" name="EmailAddress" type="hidden" value="vivek.ecomsoft@gmail.com">
                <input id="FirstName" name="FirstName" type="hidden" value="viveknadh">
                <input id="LastName" name="LastName" type="hidden" value="nadh">
                
              
                <input class="remove-default-input-styles" id="Password" name="Password" placeholder="Password" type="password">

         
             
                

                <div id="buttonContainer" class="buttons">
                    <input id="submitButton" type="submit" class="x-btn green main remove-default-input-styles" value="Activate your account">
                                        <span></span>
                </div>
            </div>
 
        </div> -->
        </div>
    


        <script src="js/jquery.min.js"></script>  
        <script src="js/maruti.login.js"></script> 
    </body>
</html>
