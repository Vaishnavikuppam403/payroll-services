<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from www.urbanui.com/victory/pages/layout/compact-menu.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 04 Sep 2018 01:04:10 GMT -->
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>HR</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>

  <link rel="stylesheet" href="node_modules/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="node_modules/simple-line-icons/css/simple-line-icons.css">
  <link rel="stylesheet" href="node_modules/flag-icon-css/css/flag-icon.min.css">
  <link rel="stylesheet" href="node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.min.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <link rel="stylesheet" href="node_modules/font-awesome/css/font-awesome.min.css" />
  <link rel="stylesheet" href="node_modules/jquery-bar-rating/dist/themes/fontawesome-stars.css">
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/build.css">
    <link rel="stylesheet" href="node_modules/select2/dist/css/select2.min.css" />

  <link rel="stylesheet" href="node_modules/select2-bootstrap-theme/dist/select2-bootstrap.min.css" />
    <link rel="stylesheet" href="node_modules/jquery-tags-input/dist/jquery.tagsinput.min.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="" />
    <style>

  .select2-container--default .select2-selection--multiple .select2-selection__rendered {
    box-sizing: border-box;
    list-style: none;
    margin: 0;
    padding: 0 5px;
    width: 100%;
    height: 50px;
    border-radius: 0;
}
label{
  outline: 0;
}
.form-control, .asColorPicker-input, .dataTables_wrapper select, .jsgrid .jsgrid-table .jsgrid-filter-row input[type=text], .jsgrid .jsgrid-table .jsgrid-filter-row select, .jsgrid .jsgrid-table .jsgrid-filter-row input[type=number], .select2-container--default .select2-selection--single, .select2-container--default .select2-selection--single .select2-search__field, .typeahead, .tt-query, .tt-hint {
    border: 1px solid #989494;
    font-family: "roboto-regular", sans-serif;
    font-size: 1.45rem;
    padding: 1rem 0.75rem;
    line-height: 14px;
}
.select2-container {
    box-sizing: border-box;
    display: inline-block;
    margin: 0;
    position: relative;
    vertical-align: middle;
    margin-top: 10px;
}
#submitme:hover{

color: #4c4b4b!important;
}
#hoverme:hover{
  background: #8ba6d5!important;
    font-size: 1.25rem;
    letter-spacing: 2px;
    border-radius: 31px;
    margin-top: 14px;
    margin-right: 20px;
    box-shadow: 0 1px 6px rgba(32, 33, 36, 0.28);
    border: 1px solid #e6e6e6;
    color: #ffff !important;}

.header-dropdown .checkbox {
    padding-left: 0px !important;
    top: -5px !important;
    position: absolute !important;
    left: 32px !important;
}
.header-dropdown{
  list-style: none;
}
  .dropdown-toggle::after {
    display: inline-block;
    width: 0;
    height: 0;
    margin-left: 0.255em;
    vertical-align: 0.255em;
    content: none !important;
    border-top: 0.3em solid;
    border-right: 0.3em solid transparent;
    border-bottom: 0;
    border-left: 0.3em solid transparent;
}
  .initials {
  position: relative;
  top: 25px; /* 25% of parent */
  font-size: 50px; /* 50% of parent */
  line-height: 50px; /* 50% of parent */
  color: #fff;
  font-family: "Courier New", monospace;
  font-weight: bold;
}
  .avatar-circle {
  width: 100px;
  height: 100px;
  background-color: brand-special;
  text-align: center;
  border-radius: 50%;
  -webkit-border-radius: 50%;
  -moz-border-radius: 50%;
  background: red;
}
.dropdown-menu{
  position: absolute;
    top: 0px;
    left: 0px;
    transform: translate3d(84px, 22px, 0px) !important;
    will-change: transform;
}
  .letter{
    width: 60px;
    height: 60px;
    background: #03a9f3;
    border-radius: 50%;
    color: #fff;
    text-align: center;
    line-height: 60px;
    font-size: 2rem;
    margin: 0 auto;
  }
  .btn-circle {
    width: 30px;
    height: 30px;
    text-align: center;
    padding: 6px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 15px;
}
.stepwizard-step p {
    margin-top: 0px;
    color:#887d7d;
}
.stepwizard-row {
    display: table-row;
}
.stepwizard {
       display: table;
    width: 67%;
    position: relative;
    margin: 0 auto;
}
.stepwizard-step button[disabled] {
    /*opacity: 1 !important;
    filter: alpha(opacity=100) !important;*/
}
.stepwizard .btn.disabled, .stepwizard .btn[disabled], .stepwizard fieldset[disabled] .btn {
    opacity:1 !important;
    color:#bbb;}
.stepwizard-row:before {
    top: 20px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 64%;
    height: 2px;
    background-color: #21282c;
    z-index: 0;
    left: 155px;
}
.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}
.stepwizard .btn.disabled, .stepwizard .btn[disabled], .stepwizard fieldset[disabled] .btn {
    opacity: 1 !important;
    color: #bbb;
}
.btn.disabled, .btn.disabled:hover, .btn.disabled:focus, .btn.disabled.focus, .btn.disabled:active, .btn.disabled.active, .btn:disabled, .btn:disabled:hover, .btn:disabled:focus, .btn:disabled.focus, .btn:disabled:active, .btn:disabled.active, .btn[disabled], .btn[disabled]:hover, .btn[disabled]:focus, .btn[disabled].focus, .btn[disabled]:active, .btn[disabled].active, fieldset[disabled] .btn, fieldset[disabled] .btn:hover, fieldset[disabled] .btn:focus, fieldset[disabled] .btn.focus, fieldset[disabled] .btn:active, fieldset[disabled] .btn.active, .navbar .navbar-nav>a.btn.disabled, .navbar .navbar-nav>a.btn.disabled:hover, .navbar .navbar-nav>a.btn.disabled:focus, .navbar .navbar-nav>a.btn.disabled.focus, .navbar .navbar-nav>a.btn.disabled:active, .navbar .navbar-nav>a.btn.disabled.active, .navbar .navbar-nav>a.btn:disabled, .navbar .navbar-nav>a.btn:disabled:hover, .navbar .navbar-nav>a.btn:disabled:focus, .navbar .navbar-nav>a.btn:disabled.focus, .navbar .navbar-nav>a.btn:disabled:active, .navbar .navbar-nav>a.btn:disabled.active, .navbar .navbar-nav>a.btn[disabled], .navbar .navbar-nav>a.btn[disabled]:hover, .navbar .navbar-nav>a.btn[disabled]:focus, .navbar .navbar-nav>a.btn[disabled].focus, .navbar .navbar-nav>a.btn[disabled]:active, .navbar .navbar-nav>a.btn[disabled].active, fieldset[disabled] .navbar .navbar-nav>a.btn, fieldset[disabled] .navbar .navbar-nav>a.btn:hover, fieldset[disabled] .navbar .navbar-nav>a.btn:focus, fieldset[disabled] .navbar .navbar-nav>a.btn.focus, fieldset[disabled] .navbar .navbar-nav>a.btn:active, fieldset[disabled] .navbar .navbar-nav>a.btn.active {
      background-color: #e9ecef;
    border-color: #727373;
    color: #ffff;
}


.btn-success.disabled, .btn-success.disabled:hover, .btn-success.disabled:focus, .btn-success.disabled.focus, .btn-success.disabled:active, .btn-success.disabled.active, .btn-success:disabled, .btn-success:disabled:hover, .btn-success:disabled:focus, .btn-success:disabled.focus, .btn-success:disabled:active, .btn-success:disabled.active, .btn-success[disabled], .btn-success[disabled]:hover, .btn-success[disabled]:focus, .btn-success[disabled].focus, .btn-success[disabled]:active, .btn-success[disabled].active, fieldset[disabled] .btn-success, fieldset[disabled] .btn-success:hover, fieldset[disabled] .btn-success:focus, fieldset[disabled] .btn-success.focus, fieldset[disabled] .btn-success:active, fieldset[disabled] .btn-success.active {
  background-color: #434a54;
    border-color: #495057;

}
.wrapper{
  text-align:center;
  margin:50px auto;
}

.tabs{
  margin-top:50px;
  font-size:15px;
  padding:0px;
  list-style:none;
  background:#fff;
  box-shadow:0px 5px 20px rgba(0,0,0,0.1);
  display:inline-block;
  border-radius:50px;
  position:relative;
}

.tabs a{
  text-decoration:none;
  color: #777;
  text-transform:uppercase;
  padding:10px 20px;
  display:inline-block;
  position:relative;
  z-index:1;
  transition-duration:0.6s;
}

.tabs a.active{
  color:#fff;
}

.tabs a i{
  margin-right:5px;
}

.tabs .selector{
  height:100%;
  display:inline-block;
  position:absolute;
  left:0px;
  top:0px;
  z-index:1;
  border-radius:50px;
  transition-duration:0.6s;
  transition-timing-function: cubic-bezier(0.68, -0.55, 0.265, 1.55);
  
  background: #05abe0;
  background: -moz-linear-gradient(45deg, #05abe0 0%, #8200f4 100%);
  background: -webkit-linear-gradient(45deg, #05abe0 0%,#8200f4 100%);
  background: linear-gradient(45deg, #05abe0 0%,#8200f4 100%);
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#05abe0', endColorstr='#8200f4',GradientType=1 );
}
  .tab-content-basic .card{
    border-radius: 4px;
    background: #fff;
    box-shadow: 0 6px 10px rgba(0,0,0,.08), 0 0 6px rgba(0,0,0,.05);
    transition: .3s transform cubic-bezier(.155,1.105,.295,1.12),.3s box-shadow,.3s -webkit-transform cubic-bezier(.155,1.105,.295,1.12);
    padding: 10px 10px 10px 10px;
    cursor: pointer;
}

 .tab-content-basic .card:hover{
     transform: scale(1.05);
  box-shadow: 0 10px 20px rgba(0,0,0,.12), 0 4px 8px rgba(0,0,0,.06);
}

 .tab-content-basic .card h3{
  font-weight: 600;
}

 .tab-content-basic .card img{
  position: absolute;
  top: 20px;
  right: 15px;
  max-height: 120px;
}

 .tab-content-basic .card-1{
  background-image: url(https://ionicframework.com/img/getting-started/ionic-native-card.png);
      background-repeat: no-repeat;
    background-position: right;
    background-size: 80px;
}

 .tab-content-basic .card-2{
   background-image: url(https://ionicframework.com/img/getting-started/components-card.png);
      background-repeat: no-repeat;
    background-position: right;
    background-size: 80px;
}

 .tab-content-basic .card-3{
   background-image: url(https://ionicframework.com/img/getting-started/theming-card.png);
      background-repeat: no-repeat;
    background-position: right;
    background-size: 80px;
}

@media(max-width: 990px){
   .tab-content-basic .card{
    margin: 20px;
  }
} 

.nav>li>a {
    position: relative;
    display: block;
    padding: 10px 15px;
}

.progress {
    border-radius: 3px;
    height: 16px;
}
.nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {
    color: #fff;
    background-color: #337ab7;
}
.nav-pills>li>a {
    border-radius: 4px;
}
.nav>li>a {
    position: relative;
    display: block;
    padding: 10px 15px;
}
.form-control, .asColorPicker-input, .dataTables_wrapper select, .jsgrid .jsgrid-table .jsgrid-filter-row input[type=text], .jsgrid .jsgrid-table .jsgrid-filter-row select, .jsgrid .jsgrid-table .jsgrid-filter-row input[type=number], .select2-container--default .select2-selection--single, .select2-container--default .select2-selection--single .select2-search__field, .typeahead, .tt-query, .tt-hint {
    border: 1px solid #aaaaaa;
    font-family: "roboto-regular", sans-serif;
    font-size: 1rem;
    padding: 0.7rem 0.75rem;
    line-height: 14px;
        border-radius: 20px !important;
        letter-spacing: 1px;
}
.select2-selection__rendered{
  letter-spacing: 2px;
}
.form-group label {
  margin-bottom: 0;
    line-height: 1.5;
    font-size: 1.3rem;
   font-family: Microsoft JhengHei UI light;
    font-weight: 100!important;
    color: #6b6565;
}

.modal-content {
    position: relative;
    display: flex;
    flex-direction: column;
    width: 60%;
    pointer-events: auto;
    background-color: #F6F8FA;
    background-clip: padding-box;
    border: 1px solid #f3f3f3;
    border-radius: 0.3rem;
    outline: 0;
    margin: auto;
}
  </style>
</head>
<body class="sidebar-mini">
  <div class="container-scroller">
    <!-- partial:../../partials/_navbar.html -->
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
        <a class="navbar-brand brand-logo" href="">LOGO</a>
        <a class="navbar-brand brand-logo-mini" href=""><img src="images/logo-mini.svg" alt="logo"/></a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">
        <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
          <span class="icon-menu"></span>
        </button>
        <ul class="navbar-nav">
          <li class="nav-item dropdown d-none d-lg-flex">
           <a class="nav-link dropdown-toggle nav-btn" id="actionDropdown" href="#" data-toggle="dropdown" style="
    color: black;   border: 1px solid #e9ecef;    border-radius: 6px;">
             <span class="btn" style=" color: black;">+ Create new</span>
            </a>
            <div class="dropdown-menu navbar-dropdown dropdown-left" aria-labelledby="actionDropdown">
              <a class="dropdown-item" href="#">
                <i class="icon-user text-primary"></i>
                User Account
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">
                <i class="icon-user-following text-warning"></i>
                Admin User
              </a>

            </div>
          </li>
        </ul>
        <ul class="navbar-nav navbar-nav-right">
          <li class="nav-item dropdown d-none d-lg-flex">
            <a class="nav-link dropdown-toggle" id="languageDropdown" href="#" data-toggle="dropdown">
              <i class="flag-icon flag-icon-gb"></i>
              English
            </a>
            <div class="dropdown-menu navbar-dropdown" aria-labelledby="languageDropdown">
              <a class="dropdown-item font-weight-medium" href="#">
                <i class="flag-icon flag-icon-fr"></i>
                French
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item font-weight-medium" href="#">
                <i class="flag-icon flag-icon-es"></i>
                Espanol
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item font-weight-medium" href="#">
                <i class="flag-icon flag-icon-lt"></i>
                Latin
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item font-weight-medium" href="#">
                <i class="flag-icon flag-icon-ae"></i>
                Arabic
              </a>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
              <i class="icon-bell mx-0"></i>
              <span class="count"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
              <a class="dropdown-item">
                <p class="mb-0 font-weight-normal float-left">You have 4 new notifications
                </p>
                <span class="badge badge-pill badge-warning float-right">View all</span>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-success">
                    <i class="icon-info mx-0"></i>
                  </div>
                </div>
                <div class="preview-item-content">
                  <h6 class="preview-subject font-weight-medium">Application Error</h6>
                  <p class="font-weight-light small-text">
                    Just now
                  </p>
                </div>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-warning">
                    <i class="icon-speech mx-0"></i>
                  </div>
                </div>
                <div class="preview-item-content">
                  <h6 class="preview-subject font-weight-medium">Settings</h6>
                  <p class="font-weight-light small-text">
                    Private message
                  </p>
                </div>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-info">
                    <i class="icon-envelope mx-0"></i>
                  </div>
                </div>
                <div class="preview-item-content">
                  <h6 class="preview-subject font-weight-medium">New user registration</h6>
                  <p class="font-weight-light small-text">
                    2 days ago
                  </p>
                </div>
              </a>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link count-indicator dropdown-toggle" id="messageDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <i class="icon-envelope mx-0"></i>
              <span class="count"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="messageDropdown">
              <div class="dropdown-item">
                <p class="mb-0 font-weight-normal float-left">You have 7 unread mails
                </p>
                <span class="badge badge-info badge-pill float-right">View all</span>
              </div>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                    <img src="images/faces/face4.jpg" alt="image" class="profile-pic">
                </div>
                <div class="preview-item-content flex-grow">
                  <h6 class="preview-subject ellipsis font-weight-medium">David Grey
                    <span class="float-right font-weight-light small-text">1 Minutes ago</span>
                  </h6>
                  <p class="font-weight-light small-text">
                    The meeting is cancelled
                  </p>
                </div>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                    <img src="images/faces/face2.jpg" alt="image" class="profile-pic">
                </div>
                <div class="preview-item-content flex-grow">
                  <h6 class="preview-subject ellipsis font-weight-medium">Tim Cook
                    <span class="float-right font-weight-light small-text">15 Minutes ago</span>
                  </h6>
                  <p class="font-weight-light small-text">
                    New product launch
                  </p>
                </div>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                    <img src="images/faces/face3.jpg" alt="image" class="profile-pic">
                </div>
                <div class="preview-item-content flex-grow">
                  <h6 class="preview-subject ellipsis font-weight-medium"> Johnson
                    <span class="float-right font-weight-light small-text">18 Minutes ago</span>
                  </h6>
                  <p class="font-weight-light small-text">
                    Upcoming board meeting
                  </p>
                </div>
              </a>
            </div>
          </li>
          <li class="nav-item nav-settings d-none d-lg-block">
            <a class="nav-link" href="#">
              <i class="icon-grid"></i>
            </a>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="icon-menu"></span>
        </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <div class="row row-offcanvas row-offcanvas-right">

        <div id="right-sidebar" class="settings-panel">
          <i class="settings-close mdi mdi-close"></i>
          <ul class="nav nav-tabs" id="setting-panel" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="todo-tab" data-toggle="tab" href="#todo-section" role="tab" aria-controls="todo-section" aria-expanded="true">TO DO LIST</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="chats-tab" data-toggle="tab" href="#chats-section" role="tab" aria-controls="chats-section">CHATS</a>
            </li>
          </ul>
          <div class="tab-content" id="setting-content">
            <div class="tab-pane fade show active scroll-wrapper" id="todo-section" role="tabpanel" aria-labelledby="todo-section">
              <div class="add-items d-flex px-3 mb-0">
                <form class="form w-100">
                  <div class="form-group d-flex">
                    <input type="text" class="form-control todo-list-input" placeholder="Add To-do">
                    <button type="submit" class="add btn btn-primary todo-list-add-btn" id="add-task">Add</button>
                  </div>
                </form>
              </div>
              <div class="list-wrapper px-3">
                <ul class="d-flex flex-column-reverse todo-list">
                  <li>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input class="checkbox" type="checkbox">
                        Team review meeting at 3.00 PM
                      </label>
                    </div>
                    <i class="remove mdi mdi-close-circle-outline"></i>
                  </li>
                  <li>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input class="checkbox" type="checkbox">
                        Prepare for presentation
                      </label>
                    </div>
                    <i class="remove mdi mdi-close-circle-outline"></i>
                  </li>
                  <li>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input class="checkbox" type="checkbox">
                        Resolve all the low priority tickets due today
                      </label>
                    </div>
                    <i class="remove mdi mdi-close-circle-outline"></i>
                  </li>
                  <li class="completed">
                    <div class="form-check">
                      <label class="form-check-label">
                        <input class="checkbox" type="checkbox" checked>
                        Schedule meeting for next week
                      </label>
                    </div>
                    <i class="remove mdi mdi-close-circle-outline"></i>
                  </li>
                  <li class="completed">
                    <div class="form-check">
                      <label class="form-check-label">
                        <input class="checkbox" type="checkbox" checked>
                        Project review
                      </label>
                    </div>
                    <i class="remove mdi mdi-close-circle-outline"></i>
                  </li>
                </ul>
              </div>
              <div class="events py-4 border-bottom px-3">
                <div class="wrapper d-flex mb-2">
                  <i class="mdi mdi-circle-outline text-primary mr-2"></i>
                  <span>Feb 11 2018</span>
                </div>
                <p class="mb-0 font-weight-thin text-gray">Creating component page</p>
                <p class="text-gray mb-0">build a js based app</p>
              </div>
              <div class="events pt-4 px-3">
                <div class="wrapper d-flex mb-2">
                  <i class="mdi mdi-circle-outline text-primary mr-2"></i>
                  <span>Feb 7 2018</span>
                </div>
                <p class="mb-0 font-weight-thin text-gray">Meeting with Alisa</p>
                <p class="text-gray mb-0 ">Call Sarah Graves</p>
              </div>
            </div>
            <!-- To do section tab ends -->
            <div class="tab-pane fade" id="chats-section" role="tabpanel" aria-labelledby="chats-section">
              <div class="d-flex align-items-center justify-content-between border-bottom">
                <p class="settings-heading border-top-0 mb-3 pl-3 pt-0 border-bottom-0 pb-0">Friends</p>
                <small class="settings-heading border-top-0 mb-3 pt-0 border-bottom-0 pb-0 pr-3 font-weight-normal">See All</small>
              </div>
              <ul class="chat-list">
                <li class="list active">
                  <div class="profile"><img src="images/faces/face1.jpg" alt="image"><span class="online"></span></div>
                  <div class="info">
                    <p>Thomas Douglas</p>
                    <p>Available</p>
                  </div>
                  <small class="text-muted my-auto">19 min</small>
                </li>
                <li class="list">
                  <div class="profile"><img src="images/faces/face2.jpg" alt="image"><span class="offline"></span></div>
                  <div class="info">
                    <div class="wrapper d-flex">
                      <p>Catherine</p>
                    </div>
                    <p>Away</p>
                  </div>
                  <div class="badge badge-success badge-pill my-auto mx-2">4</div>
                  <small class="text-muted my-auto">23 min</small>
                </li>
                <li class="list">
                  <div class="profile"><img src="images/faces/face3.jpg" alt="image"><span class="online"></span></div>
                  <div class="info">
                    <p>Daniel Russell</p>
                    <p>Available</p>
                  </div>
                  <small class="text-muted my-auto">14 min</small>
                </li>
                <li class="list">
                  <div class="profile"><img src="images/faces/face4.jpg" alt="image"><span class="offline"></span></div>
                  <div class="info">
                    <p>James Richardson</p>
                    <p>Away</p>
                  </div>
                  <small class="text-muted my-auto">2 min</small>
                </li>
                <li class="list">
                  <div class="profile"><img src="images/faces/face5.jpg" alt="image"><span class="online"></span></div>
                  <div class="info">
                    <p>Madeline Kennedy</p>
                    <p>Available</p>
                  </div>
                  <small class="text-muted my-auto">5 min</small>
                </li>
                <li class="list">
                  <div class="profile"><img src="../../images/faces/face6.jpg" alt="image"><span class="online"></span></div>
                  <div class="info">
                    <p>Sarah Graves</p>
                    <p>Available</p>
                  </div>
                  <small class="text-muted my-auto">47 min</small>
                </li>
              </ul>
            </div>
            <!-- chat tab ends -->
          </div>
        </div>
        <!-- partial -->
        <!-- partial:../../partials/_sidebar.html -->
        <nav class="sidebar sidebar-offcanvas" id="sidebar">
          <ul class="nav">
            <li class="nav-item nav-profile">
              <div class="nav-link">
                  <div class="profile-name">
                  <p class="name">
                    VIVEKNADK.K
                  </p>
                  <p class="designation">
                    Super Admin
                  </p>
                </div>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="">
                <i class="icon-grid menu-icon"></i>
                <span class="menu-title">Dashboard</span>
               
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="">
                <i class="icon-user menu-icon"></i>
                <span class="menu-title">Employees</span>
              </a>
            </li>
            <li class="nav-item d-none d-lg-block">
              <a class="nav-link" data-toggle="collapse" href="#sidebar-layouts" aria-expanded="false" aria-controls="sidebar-layouts">
                 <i class="icon-check menu-icon"></i>
                <span class="menu-title">Pay roll</span>
               
              </a>
              <div class="collapse" id="sidebar-layouts">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item"> <a class="nav-link" href="">Overview</a></li>
                  <li class="nav-item"> <a class="nav-link" href="">Payruns</a></li>
                  <li class="nav-item"> <a class="nav-link" href="">Others</a></li>
                </ul>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
              <i class="mdi mdi-settings"></i>
                <span class="menu-title">Settings</span>
              </a>
              <div class="collapse" id="ui-basic">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item"> <a class="nav-link" href="">General settings</a></li>
                  <li class="nav-item"> <a class="nav-link" href="">Payroll Settings</a></li>
                  <li class="nav-item"> <a class="nav-link" href="">Reports</a></li>
                </ul>
                </div>

            <li class="nav-item">
              <a class="nav-link" href="">
                <i class="icon-diamond menu-icon"></i>
                <span class="menu-title">Full screen</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="">
                <i class="icon-bell menu-icon"></i>
                <span class="menu-title">Logout</span>
              </a>
            </li>

          </ul>
        </nav>
        <!-- partial -->
        <div class="content-wrapper">


            <div class="row">
            <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
      

<div class="stepwizard">
        <div class="stepwizard-row setup-panel">
            <div class="stepwizard-step col-xs-3"> 
                <a href="#step-1"  class="btn btn-success btn-default btn-circle" disabled="disabled">1</a>
                <p><small>Payitems</small></p>
            </div>
            <div class="stepwizard-step col-xs-3"> 
                <a href="#step-2" class="btn btn-default btn-circle" disabled="disabled">2</a>
                <p><small>Calendars</small></p>
            </div>
            <div class="stepwizard-step col-xs-3"> 
                <a href="#step-3"  class="btn btn-default btn-circle" disabled="disabled">3</a>
                <p><small>Cycles</small></p>
            </div>
        </div>
    </div><!--stepwizard cloased-->
         <div class="card setup-content" id="step-1">   
            <div class="card-body">
              <div class="tab-content tab-content-basic"><h5 class="mb-3">Payitems</h5>                          
               <div class="row">
                <div class="col-md-6 col-lg-3 template1">
                  <div class="overlay"></div>
                  <div class="card text-center">
                    <div class="card-body">
                  <ul class="header-dropdown">
                    <li class="pull-left">
                    
                    </li>

                <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true" style=" float: right; text-decoration: none; color: #8d9498; font-size: 8pt;">
                          <i class="icon-options" data-toggle="tooltip" data-placement="top" title="More actions"></i> </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuSizeButton1" x-placement="top-start" style=" position: absolute;">
                        <a class="dropdown-item" href="#" >Copy</a>
                        <a class="dropdown-item delete" href="#">Delete</a>
                      </div>
                    </li>
                     <!-- <li><i class="fa fa-edit"  data-toggle="modal" data-target="#exampleModal"></i></li> -->
                  </ul>

                  <div class="tc-accordion-icon">
                      <span class="tc-icon-cart">1</span>
                  </div>
                  <div class="mt-3">    
                    <h4 style="color: #7296a5;font-family: helvetica;">Payroll Template</h4>
                                  
                  </div>
                  <button class="btn btn-info btn-sm mt-3 mb-2 cmm-btn" data-toggle="modal" data-target="#exampleModal">Create</button>
                
                </div>
              </div>
              </div>
                   <c:forEach items="${c}" var="m">
                              <div class="col-md-6 col-lg-3 template1">
                  <div class="card text-center">
                    <div class="card-body">
                  <ul class="header-dropdown">
                  

                   <li class="dropdown show">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true" style=" float: right; text-decoration: none; color: #8d9498; font-size: 8pt;">
                          <i class="icon-options"data-toggle="tooltip" data-placement="top" title="More actions"></i> </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuSizeButton1" x-placement="top-start" style=" position: absolute;">
                        <a class="dropdown-item" href="#">Edit</a>
                        <a class="dropdown-item delete" href="#">Delete</a>
                      </div>
                    </li>
                     <li><i class="fa fa-edit"></i></li>
                  </ul>

                  <div class="tc-accordion-icon">
                      <span class="tc-icon-cart">2</span>
                  </div>
                       
                  <div class="mt-3">    
                    <h4 style="color: #7296a5;font-family: helvetica;">${m.name}</h4>
                    
                    <p class="text-muted">Creation Date:<span>${m.date1}</span></p>
                  </div>
                <button type="submit" class="btn btn-info btn-sm mt-3 mb-2 cmm-btn" data-toggle="modal" data-target="#exampleModal2">Copy</button>
                 </form>
                </div>
              </div>
              </div>

           </c:forEach>
          </div>
         </div>
              </div>
            </div><!--step 1 completed-->
 
                  <div class="card setup-content" id="step-2">
                    <div class="card-body">
              <div class="tab-content tab-content-basic"><h5 class="mb-3">Calendars</h5>                          
               <div class="row">
                <div class="col-md-6 col-lg-3">
                  <div class="card text-center">
                    <div class="card-body">
                  <ul class="header-dropdown">
                    <li class="pull-left">
                      <div class="checkbox checkbox-info checkbox-circle">
                        <input id="check8" class="styled" type="checkbox">
                        <label for="check8"></label>
                      </div>
                    </li>

                   <li class="dropdown show">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true" style=" float: right; text-decoration: none; color: #8d9498; font-size: 8pt;">
                          <i class="icon-options"data-toggle="tooltip" data-placement="top" title="More actions"></i> </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuSizeButton1" x-placement="top-start" style=" position: absolute;">
                        <a class="dropdown-item" href="#">Edit</a>
                        <a class="dropdown-item" href="#">Delete</a>
                      </div>
                    </li>
                     <li><i class="fa fa-edit"></i></li>
                  </ul>

                  <div class="tc-accordion-icon">
                      <span class="tc-icon-cart">2</span>
                  </div>
                  <div class="mt-3">    
                     <h4 style=" color: #7296a5;">Payitem template</h4>
                    <p class="text-muted">created</p>
                  </div>
                  <button class="btn btn-info btn-sm mt-3 mb-2">Copy</button>
                  <div class="border-top pt-2">
                    <p class="text-muted">VP Sales and Marketing Sales and Marketing</p>

                  </div>
                </div>
              </div>
              </div>

                              <div class="col-md-6 col-lg-3">
                  <div class="card text-center">
                    <div class="card-body">
                  <ul class="header-dropdown">
                    <li class="pull-left">
                      <div class="checkbox checkbox-info checkbox-circle">
                        <input id="check8" class="styled" type="checkbox">
                        <label for="check8"></label>
                      </div>
                    </li>

                   <li class="dropdown show">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true" style=" float: right; text-decoration: none; color: #8d9498; font-size: 8pt;">
                          <i class="icon-options"data-toggle="tooltip" data-placement="top" title="More actions"></i> </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuSizeButton1" x-placement="top-start" style=" position: absolute;">
                        <a class="dropdown-item" href="#">Edit</a>
                        <a class="dropdown-item" href="#">Delete</a>
                      </div>
                    </li>
                     <li><i class="fa fa-edit"></i></li>
                  </ul>

                  <div class="tc-accordion-icon">
                      <span class="tc-icon-cart">2</span>
                  </div>
                  <div class="mt-3">    
                     <h4 style=" color: #7296a5;">Payitem template</h4>
                    <p class="text-muted">created</p>
                  </div>
                  <button class="btn btn-info btn-sm mt-3 mb-2">Copy</button>
                  <div class="border-top pt-2">
                    <p class="text-muted">VP Sales and Marketing Sales and Marketing</p>

                  </div>
                </div>
              </div>
              </div>

                              <div class="col-md-6 col-lg-3">
                  <div class="card text-center">
                    <div class="card-body">
                  <ul class="header-dropdown">
                    <li class="pull-left">
                      <div class="checkbox checkbox-info checkbox-circle">
                        <input id="check8" class="styled" type="checkbox">
                        <label for="check8"></label>
                      </div>
                    </li>

                   <li class="dropdown show">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true" style=" float: right; text-decoration: none; color: #8d9498; font-size: 8pt;">
                          <i class="icon-options"data-toggle="tooltip" data-placement="top" title="More actions"></i> </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuSizeButton1" x-placement="top-start" style=" position: absolute;">
                        <a class="dropdown-item" href="#">Edit</a>
                        <a class="dropdown-item" href="#">Delete</a>
                      </div>
                    </li>
                     <li><i class="fa fa-edit"></i></li>
                  </ul>

                  <div class="tc-accordion-icon">
                      <span class="tc-icon-cart">2</span>
                  </div>
                  <div class="mt-3">    
                     <h4 style=" color: #7296a5;">Payitem template</h4>
                    <p class="text-muted">created</p>
                  </div>
                  <button class="btn btn-info btn-sm mt-3 mb-2">Copy</button>
                  <div class="border-top pt-2">
                    <p class="text-muted">VP Sales and Marketing Sales and Marketing</p>

                  </div>
                </div>
              </div>
              </div>
                              <div class="col-md-6 col-lg-3">
                  <div class="card text-center">
                    <div class="card-body">
                  <ul class="header-dropdown">
                    <li class="pull-left">
                      <div class="checkbox checkbox-info checkbox-circle">
                        <input id="check8" class="styled" type="checkbox">
                        <label for="check8"></label>
                      </div>
                    </li>

                   <li class="dropdown show">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true" style=" float: right; text-decoration: none; color: #8d9498; font-size: 8pt;">
                          <i class="icon-options"data-toggle="tooltip" data-placement="top" title="More actions"></i> </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuSizeButton1" x-placement="top-start" style=" position: absolute;">
                        <a class="dropdown-item" href="#">Edit</a>
                        <a class="dropdown-item" href="#">Delete</a>
                      </div>
                    </li>
                     <li><i class="fa fa-edit"></i></li>
                  </ul>

                  <div class="tc-accordion-icon">
                      <span class="tc-icon-cart">2</span>
                  </div>
                  <div class="mt-3">    
                     <h4 style=" color: #7296a5;">Payitem template</h4>
                    <p class="text-muted">created</p>
                  </div>
                  <button class="btn btn-info btn-sm mt-3 mb-2">Copy</button>
                  <div class="border-top pt-2">
                    <p class="text-muted">VP Sales and Marketing Sales and Marketing</p>

                  </div>
                </div>
              </div>
              </div>
            </div>
         </div>
              </div>
                 </div><!--step 2 completed-->


         <div class="card setup-content" id="step-3">
            <div class="card-body">
              <div class="tab-content tab-content-basic"><h5 class="mb-3">Cycles</h5>                          
               <div class="row">
                <div class="col-md-6 col-lg-3">
                  <div class="card text-center">
                    <div class="card-body">
                  <ul class="header-dropdown">
                    <li class="pull-left">
                      <div class="checkbox checkbox-info checkbox-circle">
                        <input id="cycle" class="styled" type="checkbox">
                        <label for="cycle"></label>
                      </div>
                    </li>

                   <li class="dropdown show">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true" style=" float: right; text-decoration: none; color: #8d9498; font-size: 8pt;">
                          <i class="icon-options"data-toggle="tooltip" data-placement="top" title="More actions"></i> </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuSizeButton1" x-placement="top-start" style=" position: absolute;">
                        <a class="dropdown-item" href="#">Edit</a>
                        <a class="dropdown-item" href="#">Delete</a>
                      </div>
                    </li>
                     <li><i class="fa fa-edit"></i></li>
                  </ul>

                  <div class="tc-accordion-icon">
                      <span class="tc-icon-cart">2</span>
                  </div>
                  <div class="mt-3">    
                     <h4 style=" color: #7296a5;">Payitem template</h4>
                    <p class="text-muted">created</p>
                  </div>
                  <button class="btn btn-info btn-sm mt-3 mb-2">Copy</button>
                  <div class="border-top pt-2">
                    <p class="text-muted">VP Sales and Marketing Sales and Marketing</p>

                  </div>
                </div>
              </div>
              </div>

                              <div class="col-md-6 col-lg-3">
                  <div class="card text-center">
                    <div class="card-body">
                  <ul class="header-dropdown">
                    <li class="pull-left">
                      <div class="checkbox checkbox-info checkbox-circle">
                        <input id="cycle2" class="styled" type="checkbox">
                        <label for="cycle2"></label>
                      </div>
                    </li>

                   <li class="dropdown show">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true" style=" float: right; text-decoration: none; color: #8d9498; font-size: 8pt;">
                          <i class="icon-options"data-toggle="tooltip" data-placement="top" title="More actions"></i> </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuSizeButton1" x-placement="top-start" style=" position: absolute; transform:">
                        <a class="dropdown-item" href="#">Edit</a>
                        <a class="dropdown-item" href="#">Delete</a>
                      </div>
                    </li>
                     <li><i class="fa fa-edit"></i></li>
                  </ul>

                  <div class="tc-accordion-icon">
                      <span class="tc-icon-cart">2</span>
                  </div>
                  <div class="mt-3">    
                     <h4 style=" color: #7296a5;">Payitem template</h4>
                    <p class="text-muted">created</p>
                  </div>
                  <button class="btn btn-info btn-sm mt-3 mb-2">Copy</button>
                  <div class="border-top pt-2">
                    <p class="text-muted">VP Sales and Marketing Sales and Marketing</p>

                  </div>
                </div>
              </div>
              </div>

                              <div class="col-md-6 col-lg-3">
                  <div class="card text-center">
                    <div class="card-body">
                  <ul class="header-dropdown">
                    <li class="pull-left">
                      <div class="checkbox checkbox-info checkbox-circle">
                        <input id="cycle3" class="styled" type="checkbox">
                        <label for="cycle3"></label>
                      </div>
                    </li>

                   <li class="dropdown show">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true" style=" float: right; text-decoration: none; color: #8d9498; font-size: 8pt;">
                          <i class="icon-options"data-toggle="tooltip" data-placement="top" title="More actions"></i> </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuSizeButton1" x-placement="top-start" style=" position: absolute;">
                        <a class="dropdown-item" href="#">Edit</a>
                        <a class="dropdown-item" href="#">Delete</a>
                      </div>
                    </li>
                     <li><i class="fa fa-edit"></i></li>
                  </ul>

                  <div class="tc-accordion-icon">
                      <span class="tc-icon-cart">2</span>
                  </div>
                  <div class="mt-3">    
                     <h4 style=" color: #7296a5;">Payitem template</h4>
                    <p class="text-muted">created</p>
                  </div>
                  <button class="btn btn-info btn-sm mt-3 mb-2">Copy</button>
                  <div class="border-top pt-2">
                    <p class="text-muted">VP Sales and Marketing Sales and Marketing</p>

                  </div>
                </div>
              </div>
              </div>
                              <div class="col-md-6 col-lg-3">
                  <div class="card text-center">
                    <div class="card-body">
                  <ul class="header-dropdown">
                    <li class="pull-left">
                      <div class="checkbox checkbox-info checkbox-circle">
                        <input id="cycle4" class="styled" type="checkbox">
                        <label for="cycle4"></label>
                      </div>
                    </li>

                   <li class="dropdown show">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true" style=" float: right; text-decoration: none; color: #8d9498; font-size: 8pt;">
                          <i class="icon-options"data-toggle="tooltip" data-placement="top" title="More actions"></i> </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuSizeButton1" x-placement="top-start" style=" position: absolute;">
                        <a class="dropdown-item" href="#">Edit</a>
                        <a class="dropdown-item" href="#">Delete</a>
                      </div>
                    </li>
                     <li><i class="fa fa-edit"></i></li>
                  </ul>

                  <div class="tc-accordion-icon">
                      <span class="tc-icon-cart">2</span>
                  </div>
                  <div class="mt-3">    
                     <h4 style=" color: #7296a5;">Payitem template</h4>
                    <p class="text-muted">created</p>
                  </div>
                  <button class="btn btn-info btn-sm mt-3 mb-2">Copy</button>
                  <div class="border-top pt-2">
                    <p class="text-muted">VP Sales and Marketing Sales and Marketing</p>

                  </div>
                </div>
              </div>
              </div>
            </div>
         </div>
              </div>
         </div><!--step 3 completed-->


          </div>   </div>
  
  

</div>





                       </div>
                    </div>
                </div>

        <form action="comp" method="post">
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Create Payroll Template</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                         <div class="row">
                        <div class="col-lg-12 grid-margin stretch-card">
                          <div class="card firststep">
                            <div class="card-body ">
                  
                              <div class="col-lg-12">
                                <div class="form-group tempdetails">

  <input type="text" class="form-control" id="usr" name="name" placeholder="Enter Template name">
 </div>



<button type="button" class="btn btn-primary multi-submit" data-toggle="tooltip" data-placement="top" title="" data-original-title="Submit to create template" id="hoverme" style=" background: #ffffff;
    font-size: 1.25rem;letter-spacing: 2px; border-radius: 31px; margin-top: 14px; margin-right: 20px;   box-shadow: 0 1px 6px rgba(32, 33, 36, 0.28);  border: 1px solid #e6e6e6; color: #757171;">Submit</button> 

<button type="button" class="btn btn-primary multi-submit" id="hoverme" data-dismiss="modal" style=" background: #ffffff;
    font-size: 1.25rem;letter-spacing: 2px; border-radius: 31px; margin-top: 14px; margin-right: 20px;   box-shadow: 0 1px 6px rgba(32, 33, 36, 0.28);  border: 1px solid #e6e6e6; color: #757171;">Cancel</button>

   
                              </div>


                          
                            </div></div></div>

                                              <div class="col-lg-12 grid-margin stretch-card payitemTable">
                          <div class="card">
                            <div class="card-body">
                    
             <div class="form-group">
              
                        <label>Select PayItems:</label>
                        <select class="js-example-basic-multiple" id="selection" multiple="multiple" style="width:100%" >
                          <option value="Gross">Gross</option>
                          <option value="HRA">HRA</option>
                          <option value="Medical">Medical</option>
                          <option value="PF">PF</option>
                          <option value="Insurance">Insurance</option>
                          </select>
                        
                       
                                                <button type="button" class="btn btn-primary multi-submit1" data-toggle="tooltip" data-placement="top" title="" data-original-title="Submit to create template" id="hoverme" style=" background: #ffffff;
    font-size: 1.25rem;letter-spacing: 2px; border-radius: 31px; margin-top: 14px; margin-right: 20px;   box-shadow: 0 1px 6px rgba(32, 33, 36, 0.28);  border: 1px solid #e6e6e6; color: #757171;" >Submit</button> 
                      </div>

 
                      <div class="card">
                          
                            <div class="card-body multi-table" style="padding:0px;">
                    <label style=" margin-bottom: 0; line-height: 1.5; font-size: 1.5rem;  font-family: helvetica;
    font-weight: 100!important;  color: #454d54; margin-bottom:20px;">PayItem Template</label>
   
        
                    <table class="table todo-list" id="targettable" >
                    <tbody><tr>
                        <th>PayItem</th>
                        <th>Category</th>
                        <th>Rate</th>
                        <th>Percentage</th>
                        <th>Amount</th>
                        <th>Base Value</th>
                        <th><div class="form-check"><label class="form-check-label"><input class="checkbox" type="checkbox">
                        <i class="input-helper"></i></label></div></th>
                        <th></th></tr>

                               </tbody> </table>
                                   <div class="btn-margin">
   <button type="submit" class="btn btn-primary" id="hoverme" style=" background: #ffffff;
    font-size: 1.25rem;letter-spacing: 2px; border-radius: 31px; margin-top: 14px; margin-right: 20px;   box-shadow: 0 1px 6px rgba(32, 33, 36, 0.28);  border: 1px solid #e6e6e6; color: #757171;">Save</button>
         <button type="button" class="btn btn-primary" id="hoverme" id="add-task" data-toggle="modal" data-target="#exampleModal3"style=" background: #ffffff;
    font-size: 1.25rem;letter-spacing: 2px; border-radius: 31px; margin-top: 14px; margin-right: 20px;   box-shadow: 0 1px 6px rgba(32, 33, 36, 0.28);  border: 1px solid #e6e6e6; color: #757171;">Create</button>


 <button type="button" class="btn btn-primary back" id="hoverme" style=" background: #ffffff;
    font-size: 1.25rem;letter-spacing: 2px; border-radius: 31px; margin-top: 14px; margin-right: 20px;   box-shadow: 0 1px 6px rgba(32, 33, 36, 0.28);  border: 1px solid #e6e6e6; color: #757171;">Back</button>
<button type="button" class="btn btn-primary multi-submit" id="hoverme" data-dismiss="modal" style="
    background: #ffffff; font-size: 1.25rem;  letter-spacing: 2px;   border-radius: 31px;   margin-top: 14px; margin-right: 20px;  box-shadow: 0 1px 6px rgba(32, 33, 36, 0.28);  border: 1px solid #e6e6e6;   color: #757171;">Cancel</button>
</div>
                              
    
        <!--btn-margin closed--></div></div>
 
                              </div>

                            </div>

                          </div>
</div>




                          </div>

                        </div>
   
                      </div>
           
                    </div><!--modal closed-->
</form>

      <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                      <div class="modal-content">
                        <div class="modal-header mod-header">
                          <h5 class="modal-title" id="exampleModalLabel2">Create Payroll Template</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                         <div class="row">
                        <div class="col-lg-12 grid-margin stretch-card">
                          <div class="card firststep">
                            <div class="card-body ">
                  
                              <div class="col-lg-12">
   <div class="form-group tempdetails">

  <input type="text" class="form-control add-list-input" id="usr" placeholder="Enter Template name">
 </div>

<button type="button" class="btn btn-primary" id="add-item" data-toggle="tooltip" data-placement="top" title="" data-original-title="Submit to create template" id="hoverme" style=" background: #ffffff;
    font-size: 1.25rem;letter-spacing: 2px; border-radius: 31px; margin-top: 14px; margin-right: 20px;   box-shadow: 0 1px 6px rgba(32, 33, 36, 0.28);  border: 1px solid #e6e6e6; color: #757171;">Submit</button> 

<button type="button" class="btn btn-primary multi-submit" id="hoverme" data-dismiss="modal" style=" background: #ffffff;
    font-size: 1.25rem;letter-spacing: 2px; border-radius: 31px; margin-top: 14px; margin-right: 20px;   box-shadow: 0 1px 6px rgba(32, 33, 36, 0.28);  border: 1px solid #e6e6e6; color: #757171;">Cancel</button>

   
                              </div>


                          
                            </div></div></div>

                                              <div class="col-lg-12 grid-margin stretch-card">
                         <div class="card">
                            <div class="card-body">
                    <label style=" margin-bottom: 0; line-height: 1.5; font-size: 1.5rem;  font-family: helvetica;
    font-weight: 100!important;  color: #454d54; margin-bottom:20px;">PayItem Template</label>

                    <table class="table todo-list todoListItem">
                    <tbody><tr>
                        <th>PayItem</th>
                        <th>Category</th>
                        <th>Rate</th>
                        <th>Percentage</th>
                        <th>Amount</th>
                        <th><div class="form-check"><label class="form-check-label"><input class="checkbox" type="checkbox">
                        <i class="input-helper"></i></label></div></th>
                        <th></th></tr>

                        <tr>

                          <td>HRA</td>
                          <td>Earnings</td>
                          <td>Percentage</td>
                          <td>100%</td>
                          <td>-</td>
                          <td></td>
                        </tr>
                               </tbody></table>
                                   <div class="btn-margin">
   <button type="button" class="btn btn-primary" id="hoverme" style=" background: #ffffff;
    font-size: 1.25rem;letter-spacing: 2px; border-radius: 31px; margin-top: 14px; margin-right: 20px;   box-shadow: 0 1px 6px rgba(32, 33, 36, 0.28);  border: 1px solid #e6e6e6; color: #757171;">Save</button>


 <button type="button" class="btn btn-primary" id="hoverme" id="add-task" data-toggle="modal" data-target="#exampleModal3"style=" background: #ffffff;
    font-size: 1.25rem;letter-spacing: 2px; border-radius: 31px; margin-top: 14px; margin-right: 20px;   box-shadow: 0 1px 6px rgba(32, 33, 36, 0.28);  border: 1px solid #e6e6e6; color: #757171;">Create</button>



 <button type="button" class="btn btn-primary back" id="hoverme" style=" background: #ffffff;
    font-size: 1.25rem;letter-spacing: 2px; border-radius: 31px; margin-top: 14px; margin-right: 20px;   box-shadow: 0 1px 6px rgba(32, 33, 36, 0.28);  border: 1px solid #e6e6e6; color: #757171;">Back</button>


<button type="button" class="btn btn-primary multi-submit" id="hoverme" data-dismiss="modal" style="
    background: #ffffff; font-size: 1.25rem;  letter-spacing: 2px;   border-radius: 31px;   margin-top: 14px; margin-right: 20px;  box-shadow: 0 1px 6px rgba(32, 33, 36, 0.28);  border: 1px solid #e6e6e6;   color: #757171;">Cancel</button>
</div><!--btn-margin closed--></div></div>
                              </div>

                            </div>

                          </div>
</div>




                          </div>

                        </div>
   
                      </div>
           
                    </div><!--examplemodal2 closed-->

                    
                     <div class="modal fade" id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                      <div class="modal-content">
                 
                        <div class="modal-header mod-header">
                          <h5 class="modal-title" id="exampleModalLabel">Create Payroll Template</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
      <div class="modal-body">
                   

                  <div class="form-group form-float">
                    <label>PayItem Name:</label>
                              <input type="text" class="form-control todo-list-input inputme"  name="label" placeholder="Name here">
                            </div>
                                    <div class="form-group form-float">
                                    <label>Category:</label>
                                 <div class="form-group form-float">
                                    <select class="type form-control" name="category">
                                            <option>Choose</option>
                                            <option>Earnings</option>
                                            <option>Deductions</option>
                                            <option>Reimbursements</option>
                                        </select>

                                    </div></div>
                                  <div class="form-group form-float">
                                 <label>Rate type:</label>
                                 <div class="form-group form-float">
                              <select name="type" class="amt form-control" id="type" name="ratetype">
                                  <option>Choose</option>
                                    <option name="l_letter" value="percentage">Percentage(%)</option>
                                    <option name="parcel" value="amount">Amount</option>
                                </select>
                            </div></div>


                            
                              <div id="row_dim1">
                      <div class="row">
                      <div class="col-lg-6">
                                                              <div class="form-group form-float">
                    <label>Percentage:</label>
                    <input type="text" class="form-control todo-list-input"  id="per" name="value" placeholder="enter percentage" value="">
                  </div>
                      </div>
                       <div class="col-lg-6">
                          <div class="form-group">
              
                     <div class="form-group form-float">
                                    <label>Base Field:</label>
                                 <div class="form-group form-float">
                                    <select class="type form-control" id="bv" name="basevalue">
                          <option >Choose</option>
                          <option value="Basic">Basic</option>
                          <option value="HRA">HRA</option>                          
                          <option value="Medical">Medical</option>
                          <option value="PF">PF</option>
                          <option value="Insurance">Insurance</option>
                                           
                                        </select>

                                    </div></div>
                        
                        
                              </div>
                      </div>

                      </div>
           
                              </div>
                                 <div id="row_dim">
                                                    <div class="form-group form-float">
                    <label>Amount</label>
                    <input type="text" class="form-control todo-list-input inputme"  id="amt" name="value" placeholder="">
                  </div>
                            
                            </div>
                                   <button type="submit" class="btn btn-primary todo-list-add-btn modal-btn">Add</button>
           <button type="button" class="btn btn-secondary modal-btn" data-dismiss="modal">Close</button>
 
  <!--                           <div id="row_dim1">
                                <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text" id="validationTooltipUsernamePrepend">%</span>
        </div>
        <input type="number" class="form-control" id="validationTooltipUsername" placeholder="percentage"  required>
      </div>
    </div>
                       

        <div id="row_dim">
         <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text" id="validationTooltipUsernamePrepend">Rs</span>
        </div>
        <input type="number" class="form-control" id="validationTooltipUsername" placeholder="Enter Amount"  required>
      </div>
      </div>  -->

      <div class="modal-footer">
        
      </div>
    
    </div>
</div>




                          </div>

                        </div>
 

                      </div>
           
                    </div><!--examplemodal2 closed-->

                  </div>
 
                </div>




          </div><!--content-wrapper closed-->
 
        <!-- content-wrapper ends -->
        <!-- partial:../../partials/_footer.html -->

        <!-- partial -->
      </div>
      <!-- row-offcanvas ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- plugins:js -->
  <script src="node_modules/jquery/dist/jquery.min.js"></script>
  <script src="node_modules/popper.js/dist/umd/popper.min.js"></script>
  <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="node_modules/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js"></script>
  <!-- endinject -->

  <script src="node_modules/select2/dist/js/select2.min.js"></script>
  <!-- Plugin js for this page-->
  <script src="node_modules/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
  <script src="node_modules/chart.js/dist/Chart.min.js"></script>
  <script src="node_modules/raphael/raphael.min.js"></script>
  <script src="node_modules/morris.js/morris.min.js"></script>
  <script src="node_modules/jquery-sparkline/jquery.sparkline.min.js"></script>  
    <script src="node_modules/jquery-tags-input/dist/jquery.tagsinput.min.js"></script>
  <!-- End plugin js for this page-->

  <script src="js/form-addons.js"></script>

  <script src="js/select2.js"></script>
    <script src="js/file-upload.js"></script>

        <script src="js/multi-step-modal.js"></script>


<script>
$('#usr_btn').on('click', function(e) {
    e.preventDefault();
  if ($('#input1').val() != null) {
    var user = $('#input1').val();
    var count = $('#' + user).length;
    if (count == 0) {
      $('.addedtemp h4').append("<h4 id=" + user + ">" + user + "</h4>");
       $("#input1").attr("disabled", "disabled");
    }

  }
});
</script>


<script>
var tabs = $('.tabs');
var items = $('.tabs').find('a').length;
var selector = $(".tabs").find(".selector");
var activeItem = tabs.find('.active');
var activeWidth = activeItem.innerWidth();
$(".selector").css({
  "left": activeItem.position.left + "px", 
  "width": activeWidth + "px"
});

$(".tabs").on("click","a",function(e){
    e.preventDefault(e);
  $('.tabs a').removeClass("active");
  $(this).addClass('active');
  var activeWidth = $(this).innerWidth();
  var itemPos = $(this).position();
  $(".selector").css({
    "left":itemPos.left + "px", 
    "width": activeWidth + "px"
  });
});



$('.multi-submit').on('click', function(event) {
event.preventDefault();
var multiselect = $('.js-example-basic-multiple').val();
$(".stand").show();

});


</script>

<script>
$(document).ready(function () {

    var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-success').addClass('btn-default');
            $item.addClass('btn-success');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for (var i = 0; i < curInputs.length; i++) {
            if (!curInputs[i].validity.valid) {
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid) nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-success').trigger('click');
});
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

</script>
<script>
$('body').on('click','.delete', function(){

  $(this).closest(".template1").hide();
});

 var todoListInput1 = $('.add-list-input');
$('#exampleModal2').on('click', '#add-item', function(e){
  event.preventDefault();
  var payitem = $('.add-list-input').val();
  
 $(".mod-header h5").replaceWith('<h1 class="red">' + payitem + '</h1>');
 todoListInput1.val("");
});


 var todoListInput1 = $('.add-list-input');
$('#exampleModal').on('click', '#add-item', function(e){
  event.preventDefault();
  var payitem = $('.add-list-input').val();
  
 $(".mod-header h5").replaceWith('<h5 class="red">' + payitem + '</h5>');
 todoListInput1.val("");
});


 $(function () {
     $('#row_dim').hide();
       $('#row_dim1').hide();
     $('#type').change(function () {
         $('#row_dim').hide();
           
         if (this.options[this.selectedIndex].value == 'percentage') {
             $('#row_dim1').show();
               $('#row_dim').hide();
         }
             if (this.options[this.selectedIndex].value == 'amount') {
             $('#row_dim').show();
                $('#row_dim1').hide();
         }
     });
 });

 var todoListItem = $('.todo-list');
 var todoListInput = $('.todo-list-input');
 var todoListInput = $('.inputme');

 $('#exampleModal3').on('click', '.todo-list-add-btn', function(){
  event.preventDefault();
  var item = $('.inputme').val();
  var selectedcat = $(".type option:selected").val();
  var selectedamt = $(".amt option:selected").val();
  var percentage = $("#per").val();
  var amount = $("#amt").val();
  var basevalue=$("#bv option:selected").val();

  todoListItem.append("<tr><td> <input type='text' name='label' value="+item+"></input></td><td><input type='text' name='category' value="+selectedcat+"></input></td><td><input type='text' name='ratetype' value="+selectedamt+"></input></td><td><input type='text' name='value' value="+percentage+"></input></td><td><input type='text' name='value' value="+amount+"></input></td><td> <input type='text' name='basevalue' value="+basevalue+"></input></td><td><div class='form-check'><label class='form-check-label'><input class='checkbox' type='checkbox'/></td><td> <button id='but1' class='botaoadd'> x </button> </td></tr>");
  todoListInput.val("");
  $('#exampleModal3').modal('hide');

});

 

$(".multi-submit").click(function(){

$(".payitemTable").show();
$(".firststep").hide();
 });


$(".multi-submit1").click(function(){
$(".multi-table").show();
$(".payitemTable1").hide();
 });
$(".back").click(function(){

$(".payitemTable").hide();
$(".firststep").show();
 });

$(".payitem-submit").click(function(){
$(".payitemTable").show();
$(".tempdetails2").hide();

 });
</script>

<script>
 $("body").on("click","#but1",function(){ 

  $(this).closest('tr').remove();

  return false;

 });
</script>

<script>
 $('.paycalender').change(function() {
        $('.payname, .paycal, .paydate').show();
        
  $('#exampleModal3').on('hidden.bs.modal', function (e) {
           location.reload();
       });   
 });
</script>

<script>
$("#filter").change(function() {
  console.clear();
  var filterValue = $(this).val();
  var row = $('.row');

  row.each(function(i, el) {
    if ($(el).attr('data-type') == filterValue) {
      row.hide()
      $(el).show();
    }
  });
// In Addition to Wlin's Answer (For "All" value)
  if ("all" == filterValue) {
    row.show();
  }

});
</script>
<script>

function multisel(){

  var e = document.getElementById("selection");
  var strUser = e.options[e.selectedIndex].value;
  //alert("sel "+strUser);
  
  var opts = [];
  
  for (var i=0, len=e.options.length; i<len; i++) {
        opt = e.options[i];
        //alert("opt "+opt.value);
        // check if selected
        if ( opt.selected ) {
            // add to array of option elements to return from this function
            opts.push(opt.value);
            
        }
    //alert(opts[0]);
    }
    
  //alert("Array "+opts);
  return opts;

}

//var opts;

$('#selection').on('change', function() {

  multisel();

  
});

$('.multi-submit1').on('click', function(event) {
//alert("hi");

  event.preventDefault();

  var opts = multisel();
  
  //alert("Array "+opts.length);
  
  //$("#targettable tr").remove(); 
  $("#targettable").find("tr:not(:first)").remove();
  
  var table = document.getElementById("targettable");

  for (var i = 0; i < opts.length; i++) {
  
    var tr = document.createElement("tr");
    var td1 = document.createElement("td");
    
    var txt1 = document.createTextNode(opts[i]);
    //alert(opts[i]);
    var input1 = document.createElement('input');
    input1.type = "text";
    input1.value = opts[i];
    
    var td2 = document.createElement("td");
    var txt2 = document.createTextNode("Earnings");
    
    var input2 = document.createElement('input');
    input2.type = "text";
    input2.value = "Earnings";
    
    var td3 = document.createElement("td");
    var txt3 = document.createTextNode("Percentage");
    
    var input3 = document.createElement('input');
    input3.type = "text";
    input3.value = "Percentage";
    
    var td4 = document.createElement("td");
    var txt4 = document.createTextNode("10%");
    
    var input4 = document.createElement('input');
    input4.type = "text";
    input4.value = "10%";
    
    var td5 = document.createElement("td");
    var txt5 = document.createTextNode("-");

    td1.appendChild(input1);
    tr.appendChild(td1);
    td2.appendChild(input2);
    tr.appendChild(td2);
    td3.appendChild(input3);
    tr.appendChild(td3);
    td4.appendChild(input4);
    tr.appendChild(td4);
    td5.appendChild(txt5);
    tr.appendChild(td5);
    table.appendChild(tr);
  }
  
});

</script>
</body>

</html>