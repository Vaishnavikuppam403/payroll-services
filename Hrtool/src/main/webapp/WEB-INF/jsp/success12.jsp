
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <style>
        table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
}
    </style>
    <body>
        <h1>Tables</h1>
      
          
                <table>
                     <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Designation</th>
                        <th>Company</th>
                    </tr>
            <c:forEach items="${tempStudentList}" var="tempStudent"> 
                <tr>
                <td>${tempStudent.id}</td>
                <td>${tempStudent.name1}</td>
                <td>${tempStudent.address}</td>
                <td>${tempStudent.designation}</td>
                <td>${tempStudent.company}</td>
                </tr>
               
            </c:forEach>
               </table>
            
        <a href="basic">AddEmployee</a>
         <a href="imageupload">Image upload</a>
    
       
    </body>
</html>
