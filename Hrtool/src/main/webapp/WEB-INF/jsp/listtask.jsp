<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<title>Home</title>
<!-- <link rel="icon" href="favicon.ico" type="image/x-icon"> --> <!-- Favicon-->
<link rel="stylesheet" href="plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="plugins/morrisjs/morris.css" />
<link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-2.0.3.min.css"/>
<!-- Custom Css -->
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>

<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="css/color_skins.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
<!-- JQuery DataTable Css -->
<link rel="stylesheet" href="plugins/jquery-datatable/dataTables.bootstrap4.min.css">
</head>
<body class="theme-black">
<!-- Page Loader -->
<!-- <div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img src="assets/images/logo.svg" width="48" height="48" alt="Alpino"></div>
        <p>Please wait...</p>        
    </div>
</div> -->
<div class="overlay_menu">
    <button class="btn btn-primary btn-icon btn-icon-mini btn-round"><i class="zmdi zmdi-close"></i></button>
    <div class="container">        
        <div class="row clearfix">
            <div class="card">
                <div class="body">
                    <div class="input-group mb-0">
                        <input type="text" class="form-control" placeholder="Search...">
                        <div class="input-group-append">
                            <span class="input-group-text"><i class="zmdi zmdi-search"></i></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card links">
                <div class="body">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-sm-12">
                            <h6>App</h6>
                            <ul class="list-unstyled">
                                <li><a href="">Inbox</a></li>
                                <li><a href="">Chat</a></li>
                                <li><a href="">Calendar</a></li>
                                <li><a href="">File Manager</a></li>
                                <li><a href="contact.html">Contact list</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12">
                            <h6>User Interface (UI)</h6>
                            <ul class="list-unstyled">
                                <li><a href="">UI KIT</a></li>
                                <li><a href="">Tabs</a></li>
                                <li><a href="">Range Sliders</a></li>
                                <li><a href="">Modals</a></li>
                                <li><a href="">Alerts</a></li>
                                <li><a href="">Dialogs</a></li>
                                <li><a href="">Collapse</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12">
                            <h6>Sample Pages</h6>
                            <ul class="list-unstyled">                            
                                <li><a href="">Image Gallery</a></li>
                                <li><a href="">Profile</a></li>
                                <li><a href="">Timeline</a></li>
                                <li><a href="">Pricing</a></li>
                                <li><a href="">Invoices</a></li>
                                <li><a href="">FAQs</a></li>
                                <li><a href="">Search Results</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12">
                            <h6>About</h6>
                            <ul class="list-unstyled">
                                <li><a href="" target="_blank">About</a></li>
                                <li><a href="" target="_blank">Contact Us</a></li>
                                <li><a href="" target="_blank">Admin Templates</a></li>
                                <li><a href="" target="_blank">Services</a></li>
                                <li><a href="" target="_blank">Portfolio</a></li>                            
                            </ul>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12">
        
            </div>
        </div>
    </div>
</div>
<div class="overlay"></div><!-- Overlay For Sidebars -->

<!-- Left Sidebar -->
<aside id="minileftbar" class="minileftbar">
    <ul class="menu_list">
        <li>
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" href="index.html"><img src="images/logo.svg" alt="Alpino"></a>
        </li>
        <li><a href="javascript:void(0);" class="btn_overlay hidden-sm-down"><i class="zmdi zmdi-search"></i></a></li>        
       <!--  <li><a href="javascript:void(0);" class="menu-sm"><i class="zmdi zmdi-swap"></i></a></li>      -->   
        <li class="menuapp-btn"><a href=""><i class="zmdi zmdi-apps"></i></a></li>
        <li class="notifications badgebit">
            <a href="javascript:void(0);">
                <i class="zmdi zmdi-notifications"></i>
                <div class="notify">
                    <span class="heartbit"></span>
                    <span class="point"></span>
                </div>
            </a>
        </li>
        <li class="task badgebit">
            <a href="javascript:void(0);">
                <i class="zmdi zmdi-account-box-phone"></i>
                <div class="notify">
                    <span class="heartbit"></span>
                    <span class="point"></span>
                </div>
            </a>
        </li>
        <li><a href="events.html" title="Events"><i class="zmdi zmdi-calendar"></i></a></li>
        <!-- <li><a href="mail-inbox.html" title="Inbox"><i class="zmdi zmdi-email"></i></a></li> -->
        <li><a href="" title="Contact List"><i class="zmdi zmdi-account-box-phone"></i></a></li>        
        <li><a href=""><i class="zmdi zmdi-comments"></i></a></li>        
        <li><a href="javascript:void(0);" class="fullscreen" data-provide="fullscreen"><i class="zmdi zmdi-fullscreen"></i></a></li>
        <li class="power">
            <a href="javascript:void(0);" class="js-right-sidebar"><i class="zmdi zmdi-settings zmdi-hc-spin"></i></a>            
            <a href="sign-in.html" class="mega-menu"><i class="zmdi zmdi-power"></i></a>
        </li>
    </ul>    
</aside>

<aside class="right_menu">
    <div class="menu-app">
        <div class="slim_scroll">
            <div class="card">
                <div class="header">
                    <h2><strong>App</strong> Menu</h2>
                </div>
                <div class="body">
                    <ul class="list-unstyled menu">
                        <li><a href=""><i class="zmdi zmdi-calendar-note"></i><span>Calendar</span></a></li>
                        <li><a href=""><i class="zmdi zmdi-file-text"></i><span>File Manager</span></a></li>
                        <li><a href=""><i class="zmdi zmdi-blogger"></i><span>Blog</span></a></li>
                        <li><a href="javascript:void(0)"><i class="zmdi zmdi-arrows"></i><span>Notes</span></a></li>
                        <li><a href="javascript:void(0)"><i class="zmdi zmdi-view-column"></i><span>Taskboard</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="notif-menu">
        <div class="slim_scroll">
            <div class="card">
                <div class="header">
                    <h2><strong>Messages</strong></h2>
                </div>
                <div class="body">
                    <ul class="messages list-unstyled">
                        <li>
                            <a href="javascript:void(0);">
                                <div class="media">
                                    <img class="media-object rounded-circle" src="images/xs/avatar1.jpg" alt="">
                                    <div class="media-body">
                                        <span class="name">Alexander <small class="time">35min ago</small></span>
                                        <p class="message">New tasks needs to be done</p>                                        
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <div class="media">
                                    <img class="media-object rounded-circle" src="images/xs/avatar2.jpg" alt="">
                                    <div class="media-body">
                                        <span class="name">Grayson <small class="time">1hr ago</small></span>
                                        <p class="message">New tasks needs to be done</p>                                        
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <div class="media">
                                    <img class="media-object rounded-circle" src="images/xs/avatar3.jpg" alt="">
                                    <div class="media-body">
                                        <span class="name">Sophia <small class="time">31min ago</small></span>
                                        <p class="message">New tasks needs to be done</p>                                        
                                    </div>
                                </div>
                            </a>
                        </li>        
                    </ul>  
                </div>
            </div>
            <div class="card">
                <div class="header">
                    <h2><strong>Notifications</strong></h2>
                </div>
                <div class="body">
                    <ul class="notification list-unstyled">
                        <li>
                            <i class="zmdi zmdi-balance-wallet text-success"></i>
                            <strong>+$30 New sale</strong>
                            <p><a href="javascript:void(0)">Admin Template</a></p>
                            <small class="text-muted">7 min ago</small>
                        </li>
                        <li>
                            <i class="zmdi zmdi-edit text-info"></i>
                            <strong>You Edited file</strong>
                            <p><a href="javascript:void(0)">Docs.doc</a></p>
                            <small class="text-muted">15 min ago</small>
                        </li>
                        <li>
                            <i class="zmdi zmdi-delete text-danger"></i>
                            <strong>Project removed</strong>
                            <p><a href="javascript:void(0)">AdminX BS4</a></p>
                            <small class="text-muted">1 hours ago</small>
                        </li>
                        <li>
                            <i class="zmdi zmdi-account text-success"></i>
                            <strong>New user</strong>
                            <p><a href="javascript:void(0)">UI Designer</a></p>
                            <small class="text-muted">1 hours ago</small>
                        </li>
                        <li>
                            <i class="zmdi zmdi-flag text-warning"></i>
                            <strong>Alpino v1.0.0 is available</strong>
                            <p><a href="javascript:void(0)">Update now</a></p>
                            <small class="text-muted">5 hours ago</small>
                        </li>
                    </ul>  
                </div>
            </div>
        </div>
    </div>
    <div class="task-menu">


               <div class="slim_scroll">
     <div class="card">
        <h2><strong>Employees</strong></h2>
                <div class="header">
                    <h2><strong>Main</strong></h2>
                </div>
                <div class="body">
                    <ul class="notification list-unstyled">
                        <li>
                             <i class="zmdi zmdi-balance-wallet text-success"></i>                           
                            <p><a href="">Overwiew</a></p>
                            
                        </li>

                        <li>
                            <i class="zmdi zmdi-account text-success"></i>                           
                            <p><a href="">Employee Directory</a></p>
                            
                        </li>

                    </ul>  
                </div>
            </div>
            <div class="card">
                <div class="header">
                    <h2><strong>Information</strong></h2>
                </div>
                <div class="body">
                    <ul class="notification list-unstyled">
                           <li>
                             <i class="zmdi zmdi-balance-wallet text-success"></i>                           
                            <p><a href="">Employee Profile</a></p>
                            
                        </li>
                           <li>
                             <i class="zmdi zmdi-balance-wallet text-success"></i>                           
                            <p><a href="">Bank/PF/ESI</a></p>
                            
                        </li>
                           <li>
                             <i class="zmdi zmdi-balance-wallet text-success"></i>                           
                            <p><a href="">Position History</a></p>
                            
                        </li>
                           <li>
                             <i class="zmdi zmdi-balance-wallet text-success"></i>                           
                            <p><a href="">Employee Documents</a></p>
                            
                        </li>
                           <li>
                             <i class="zmdi zmdi-balance-wallet text-success"></i>                           
                            <p><a href="">Employee Salary</a></p>
                            
                        </li>

                 
                    </ul>  
                </div>
            </div>
                        <div class="card">
                <div class="header">
                    <h2><strong>Setup</strong></h2>
                </div>
                <div class="body">
                    <ul class="notification list-unstyled">
                        <li>
                             <i class="zmdi zmdi-balance-wallet text-success"></i>                           
                            <p><a href="">HR Forms</a></p>
                            
                        </li>
                                             <li>
                             <i class="zmdi zmdi-balance-wallet text-success"></i>                           
                            <p><a href="">Letter Template</a></p>
                            
                        </li>
                    </ul>  
                </div>
            </div>
        </div>
        <div class="slim_scroll">
            <div class="card tasks">
                <div class="header">
                    <h2><strong>Project</strong> Status</h2>
                </div>
                
                                <ul>
                                        <li class="mid-level open">
                                            <a href="/ngapp/employee/main">
                                                <span class="menu_text">Main</span>
                                                <i class="icon-chevron-down"></i>
                                            </a>
                                                <ul>
                                                            <li class="leaf-level open">
                                                                <a href="/ngapp/employee/main/overview">
                                                                    <span class="menu_text">Overview</span>
                                                                </a>
                                                            </li>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/main/emp-directory">
                                                                    <span class="menu_text">Employee Directory</span>
                                                                </a>
                                                            </li>

                                                </ul>
                                        </li>
                                        <li class="mid-level">
                                            <a href="/ngapp/employee/info">
                                                <span class="menu_text">Information</span>
                                                <i class="icon-chevron-down"></i>
                                            </a>
                                                <ul>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/info/profile">
                                                                    <span class="menu_text">Employee Profile</span>
                                                                </a>
                                                            </li>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/info/accounts">
                                                                    <span class="menu_text">Bank/PF/ESI</span>
                                                                </a>
                                                            </li>

                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/info/assets">
                                                                    <span class="menu_text">Assets</span>
                                                                </a>
                                                            </li>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/info/career_history">
                                                                    <span class="menu_text">Position History</span>
                                                                </a>
                                                            </li>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/info/previousemployeement">
                                                                    <span class="menu_text">Previous Employment</span>
                                                                </a>
                                                            </li>

                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/info/employee-docs">
                                                                    <span class="menu_text">Employee Documents</span>
                                                                </a>
                                                            </li>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/info/employee-contract">
                                                                    <span class="menu_text">Employee Contracts</span>
                                                                </a>
                                                            </li>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/info/employeesalary">
                                                                    <span class="menu_text">Employee Salary</span>
                                                                </a>
                                                            </li>
                                                </ul>
                                        </li>

                                        <li class="mid-level">
                                            <a href="/ngapp/employee/setup">
                                                <span class="menu_text">Setup</span>
                                                <i class="icon-chevron-down"></i>
                                            </a>
                                                <ul>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/setup/letter-template">
                                                                    <span class="menu_text">Letter Template</span>
                                                                </a>
                                                            </li>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/setup/hr-forms">
                                                                    <span class="menu_text">HR Forms</span>
                                                                </a>
                                                            </li>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/setup/employeesegment">
                                                                    <span class="menu_text">Employee Segment</span>
                                                                </a>
                                                            </li>
                                                            <li class="leaf-level">
                                                                <a href="/ngapp/employee/setup/v2/qb/employee-filter">
                                                                    <span class="menu_text">Employee Filter</span>
                                                                </a>
                                                            </li>
                                                </ul>
                                        </li>

                            </ul>
            </div>
        </div>



    </div>
    <div id="rightsidebar" class="right-sidebar">
        <ul class="nav nav-tabs">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#setting">Setting</a></li>        
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#activity">Activity</a></li>
        </ul>
        <div class="tab-content slim_scroll">
            <div class="tab-pane slideRight active" id="setting">
                <div class="card">
                    <div class="header">
                        <h2><strong>Colors</strong> Skins</h2>
                    </div>
                    <div class="body">
                        <ul class="choose-skin list-unstyled m-b-0">
                            <li data-theme="black" class="active">
                                <div class="black"></div>
                            </li>
                            <li data-theme="purple">
                                <div class="purple"></div>
                            </li>                   
                            <li data-theme="blue">
                                <div class="blue"></div>
                            </li>
                            <li data-theme="cyan">
                                <div class="cyan"></div>                    
                            </li>
                            <li data-theme="green">
                                <div class="green"></div>
                            </li>
                            <li data-theme="orange">
                                <div class="orange"></div>
                            </li>
                            <li data-theme="blush">
                                <div class="blush"></div>                    
                            </li>
                        </ul>
                    </div>
                </div>                
                <div class="card">
                    <div class="header">
                        <h2><strong>General</strong> Settings</h2>
                    </div>
                    <div class="body">
                        <ul class="setting-list list-unstyled m-b-0">
                            <li>
                                <div class="checkbox">
                                    <input id="checkbox1" type="checkbox">
                                    <label for="checkbox1">Report Panel Usage</label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox">
                                    <input id="checkbox2" type="checkbox" checked="">
                                    <label for="checkbox2">Email Redirect</label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox">
                                    <input id="checkbox3" type="checkbox">
                                    <label for="checkbox3">Notifications</label>
                                </div>                        
                            </li>
                            <li>
                                <div class="checkbox">
                                    <input id="checkbox4" type="checkbox">
                                    <label for="checkbox4">Auto Updates</label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox">
                                    <input id="checkbox5" type="checkbox" checked="">
                                    <label for="checkbox5">Offline</label>
                                </div>
                            </li>
                            <li>
                                <div class="checkbox m-b-0">
                                    <input id="checkbox6" type="checkbox">
                                    <label for="checkbox6">Location Permission</label>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="card">
                    <div class="header">
                        <h2><strong>Left</strong> Menu</h2>
                    </div>
                    <div class="body theme-light-dark">
                        <button class="t-dark btn btn-primary btn-round btn-block">Dark</button>
                    </div>
                </div>               
            </div>
            <div class="tab-pane slideLeft" id="activity">
                <div class="card activities">
                    <div class="header">
                        <h2><strong>Recent</strong> Activity Feed</h2>
                    </div>
                    <div class="body">
                        <div class="streamline b-accent">
                            <div class="sl-item">
                                <div class="sl-content">
                                    <div class="text-muted">Just now</div>
                                    <p>Finished task <a href="#" class="text-info">#features 4</a>.</p>
                                </div>
                            </div>
                            <div class="sl-item b-info">
                                <div class="sl-content">
                                    <div class="text-muted">10:30</div>
                                    <p><a href="#">@Jessi</a> retwit your post</p>
                                </div>
                            </div>
                            <div class="sl-item b-primary">
                                <div class="sl-content">
                                    <div class="text-muted">12:30</div>
                                    <p>Call to customer <a href="#" class="text-info">Jacob</a> and discuss the detail.</p>
                                </div>
                            </div>
                            <div class="sl-item b-warning">
                                <div class="sl-content">
                                    <div class="text-muted">1 days ago</div>
                                    <p><a href="#" class="text-info">Jessi</a> commented your post.</p>
                                </div>
                            </div>
                            <div class="sl-item b-primary">
                                <div class="sl-content">
                                    <div class="text-muted">2 days ago</div>
                                    <p>Call to customer <a href="#" class="text-info">Jacob</a> and discuss the detail.</p>
                                </div>
                            </div>
                            <div class="sl-item b-primary">
                                <div class="sl-content">
                                    <div class="text-muted">3 days ago</div>
                                    <p>Call to customer <a href="#" class="text-info">Jacob</a> and discuss the detail.</p>
                                </div>
                            </div>
                            <div class="sl-item b-warning">
                                <div class="sl-content">
                                    <div class="text-muted">4 Week ago</div>
                                    <p><a href="#" class="text-info">Jessi</a> commented your post.</p>
                                </div>
                            </div>
                            <div class="sl-item b-warning">
                                <div class="sl-content">
                                    <div class="text-muted">5 days ago</div>
                                    <p><a href="#" class="text-info">Jessi</a> commented your post.</p>
                                </div>
                            </div>
                            <div class="sl-item b-primary">
                                <div class="sl-content">
                                    <div class="text-muted">5 Week ago</div>
                                    <p>Call to customer <a href="#" class="text-info">Jacob</a> and discuss the detail.</p>
                                </div>
                            </div>
                            <div class="sl-item b-primary">
                                <div class="sl-content">
                                    <div class="text-muted">3 Week ago</div>
                                    <p>Call to customer <a href="#" class="text-info">Jacob</a> and discuss the detail.</p>
                                </div>
                            </div>
                            <div class="sl-item b-warning">
                                <div class="sl-content">
                                    <div class="text-muted">1 Month ago</div>
                                    <p><a href="#" class="text-info">Jessi</a> commented your post.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

 </aside>

<!-- Main Content -->
<section class="content home">
    <div class="container-fluid">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Link</a>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Dropdown
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#">Disabled</a>
      </li>
    </ul>
<form class="form-inline md-form form-sm">
    <i class="fa fa-search" aria-hidden="true"></i>
    <input class="form-control form-control-sm ml-3 w-75" type="text" placeholder="Search" aria-label="Search">
</form>
  </div>
</nav>
    </div>
</section>
<section class="content profile-page">
    <div class="container-fluid">

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">                    
                    <div class="body">
                        <button type="button" class="btn btn-round btn-simple btn-sm btn-default btn-filter" data-target="all">All Employees</button>
                        <button type="button" class="btn btn-round btn-simple btn-sm btn-success btn-filter" data-target="approved">Confirmed</button>
                        <button type="button" class="btn btn-round btn-simple btn-sm btn-warning btn-filter" data-target="suspended">Trainee</button>
                        <button type="button" class="btn btn-round btn-simple btn-sm btn-info btn-filter" data-target="pending">Contract</button>
                        <button type="button" class="btn btn-round btn-simple btn-sm btn-danger btn-filter" data-target="blocked">Probation</button>                        
                    </div>
                    <div class="header">
                        <h2><strong>Employees</strong></h2>                        
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <a href="#" class="btn btn-secondary btn-lg active pull-right" role="button" aria-pressed="true">Add Employee</a>
                            
                        </div>

                        <table class="table" id="makeEditable">
    <thead>
      <tr>
        <th>To</th>
        <th>Task Name</th>
    
        <th>Date</th>
        
      </tr>
    </thead>
    <tbody>
        <c:forEach items="${list}" var="l" >
      <tr>
        <td>${l.uname}</td>
        <td>${l.tname}</td>
        <td>${l.date}</td>
        
      </tr>  
      </c:forEach>
  
    </tbody>
  </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


</section>

<section>

</section>
<!-- Jquery Core Js -->
<script src="bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js ( jquery.v3.2.1, Bootstrap4 js) -->
<script src="bundles/vendorscripts.bundle.js"></script> <!-- slimscroll, waves Scripts Plugin Js -->

<script src="bundles/knob.bundle.js"></script> <!-- Jquery Knob-->
<script src="bundles/jvectormap.bundle.js"></script> <!-- JVectorMap Plugin Js -->
<script src="bundles/morrisscripts.bundle.js"></script> <!-- Morris Plugin Js --> 
<script src="bundles/sparkline.bundle.js"></script> <!-- sparkline Plugin Js --> 
<script src="bundles/doughnut.bundle.js"></script>

<script src="assets/bundles/mainscripts.bundle.js"></script>
<script src="assets/js/pages/index.js"></script>

<!-- Jquery DataTable Plugin Js --> 
<script src="bundles/datatablescripts.bundle.js"></script>
<script src="plugins/jquery-datatable/buttons/dataTables.buttons.min.js"></script>
<script src="plugins/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
<script src="plugins/jquery-datatable/buttons/buttons.colVis.min.js"></script>
<script src="plugins/jquery-datatable/buttons/buttons.html5.min.js"></script>
<script src="plugins/jquery-datatable/buttons/buttons.print.min.js"></script>

<script src="bundles/mainscripts.bundle.js"></script><!-- Custom Js --> 
<script src="js/pages/tables/jquery-datatable.js"></script>
      <script src="js/jquery.bootgrid.js"></script>
        <script src="js/jquery.bootgrid.fa.js"></script>
        <script src="js/pages/tables/tableedit.js"></script>
<script>
    $(document).ready(function () {
        $('.star').on('click', function () {
            $(this).toggleClass('star-checked');
        });

        $('.ckbox label').on('click', function () {
            $(this).parents('tr').toggleClass('selected');
        });

        $('.btn-filter').on('click', function () {
            var $target = $(this).data('target');
            if ($target != 'all') {
                $('.table tr').css('display', 'none');
                $('.table tr[data-status="' + $target + '"]').fadeIn('slow');
            } else {
                $('.table tr').css('display', 'none').fadeIn('slow');
            }
        });
    });
</script>
        <script>
 $('#makeEditable').SetEditable({ $addButton: $('#but_add')});
</script>
</body>

</html>