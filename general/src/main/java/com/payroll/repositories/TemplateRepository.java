/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payroll.repositories;

import com.payroll.template.Template;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 *
 * @author "Naveen Chindam"
 */
public interface TemplateRepository extends MongoRepository<Template,Integer>{
    
  
    
}
