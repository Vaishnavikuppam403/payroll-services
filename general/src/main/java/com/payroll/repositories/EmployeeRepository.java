/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payroll.repositories;

import com.payroll.employee.Employee;
import com.payroll.template.Salary;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 *
 * @author "Naveen Chindam"
 */
public interface EmployeeRepository extends MongoRepository<Employee,Integer>{
    
    public Employee findByEmpid(String empid);

   // public void save(Salary sal);
    
}
