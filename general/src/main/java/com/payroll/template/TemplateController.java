/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payroll.template;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class TemplateController {
    
    
    @Autowired
    TemplateService temservice;
    
    
    @RequestMapping(value="/payitms")
    public String payitems(){
        
        
        return "payitems";
    }
    
    
    @RequestMapping(value="/template")
    public String tempsave(HttpServletRequest req,HttpServletResponse res){
        
         System.out.println("ssssssssss");
        String[] arr=req.getParameterValues("label");
        String[] arr1=req.getParameterValues("category");
        String[] arr2=req.getParameterValues("ratetype");
        String[] arr3=req.getParameterValues("value");
        String[] arr4=req.getParameterValues("basevalue");
        
        List<Template> component=new ArrayList<Template>();
        
        
        
       int numberOfItems = arr.length;
        for (int i=0; i<numberOfItems; i++)
        {
            Template comp=new Template();
            
           comp.setLabel(arr[i]);
           comp.setCategory(arr1[i]);
           comp.setRatetype(arr2[i]);
           comp.setValue(arr3[i]);
           comp.setBasevalue(arr4[i]);
          
            System.out.println("Hello " + arr[i]);
            
            component.add(comp);
        }
         
          for(Template com : component){
              
             System.out.println("Label "+com.getLabel());
             
              System.out.println("category "+com.getCategory());
              
               System.out.println("percentage "+com.getRatetype());
               
               System.out.println("value "+com.getValue());
               
               System.out.println("base "+com.getBasevalue());
               
             temservice.tempsave(com);
          }
     
        return "index";
    }
    
    @RequestMapping(value="/sal")
    public String sal(String empid){
        
        System.out.println("ssss");
        
       temservice.sal();
        
        return "index";
    }
    
}
