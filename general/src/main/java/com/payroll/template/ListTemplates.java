/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payroll.template;

/**
 *
 * @author "Naveen Chindam"
 */
public class ListTemplates {
    
    private String[] label;
    
    private String[] category;
    
    private String[] percentage;
    
    private double[] value;
    
    private double[] basevalue;

    public String[] getLabel() {
        return label;
    }

    public void setLabel(String[] label) {
        this.label = label;
    }

    public String[] getCategory() {
        return category;
    }

    public void setCategory(String[] category) {
        this.category = category;
    }

    public String[] getPercentage() {
        return percentage;
    }

    public void setPercentage(String[] percentage) {
        this.percentage = percentage;
    }

    public double[] getValue() {
        return value;
    }

    public void setValue(double[] value) {
        this.value = value;
    }

    public double[] getBasevalue() {
        return basevalue;
    }

    public void setBasevalue(double[] basevalue) {
        this.basevalue = basevalue;
    }
 
}
