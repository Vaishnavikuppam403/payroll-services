/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payroll.template;

/**
 *
 * @author "Naveen Chindam"
 */
public class Template {
    
    
    String label;
    
    String category;
    
    String ratetype;
    
    String value;
    
    String basevalue;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getRatetype() {
        return ratetype;
    }

    public void setRatetype(String ratetype) {
        this.ratetype = ratetype;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getBasevalue() {
        return basevalue;
    }

    public void setBasevalue(String basevalue) {
        this.basevalue = basevalue;
    }
    
    
    
}
