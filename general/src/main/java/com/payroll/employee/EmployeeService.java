/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payroll.employee;

import java.util.List;

/**
 *
 * @author "Naveen Chindam"
 */
public interface EmployeeService {
    
    public Employee empsave(Employee emp);
    
    public Employee empupdate(Employee emp);
    
    public List<Employee> listemp();
 
    
}
