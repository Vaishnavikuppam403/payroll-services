/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payroll.employee;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author "Naveen Chindam"
 */
@Controller
public class EmployeeController {
    
    @Autowired
    EmployeeService empservice;
    
    
    @RequestMapping(value="/")
    public String emp(Model model){
     
        List<Employee> list=empservice.listemp();
     for(Employee emp:list){
            
            System.out.println("empid"+emp.getEmpid());
            System.out.println("empfirstname"+emp.getFirstname());
        }
        
        model.addAttribute("list", list);
        
        return "index";
    }
    
    @RequestMapping(value="emp1")
    public String emp1(HttpServletRequest req,HttpServletResponse res,Employee emp,Model model){
        
       empservice.empsave(emp);
       
       model.addAttribute("emp", emp);
        
        return "employee"; 
    }
    
    @RequestMapping(value="emp2")
    public String empupdate(HttpServletRequest req,HttpServletResponse res,Employee emp,Model model){
       
        empservice.empupdate(emp);
      
        return "employee";  
    }
}
