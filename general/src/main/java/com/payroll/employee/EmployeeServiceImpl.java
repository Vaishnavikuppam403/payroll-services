/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payroll.employee;

import com.payroll.repositories.EmployeeRepository;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author "Naveen Chindam"
 */

@Service()
public class EmployeeServiceImpl implements EmployeeService {
    
    
    @Autowired 
    EmployeeRepository emprepo;

    @Override
    public Employee empsave(Employee emp) {
        
       AtomicLong numberGenerator = new AtomicLong(10000);
       
       int random=(int) numberGenerator.getAndIncrement();
       
       String empid="E"+String.valueOf(random);
       
      // Employee empid1=emprepo.findByEmpid(empid);
       
       List<Employee> emplist=emprepo.findAll();
       
       for(Employee emp1:emplist){
        
       System.out.println("hello"+emp1.getContact());
           
       if(emp1.getEmpid().equals(empid)){
           
        random=(int) numberGenerator.getAndIncrement();
           
        empid="E"+String.valueOf(random);
           
            System.out.println("empidddd"+empid);
         
       }
       
       }
       System.out.println("empid"+empid);
       
       System.out.println("empid"+empid);
       
       emp.setEmpid(empid);
        
        emprepo.save(emp);
        
        return emp;
    }

    @Override
    public Employee empupdate(Employee emp) {
        
        Employee emp1=emprepo.findByEmpid(emp.getEmpid());
        
        if(emp1.getEmpid().equals(emp.getEmpid())){
            
            emprepo.save(emp);
            
            return emp;
            
        }
        else{
        
        return null;   
    }

    }

    @Override
    public List<Employee> listemp() {
        
        List<Employee> list=emprepo.findAll();
//        System.out.println("value"+list.getClass());
        
        return list;
    }
    
}
