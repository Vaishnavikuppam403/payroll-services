/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.payroll.employee;

import org.springframework.data.annotation.Id;

/**
 *
 * @author "Naveen Chindam"
 */
public class Employee {
    
    @Id
    private String empid;
    
    private String firstname;
    
    private String lastname;
    
    private String gender;
    
    private String contact;
    
    private String mail;
    
    private String role;
    
    private double ctc;
    
    private String startdate;
    
    private String dob;
    
    private String bloodgroup;
    
    private String pannumber;
    
    private String address;
    
    private String country;
    
    private String pfnumber;
    
    private String dlnumber;
    
    private String esinumber;
    
    private String fbid;
    
    private String linkedinid;
    
    private String twitterid;
    
    private String skypeid;
    
    private String jobtype;
    
    private String jobtitle;
    
    private String team;
    
    private String department;
    
    private String hr;
    
    private String paayruntype;
    
    private String paytemplate;
    
    private String relievingdate;
    
    

    public String getEmpid() {
        return empid;
    }

    public void setEmpid(String empid) {
        this.empid = empid;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public double getCtc() {
        return ctc;
    }

    public void setCtc(double ctc) {
        this.ctc = ctc;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getBloodgroup() {
        return bloodgroup;
    }

    public void setBloodgroup(String bloodgroup) {
        this.bloodgroup = bloodgroup;
    }

    public String getPannumber() {
        return pannumber;
    }

    public void setPannumber(String pannumber) {
        this.pannumber = pannumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPfnumber() {
        return pfnumber;
    }

    public void setPfnumber(String pfnumber) {
        this.pfnumber = pfnumber;
    }

    public String getDlnumber() {
        return dlnumber;
    }

    public void setDlnumber(String dlnumber) {
        this.dlnumber = dlnumber;
    }

    public String getEsinumber() {
        return esinumber;
    }

    public void setEsinumber(String esinumber) {
        this.esinumber = esinumber;
    }

    public String getFbid() {
        return fbid;
    }

    public void setFbid(String fbid) {
        this.fbid = fbid;
    }

    public String getLinkedinid() {
        return linkedinid;
    }

    public void setLinkedinid(String linkedinid) {
        this.linkedinid = linkedinid;
    }

    public String getTwitterid() {
        return twitterid;
    }

    public void setTwitterid(String twitterid) {
        this.twitterid = twitterid;
    }

    public String getSkypeid() {
        return skypeid;
    }

    public void setSkypeid(String skypeid) {
        this.skypeid = skypeid;
    }

    public String getJobtype() {
        return jobtype;
    }

    public void setJobtype(String jobtype) {
        this.jobtype = jobtype;
    }

    public String getJobtitle() {
        return jobtitle;
    }

    public void setJobtitle(String jobtitle) {
        this.jobtitle = jobtitle;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getHr() {
        return hr;
    }

    public void setHr(String hr) {
        this.hr = hr;
    }

    public String getPaayruntype() {
        return paayruntype;
    }

    public void setPaayruntype(String paayruntype) {
        this.paayruntype = paayruntype;
    }

    public String getPaytemplate() {
        return paytemplate;
    }

    public void setPaytemplate(String paytemplate) {
        this.paytemplate = paytemplate;
    }

    public String getRelievingdate() {
        return relievingdate;
    }

    public void setRelievingdate(String relievingdate) {
        this.relievingdate = relievingdate;
    }
    
    
   
}
