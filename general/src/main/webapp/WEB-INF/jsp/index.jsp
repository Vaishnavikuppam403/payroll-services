<%-- 
    Document   : index1
    Created on : 3 Aug, 2018, 4:19:29 PM
    Author     : Burgeonits
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from www.urbanui.com/victory/pages/layout/compact-menu.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 04 Sep 2018 01:04:10 GMT -->
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>HR</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>

  <link rel="stylesheet" href="../../node_modules/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="../../node_modules/simple-line-icons/css/simple-line-icons.css">
  <link rel="stylesheet" href="../../node_modules/flag-icon-css/css/flag-icon.min.css">
  <link rel="stylesheet" href="../../node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.min.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <link rel="stylesheet" href="../../node_modules/font-awesome/css/font-awesome.min.css" />
  <link rel="stylesheet" href="../../node_modules/jquery-bar-rating/dist/themes/fontawesome-stars.css">
  <!-- End plugin css for this page -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css">
  <!-- inject:css -->
  <link rel="stylesheet" href="../../css/style.css">
  <link rel="stylesheet" href="../../css/build.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="" />
  <style>
label{
  outline: 0;
}
.header-dropdown .checkbox {
    padding-left: 0px !important;
    top: -5px !important;
    position: absolute !important;
    left: 32px !important;
}
.header-dropdown{
  list-style: none;
}
  .dropdown-toggle::after {
    display: inline-block;
    width: 0;
    height: 0;
    margin-left: 0.255em;
    vertical-align: 0.255em;
    content: none !important;
    border-top: 0.3em solid;
    border-right: 0.3em solid transparent;
    border-bottom: 0;
    border-left: 0.3em solid transparent;
}
  .initials {
  position: relative;
  top: 25px; /* 25% of parent */
  font-size: 50px; /* 50% of parent */
  line-height: 50px; /* 50% of parent */
  color: #fff;
  font-family: "Courier New", monospace;
  font-weight: bold;
}

.dropdown-menu{
  position: absolute;
    top: 0px;
    left: 0px;
    transform: translate3d(84px, 22px, 0px) !important;
    will-change: transform;
}
  .letter{
    width: 60px;
    height: 60px;
    background: #03a9f3;
    border-radius: 50%;
    color: #fff;
    text-align: center;
    line-height: 60px;
    font-size: 2rem;
    margin: 0 auto;
  }
  .btn-circle {
    width: 30px;
    height: 30px;
    text-align: center;
    padding: 6px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 15px;
}
.stepwizard-step p {
    margin-top: 0px;
    color:#666;
}
.stepwizard-row {
    display: table-row;
}
.stepwizard {
       display: table;
    width: 67%;
    position: relative;
    margin: 0 auto;
}
.stepwizard-step button[disabled] {
    /*opacity: 1 !important;
    filter: alpha(opacity=100) !important;*/
}
.stepwizard .btn.disabled, .stepwizard .btn[disabled], .stepwizard fieldset[disabled] .btn {
    opacity:1 !important;
    color:#bbb;
}
.stepwizard-row:before {
    top: 20px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 80%;
    height: 2px;
    background-color: #21282c;
    z-index: 0;
    left: 120px;
}
.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}
.stepwizard .btn.disabled, .stepwizard .btn[disabled], .stepwizard fieldset[disabled] .btn {
    opacity: 1 !important;
    color: #bbb;
}
.btn.disabled, .btn.disabled:hover, .btn.disabled:focus, .btn.disabled.focus, .btn.disabled:active, .btn.disabled.active, .btn:disabled, .btn:disabled:hover, .btn:disabled:focus, .btn:disabled.focus, .btn:disabled:active, .btn:disabled.active, .btn[disabled], .btn[disabled]:hover, .btn[disabled]:focus, .btn[disabled].focus, .btn[disabled]:active, .btn[disabled].active, fieldset[disabled] .btn, fieldset[disabled] .btn:hover, fieldset[disabled] .btn:focus, fieldset[disabled] .btn.focus, fieldset[disabled] .btn:active, fieldset[disabled] .btn.active, .navbar .navbar-nav>a.btn.disabled, .navbar .navbar-nav>a.btn.disabled:hover, .navbar .navbar-nav>a.btn.disabled:focus, .navbar .navbar-nav>a.btn.disabled.focus, .navbar .navbar-nav>a.btn.disabled:active, .navbar .navbar-nav>a.btn.disabled.active, .navbar .navbar-nav>a.btn:disabled, .navbar .navbar-nav>a.btn:disabled:hover, .navbar .navbar-nav>a.btn:disabled:focus, .navbar .navbar-nav>a.btn:disabled.focus, .navbar .navbar-nav>a.btn:disabled:active, .navbar .navbar-nav>a.btn:disabled.active, .navbar .navbar-nav>a.btn[disabled], .navbar .navbar-nav>a.btn[disabled]:hover, .navbar .navbar-nav>a.btn[disabled]:focus, .navbar .navbar-nav>a.btn[disabled].focus, .navbar .navbar-nav>a.btn[disabled]:active, .navbar .navbar-nav>a.btn[disabled].active, fieldset[disabled] .navbar .navbar-nav>a.btn, fieldset[disabled] .navbar .navbar-nav>a.btn:hover, fieldset[disabled] .navbar .navbar-nav>a.btn:focus, fieldset[disabled] .navbar .navbar-nav>a.btn.focus, fieldset[disabled] .navbar .navbar-nav>a.btn:active, fieldset[disabled] .navbar .navbar-nav>a.btn.active {
      background-color: #e9ecef;
    border-color: #727373;
    color: #ffff;
}


.btn-success.disabled, .btn-success.disabled:hover, .btn-success.disabled:focus, .btn-success.disabled.focus, .btn-success.disabled:active, .btn-success.disabled.active, .btn-success:disabled, .btn-success:disabled:hover, .btn-success:disabled:focus, .btn-success:disabled.focus, .btn-success:disabled:active, .btn-success:disabled.active, .btn-success[disabled], .btn-success[disabled]:hover, .btn-success[disabled]:focus, .btn-success[disabled].focus, .btn-success[disabled]:active, .btn-success[disabled].active, fieldset[disabled] .btn-success, fieldset[disabled] .btn-success:hover, fieldset[disabled] .btn-success:focus, fieldset[disabled] .btn-success.focus, fieldset[disabled] .btn-success:active, fieldset[disabled] .btn-success.active {
  background-color: #434a54;
    border-color: #495057;

}
.wrapper{
  text-align:center;
  margin:50px auto;
}

.tabs{
  margin-top:50px;
  font-size:15px;
  padding:0px;
  list-style:none;
  background:#fff;
  box-shadow:0px 5px 20px rgba(0,0,0,0.1);
  display:inline-block;
  border-radius:50px;
  position:relative;
}

.tabs a{
  text-decoration:none;
  color: #777;
  text-transform:uppercase;
  padding:10px 20px;
  display:inline-block;
  position:relative;
  z-index:1;
  transition-duration:0.6s;
}

.tabs a.active{
  color:#fff;
}

.tabs a i{
  margin-right:5px;
}

.tabs .selector{
  height:100%;
  display:inline-block;
  position:absolute;
  left:0px;
  top:0px;
  z-index:1;
  border-radius:50px;
  transition-duration:0.6s;
  transition-timing-function: cubic-bezier(0.68, -0.55, 0.265, 1.55);
  
  background: #05abe0;
  background: -moz-linear-gradient(45deg, #05abe0 0%, #8200f4 100%);
  background: -webkit-linear-gradient(45deg, #05abe0 0%,#8200f4 100%);
  background: linear-gradient(45deg, #05abe0 0%,#8200f4 100%);
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#05abe0', endColorstr='#8200f4',GradientType=1 );
}
  .tab-content-basic .card{
    border-radius: 4px;
    background: #fff;
    box-shadow: 0 6px 10px rgba(0,0,0,.08), 0 0 6px rgba(0,0,0,.05);
    transition: .3s transform cubic-bezier(.155,1.105,.295,1.12),.3s box-shadow,.3s -webkit-transform cubic-bezier(.155,1.105,.295,1.12);
    padding: 10px 10px 10px 10px;
    cursor: pointer;
}

 .tab-content-basic .card:hover{
     transform: scale(1.05);
  box-shadow: 0 10px 20px rgba(0,0,0,.12), 0 4px 8px rgba(0,0,0,.06);
}

 .tab-content-basic .card h3{
  font-weight: 600;
}

 .tab-content-basic .card img{
  position: absolute;
  top: 20px;
  right: 15px;
  max-height: 120px;
}

 .tab-content-basic .card-1{
  background-image: url(https://ionicframework.com/img/getting-started/ionic-native-card.png);
      background-repeat: no-repeat;
    background-position: right;
    background-size: 80px;
}

 .tab-content-basic .card-2{
   background-image: url(https://ionicframework.com/img/getting-started/components-card.png);
      background-repeat: no-repeat;
    background-position: right;
    background-size: 80px;
}

 .tab-content-basic .card-3{
   background-image: url(https://ionicframework.com/img/getting-started/theming-card.png);
      background-repeat: no-repeat;
    background-position: right;
    background-size: 80px;
}

@media(max-width: 990px){
   .tab-content-basic .card{
    margin: 20px;
  }
} 


  </style>

</head>
<body class="sidebar-mini">
  <div class="container-scroller">
    <!-- partial:../../partials/_navbar.html -->
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
        <a class="navbar-brand brand-logo" href="">LOGO</a>
        <a class="navbar-brand brand-logo-mini" href=""><img src="../../images/logo-mini.svg" alt="logo"/></a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">
        <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
          <span class="icon-menu"></span>
        </button>
        <ul class="navbar-nav">
          <li class="nav-item dropdown d-none d-lg-flex">
           <a class="nav-link dropdown-toggle nav-btn" id="actionDropdown" href="#" data-toggle="dropdown" style="
    color: black;   border: 1px solid #e9ecef;    border-radius: 6px;">
             <span class="btn" style=" color: black;">+ Create new</span>
            </a>
            <div class="dropdown-menu navbar-dropdown dropdown-left" aria-labelledby="actionDropdown">
              <a class="dropdown-item" href="#">
                <i class="icon-user text-primary"></i>
                User Account
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">
                <i class="icon-user-following text-warning"></i>
                Admin User
              </a>

            </div>
          </li>
        </ul>
        <ul class="navbar-nav navbar-nav-right">
          <li class="nav-item dropdown d-none d-lg-flex">
            <a class="nav-link dropdown-toggle" id="languageDropdown" href="#" data-toggle="dropdown">
              <i class="flag-icon flag-icon-gb"></i>
              English
            </a>
            <div class="dropdown-menu navbar-dropdown" aria-labelledby="languageDropdown">
              <a class="dropdown-item font-weight-medium" href="#">
                <i class="flag-icon flag-icon-fr"></i>
                French
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item font-weight-medium" href="#">
                <i class="flag-icon flag-icon-es"></i>
                Espanol
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item font-weight-medium" href="#">
                <i class="flag-icon flag-icon-lt"></i>
                Latin
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item font-weight-medium" href="#">
                <i class="flag-icon flag-icon-ae"></i>
                Arabic
              </a>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
              <i class="icon-bell mx-0"></i>
              <span class="count"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
              <a class="dropdown-item">
                <p class="mb-0 font-weight-normal float-left">You have 4 new notifications
                </p>
                <span class="badge badge-pill badge-warning float-right">View all</span>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-success">
                    <i class="icon-info mx-0"></i>
                  </div>
                </div>
                <div class="preview-item-content">
                  <h6 class="preview-subject font-weight-medium">Application Error</h6>
                  <p class="font-weight-light small-text">
                    Just now
                  </p>
                </div>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-warning">
                    <i class="icon-speech mx-0"></i>
                  </div>
                </div>
                <div class="preview-item-content">
                  <h6 class="preview-subject font-weight-medium">Settings</h6>
                  <p class="font-weight-light small-text">
                    Private message
                  </p>
                </div>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-info">
                    <i class="icon-envelope mx-0"></i>
                  </div>
                </div>
                <div class="preview-item-content">
                  <h6 class="preview-subject font-weight-medium">New user registration</h6>
                  <p class="font-weight-light small-text">
                    2 days ago
                  </p>
                </div>
              </a>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link count-indicator dropdown-toggle" id="messageDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <i class="icon-envelope mx-0"></i>
              <span class="count"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="messageDropdown">
              <div class="dropdown-item">
                <p class="mb-0 font-weight-normal float-left">You have 7 unread mails
                </p>
                <span class="badge badge-info badge-pill float-right">View all</span>
              </div>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                    <img src="../../images/faces/face4.jpg" alt="image" class="profile-pic">
                </div>
                <div class="preview-item-content flex-grow">
                  <h6 class="preview-subject ellipsis font-weight-medium">David Grey
                    <span class="float-right font-weight-light small-text">1 Minutes ago</span>
                  </h6>
                  <p class="font-weight-light small-text">
                    The meeting is cancelled
                  </p>
                </div>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                    <img src="../../images/faces/face2.jpg" alt="image" class="profile-pic">
                </div>
                <div class="preview-item-content flex-grow">
                  <h6 class="preview-subject ellipsis font-weight-medium">Tim Cook
                    <span class="float-right font-weight-light small-text">15 Minutes ago</span>
                  </h6>
                  <p class="font-weight-light small-text">
                    New product launch
                  </p>
                </div>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                    <img src="../../images/faces/face3.jpg" alt="image" class="profile-pic">
                </div>
                <div class="preview-item-content flex-grow">
                  <h6 class="preview-subject ellipsis font-weight-medium"> Johnson
                    <span class="float-right font-weight-light small-text">18 Minutes ago</span>
                  </h6>
                  <p class="font-weight-light small-text">
                    Upcoming board meeting
                  </p>
                </div>
              </a>
            </div>
          </li>
          <li class="nav-item nav-settings d-none d-lg-block">
            <a class="nav-link" href="#">
              <i class="icon-grid"></i>
            </a>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="icon-menu"></span>
        </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <div class="row row-offcanvas row-offcanvas-right">

        <div id="right-sidebar" class="settings-panel">
          <i class="settings-close mdi mdi-close"></i>
          <ul class="nav nav-tabs" id="setting-panel" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="todo-tab" data-toggle="tab" href="#todo-section" role="tab" aria-controls="todo-section" aria-expanded="true">TO DO LIST</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="chats-tab" data-toggle="tab" href="#chats-section" role="tab" aria-controls="chats-section">CHATS</a>
            </li>
          </ul>
          <div class="tab-content" id="setting-content">
            <div class="tab-pane fade show active scroll-wrapper" id="todo-section" role="tabpanel" aria-labelledby="todo-section">
              <div class="add-items d-flex px-3 mb-0">
                <form class="form w-100">
                  <div class="form-group d-flex">
                    <input type="text" class="form-control todo-list-input" placeholder="Add To-do">
                    <button type="submit" class="add btn btn-primary todo-list-add-btn" id="add-task">Add</button>
                  </div>
                </form>
              </div>
              <div class="list-wrapper px-3">
                <ul class="d-flex flex-column-reverse todo-list">
                  <li>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input class="checkbox" type="checkbox">
                        Team review meeting at 3.00 PM
                      </label>
                    </div>
                    <i class="remove mdi mdi-close-circle-outline"></i>
                  </li>
                  <li>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input class="checkbox" type="checkbox">
                        Prepare for presentation
                      </label>
                    </div>
                    <i class="remove mdi mdi-close-circle-outline"></i>
                  </li>
                  <li>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input class="checkbox" type="checkbox">
                        Resolve all the low priority tickets due today
                      </label>
                    </div>
                    <i class="remove mdi mdi-close-circle-outline"></i>
                  </li>
                  <li class="completed">
                    <div class="form-check">
                      <label class="form-check-label">
                        <input class="checkbox" type="checkbox" checked>
                        Schedule meeting for next week
                      </label>
                    </div>
                    <i class="remove mdi mdi-close-circle-outline"></i>
                  </li>
                  <li class="completed">
                    <div class="form-check">
                      <label class="form-check-label">
                        <input class="checkbox" type="checkbox" checked>
                        Project review
                      </label>
                    </div>
                    <i class="remove mdi mdi-close-circle-outline"></i>
                  </li>
                </ul>
              </div>
              <div class="events py-4 border-bottom px-3">
                <div class="wrapper d-flex mb-2">
                  <i class="mdi mdi-circle-outline text-primary mr-2"></i>
                  <span>Feb 11 2018</span>
                </div>
                <p class="mb-0 font-weight-thin text-gray">Creating component page</p>
                <p class="text-gray mb-0">build a js based app</p>
              </div>
              <div class="events pt-4 px-3">
                <div class="wrapper d-flex mb-2">
                  <i class="mdi mdi-circle-outline text-primary mr-2"></i>
                  <span>Feb 7 2018</span>
                </div>
                <p class="mb-0 font-weight-thin text-gray">Meeting with Alisa</p>
                <p class="text-gray mb-0 ">Call Sarah Graves</p>
              </div>
            </div>
            <!-- To do section tab ends -->
            <div class="tab-pane fade" id="chats-section" role="tabpanel" aria-labelledby="chats-section">
              <div class="d-flex align-items-center justify-content-between border-bottom">
                <p class="settings-heading border-top-0 mb-3 pl-3 pt-0 border-bottom-0 pb-0">Friends</p>
                <small class="settings-heading border-top-0 mb-3 pt-0 border-bottom-0 pb-0 pr-3 font-weight-normal">See All</small>
              </div>
              <ul class="chat-list">
                <li class="list active">
                  <div class="profile"><img src="../../images/faces/face1.jpg" alt="image"><span class="online"></span></div>
                  <div class="info">
                    <p>Thomas Douglas</p>
                    <p>Available</p>
                  </div>
                  <small class="text-muted my-auto">19 min</small>
                </li>
                <li class="list">
                  <div class="profile"><img src="../../images/faces/face2.jpg" alt="image"><span class="offline"></span></div>
                  <div class="info">
                    <div class="wrapper d-flex">
                      <p>Catherine</p>
                    </div>
                    <p>Away</p>
                  </div>
                  <div class="badge badge-success badge-pill my-auto mx-2">4</div>
                  <small class="text-muted my-auto">23 min</small>
                </li>
                <li class="list">
                  <div class="profile"><img src="../../images/faces/face3.jpg" alt="image"><span class="online"></span></div>
                  <div class="info">
                    <p>Daniel Russell</p>
                    <p>Available</p>
                  </div>
                  <small class="text-muted my-auto">14 min</small>
                </li>
                <li class="list">
                  <div class="profile"><img src="../../images/faces/face4.jpg" alt="image"><span class="offline"></span></div>
                  <div class="info">
                    <p>James Richardson</p>
                    <p>Away</p>
                  </div>
                  <small class="text-muted my-auto">2 min</small>
                </li>
                <li class="list">
                  <div class="profile"><img src="../../images/faces/face5.jpg" alt="image"><span class="online"></span></div>
                  <div class="info">
                    <p>Madeline Kennedy</p>
                    <p>Available</p>
                  </div>
                  <small class="text-muted my-auto">5 min</small>
                </li>
                <li class="list">
                  <div class="profile"><img src="../../images/faces/face6.jpg" alt="image"><span class="online"></span></div>
                  <div class="info">
                    <p>Sarah Graves</p>
                    <p>Available</p>
                  </div>
                  <small class="text-muted my-auto">47 min</small>
                </li>
              </ul>
            </div>
            <!-- chat tab ends -->
          </div>
        </div>
        <!-- partial -->
        <!-- partial:../../partials/_sidebar.html -->
        <nav class="sidebar sidebar-offcanvas" id="sidebar">
          <ul class="nav">
            <li class="nav-item nav-profile">
              <div class="nav-link">
                  <div class="profile-name">
                  <p class="name">
                    VIVEKNADK.K
                  </p>
                  <p class="designation">
                    Super Admin
                  </p>
                </div>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="">
                <i class="icon-grid menu-icon"></i>
                <span class="menu-title">Dashboard</span>
               
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="">
                <i class="icon-user menu-icon"></i>
                <span class="menu-title">Employees</span>
              </a>
            </li>
            <li class="nav-item d-none d-lg-block">
              <a class="nav-link" data-toggle="collapse" href="#sidebar-layouts" aria-expanded="false" aria-controls="sidebar-layouts">
                 <i class="icon-check menu-icon"></i>
                <span class="menu-title">Pay roll</span>
               
              </a>
              <div class="collapse" id="sidebar-layouts">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item"> <a class="nav-link" href="">Overview</a></li>
                  <li class="nav-item"> <a class="nav-link" href="">Payruns</a></li>
                  <li class="nav-item"> <a class="nav-link" href="">Others</a></li>
                </ul>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
              <i class="mdi mdi-settings"></i>
                <span class="menu-title">Settings</span>
              </a>
              <div class="collapse" id="ui-basic">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item"> <a class="nav-link" href="">General settings</a></li>
                  <li class="nav-item"> <a class="nav-link" href="">Payroll Settings</a></li>
                  <li class="nav-item"> <a class="nav-link" href="">Reports</a></li>
                </ul>
                </div>

            <li class="nav-item">
              <a class="nav-link" href="">
                <i class="icon-diamond menu-icon"></i>
                <span class="menu-title">Full screen</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="">
                <i class="icon-bell menu-icon"></i>
                <span class="menu-title">Logout</span>
              </a>
            </li>

          </ul>
        </nav>
        <!-- partial -->
        <div class="content-wrapper">
          <div class="row">
<div class="col-lg-6">
         <div class="form-group">
                    <select id='filterText' style='display:inline-block' onchange='filterText()'>
                <option disabled selected>Select</option>
                <option value='all'>All</option>
                <option value='Active'>Active</option>
                <option value='Inactive'>Inactive</option> 
                <option value='Active'>Team</option>
                <option value='Active'>Unverified</option>
                <option value='Active'>Archive</option>                               
                
                </select>
              </div>
</div>
<div class="col-lg-6">
                         <div class="pull-right">
            <button type="button" class="btn btn-secondary btn-rounded btn-fw" data-toggle="modal" data-target="#exampleModal-5" style="cursor:pointer; color:#0d6ece;">Add Employee</button>
            </div>
</div>


                   <div class="table-responsive table-custom">

             <table class="table table-hover table-fixed table-condensed" id="mytab">
                
            <tr>
              <th class="addon-checkbox-container1">
                  <div class="custom-checkbox rounded dir-checkbox form-group dirty focus-out ember-view">
                    <div class="checkbox ">
                         <input type="checkbox">
                          <span class="icon-checkbox"></span>
                        </div>
                </div>
              </th>
              <th class="width-22 bck">Employee</th>
              <th class="bck">Contact</th>
              <th  class="bck">Team/Business unit</th>
              <th class="width-15 bck">Employee status</th>
              <th class="width-10 bck">Action</th>

            </tr>    
             <c:forEach items="${list}" var="emp">
          <tr class="employee-list-item focused-item content">
              <td class="addon-checkbox-container">
                  <div class="custom-checkbox rounded dir-checkbox form-group dirty focus-out ember-view">
                    <div class="checkbox ">
                         <input type="checkbox">
                          <span class="icon-checkbox"></span>
                        </div>
                </div>
              </td>

              <td>
                  <a href="" class="ember-view">
                    <div class="vertical-align relative">
                    <div class="vertical-align ember-view">
                      <div class="avatar-circle">
                       <img src="../../images/faces-clipart/pic-1.png">
                      </div>
                    </div>                 
                  </div>

                  <div class="vertical-align profile-details">
                    <div class="name ellipsis">
                       ${emp.lastname}
                    </div>
                    <div class="text-muted ellipsis">${emp.role}</div>
                  </div>
                  </a>        
              </td>

              <td class="contacts">
                  <div aria-label="lbrennan@freshteam.com" class="vertical-align hint--top">
                    <i class="icon-email vertical-align"></i>
                      <a href="" class="ember-view">
                        <span class="vertical-align">&nbsp;${emp.mail}</span>
                      </a>          
                  </div>
              </td>

            <td class="job-info ellipsis">${emp.team}<br>${emp.jobtype}</td>

            <td>Active</td>

            <td>Action</td>
      </tr>
     </c:forEach>
<!--       <tr class="employee-list-item focused-item content">
              <td class="addon-checkbox-container">
                  <div class="custom-checkbox rounded dir-checkbox form-group dirty focus-out ember-view">
                    <div class="checkbox ">
                         <input type="checkbox">
                          <span class="icon-checkbox"></span>
                        </div>
                </div>
              </td>

              <td>
                  <a href="" class="ember-view">
                    <div class="vertical-align relative">
                    <div class="vertical-align ember-view">
                      <div class="avatar-circle">
                        <img src="../../images/faces-clipart/pic-2.png">
                      </div>
                    </div>                 
                  </div>

                  <div class="vertical-align profile-details">
                    <div class="name ellipsis">
                        Brennan 
                    </div>
                    <div class="text-muted ellipsis">HR Manager</div>
                  </div>
                  </a>        
              </td>

              <td class="contacts">
                  <div aria-label="lbrennan@freshteam.com" class="vertical-align hint--top">
                    <i class="icon-email vertical-align"></i>
                      <a href="" class="ember-view">
                        <span class="vertical-align">&nbsp;lbrennan@adriott.com</span>
                      </a>          
                  </div>
              </td>

            <td class="job-info ellipsis"> Talent Acquisition<br>Human Resources</td>

            <td>Active</td>
            
            <td>Action</td>
      </tr>
                <tr class="employee-list-item focused-item content">
              <td class="addon-checkbox-container">
                  <div class="custom-checkbox rounded dir-checkbox form-group dirty focus-out ember-view">
                    <div class="checkbox ">
                         <input type="checkbox">
                          <span class="icon-checkbox"></span>
                        </div>
                </div>
              </td>

              <td>
                  <a href="" class="ember-view">
                    <div class="vertical-align relative">
                    <div class="vertical-align ember-view">
                      <div class="avatar-circle">
                       <img src="../../images/faces-clipart/pic-3.png">
                      </div>
                    </div>                 
                  </div>

                  <div class="vertical-align profile-details">
                    <div class="name ellipsis">
                        Viveknadh
                    </div>
                    <div class="text-muted ellipsis">HR Manager</div>
                  </div>
                  </a>        
              </td>

              <td class="contacts">
                  <div aria-label="lbrennan@freshteam.com" class="vertical-align hint--top">
                    <i class="icon-email vertical-align"></i>
                      <a href="" class="ember-view">
                        <span class="vertical-align">&nbsp;ratnan@adriott.com</span>
                      </a>          
                  </div>
              </td>

            <td class="job-info ellipsis">Talent Acquisition<br>Human Resources</td>

            <td>Active</td>

            <td>Action</td>
      </tr>
                <tr class="employee-list-item focused-item content ">
      <td class="addon-checkbox-container">
                  <div class="custom-checkbox rounded dir-checkbox form-group dirty focus-out ember-view">
                    <div class="checkbox ">
                         <input type="checkbox">
                          <span class="icon-checkbox"></span>
                        </div>
                </div>
              </td>

              <td>
                  <a href="" class="ember-view">
                    <div class="vertical-align relative">
                    <div class="vertical-align ember-view">
                      <div class="avatar-circle">
                      <img src="../../images/faces-clipart/pic-4.png">
                      </div>
                    </div>                 
                  </div>

                  <div class="vertical-align profile-details">
                    <div class="name ellipsis">
                        Ratan Tata
                    </div>
                    <div class="text-muted ellipsis">HR Manager</div>
                  </div>
                  </a>        
              </td>

              <td class="contacts">
                  <div aria-label="lbrennan@freshteam.com" class="vertical-align hint--top">
                    <i class="icon-email vertical-align"></i>
                      <a href="" class="ember-view">
                        <span class="vertical-align">&nbsp;ratnan@adriott.com</span>
                      </a>          
                  </div>
              </td>

            <td class="job-info ellipsis">Talent Acquisition<br>Human Resources</td>

            <td>Inactive</td>

            <td>Action</td>
      </tr>-->
    </table>
    </div><!--custom table closed-->

                      <div class="modal fade" id="exampleModal-5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-5" aria-hidden="true">
                    <div class="modal-dialog" role="document" id="modal-width" style="max-width: 80% !important;">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel-5"><span>Add New Employee</span></h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                                       <div class="row">
                                           
                                           <form action="emp1" method="post">
                                
                <div class="col-lg-12 empinfo" style="margin-top:0px !important">
                  <h4 class="headings">Employee details</h4>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="wfirstName2"> First Name :
                                                        <span class="danger">*</span>
                                                    </label>
                                                    <input type="text" class="form-control required" id="wfirstName2" name="firstname" aria-required="true"> </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="wlastName2"> Last Name :
                                                        <span class="danger">*</span>
                                                    </label>
                                                    <input type="text" class="form-control required" id="wlastName2" name="lastname" aria-required="true"> </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                             <div class="form-group">
                                                    <label for="wlastName2"> Contact :
                                                        <span class="danger">*</span>
                                                    </label>
                                                    <input type="text" class="form-control required" id="wlastName2" name="contact" aria-required="true"> </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="wphoneNumber2">Gender :</label>
                                                    <select class="form-control" name="gender">
                                                      <option>Male</option>
                                                      <option>Female</option>
                                                    </select>
                                            </div>
                                        </div>
                          
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="wlastName2"> Email Id :
                                                        <span class="danger">*</span>
                                                    </label>
                                                    <input type="text" class="form-control required" id="wlastName2" name="mail" aria-required="true"> </div>
                                            </div>
                                                                              <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="wphoneNumber2">Role :</label>
                                                    <select class="form-control" name="role">
                                                      <option>Talent Acquisition</option>
                                                      <option>Developer</option>
                                                    </select>
                                            </div>
                                        </div>
                                         <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="wlastName2"> CTC :
                                                        <span class="danger">*</span>
                                                    </label>
                                                    <input type="text" class="form-control required" id="wlastName2" name="ctc" aria-required="true"> </div>
                                            </div>
                                            
                                            
                                             <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="wlastName2"> Start Date :
                                                        <span class="danger">*</span>
                                                    </label>
                                                    <input type="text" name="startdate" placeholder="dd/mm/yyyy" aria-required="true">
                                                    </div>
                                            </div>
                               
                    </div>
                </div>
                 <button class="btn btn-info btn-sm mt-3 mb-2 cmm-btn">Submit</button>
                 
                 </form>
                              </div>
                       
                        </div>
                        </div>               
                      </div>
                    </div>  
          </div>
        </div><!--content-wrapper-->
        </div>

   <!-- plugins:js -->
  <script src="../../node_modules/jquery/dist/jquery.min.js"></script>
  <script src="../../node_modules/popper.js/dist/umd/popper.min.js"></script>
  <script src="../../node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="../../node_modules/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <script src="../../node_modules/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
  <script src="../../node_modules/chart.js/dist/Chart.min.js"></script>
  <script src="../../node_modules/raphael/raphael.min.js"></script>
  <script src="../../node_modules/morris.js/morris.min.js"></script>
  <script src="../../node_modules/jquery-sparkline/jquery.sparkline.min.js"></script>  
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="../../js/off-canvas.js"></script>
  <script src="../../js/hoverable-collapse.js"></script>
  <script src="../../js/misc.js"></script>
  <script src="../../js/settings.js"></script>
  <script src="../../js/todolist.js"></script>

  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="../../js/dashboard.js"></script>
  <!-- End custom js for this page-->

  <script>


 $(function () {
     $('#row_dim').hide();
       $('#row_dim1').hide();
     $('#type').change(function () {
         $('#row_dim').hide();
           
         if (this.options[this.selectedIndex].value == 'percentage') {
             $('#row_dim1').show();
               $('#row_dim').hide();
         }
             if (this.options[this.selectedIndex].value == 'amount') {
             $('#row_dim').show();
                $('#row_dim1').hide();
         }
     });
 });


 var todoListItem = $('.todo-list');
 var todoListInput = $('.todo-list-input');
 var todoListInput = $('.inputme');

 $('#exampleModal').on('click', '.btn-primary', function(){
  event.preventDefault();
  var item = $('.inputme').val();
  var selectedcat = $(".type option:selected").val();
  var selectedamt = $(".amt option:selected").val();

  todoListItem.append("<tr><td>"+ item +"</td><td>" + selectedcat + "</td><td>"+ selectedamt +"</td><td><div class='form-check'><label class='form-check-label'><input class='checkbox' type='checkbox'/><i class='input-helper'></i></label></div><td> <button id='but1' class='botaoadd'> x </button> </td></tr>");
  todoListInput.val("");
  $('#exampleModal').modal('hide');

});

  $("body").on("click","#but1",function(){ 

  $(this).closest('tr').remove();

  return false;
});


  $("#createtemp").click(function(){
    $(".showtemp").toggle(300);
    $(".notemp").hide();
});

    $(".createbtn").click(function(){
    $(".temp").toggle(300);
        $(".showtemp").toggle();
          $("#createtemp").hide();
 
});

      $(".todo-list-add-btn").click(function(){
        $(".not-hide").hide();
});
</script>
<script>
$('#usr_btn').on('click', function(e) {
    e.preventDefault();
  if ($('#input1').val() != null) {
    var user = $('#input1').val();
    var count = $('#' + user).length;
    if (count == 0) {
      $('.addedtemp h4').append("<h4 id=" + user + ">" + user + "</h4>");
       $("#input1").attr("disabled", "disabled");
    }

  }
});
$(function() {
    $( "#datepicker-1, #datepicker-2" ).datepicker();
});
</script>

<script>
var tabs = $('.tabs');
var items = $('.tabs').find('a').length;
var selector = $(".tabs").find(".selector");
var activeItem = tabs.find('.active');
var activeWidth = activeItem.innerWidth();
$(".selector").css({
  "left": activeItem.position.left + "px", 
  "width": activeWidth + "px"
});

$(".tabs").on("click","a",function(e){
    e.preventDefault(e);
  $('.tabs a').removeClass("active");
  $(this).addClass('active');
  var activeWidth = $(this).innerWidth();
  var itemPos = $(this).position();
  $(".selector").css({
    "left":itemPos.left + "px", 
    "width": activeWidth + "px"
  });
});
</script>

<script>
$(document).ready(function () {

    var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
            $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-success').addClass('btn-default');
            $item.addClass('btn-success');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function () {
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for (var i = 0; i < curInputs.length; i++) {
            if (!curInputs[i].validity.valid) {
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid) nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-success').trigger('click');
});
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

</script>



<script>
function filterText()
  {  
    var rex = new RegExp($('#filterText').val());
    if(rex =="/all/"){clearFilter()}else{
      $('.content').hide();
      $('.content').filter(function() {
      return rex.test($(this).text());
      }).show();
  }
  }
  
function clearFilter()
  {
    $('.filterText').val('');
    $('.content').show();
  }
</script>
</body>

</html>
