<%-- 
    Document   : index1
    Created on : 3 Aug, 2018, 4:19:29 PM
    Author     : Burgeonits
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from www.urbanui.com/victory/pages/layout/compact-menu.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 04 Sep 2018 01:04:10 GMT -->
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>HR</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>

  <link rel="stylesheet" href="../../node_modules/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="../../node_modules/simple-line-icons/css/simple-line-icons.css">
  <link rel="stylesheet" href="../../node_modules/flag-icon-css/css/flag-icon.min.css">
  <link rel="stylesheet" href="../../node_modules/perfect-scrollbar/dist/css/perfect-scrollbar.min.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <link rel="stylesheet" href="../../node_modules/font-awesome/css/font-awesome.min.css" />
  <link rel="stylesheet" href="../../node_modules/jquery-bar-rating/dist/themes/fontawesome-stars.css">
  <!-- End plugin css for this page -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css">
    <link rel="stylesheet" href="../../node_modules/morris.js/morris.css" />
  <!-- inject:css -->
  <link rel="stylesheet" href="../../css/style.css">
  <link rel="stylesheet" href="../../css/build.css">
    <link rel="stylesheet" href="../../css/jquery-ui.css">

  <!-- endinject -->
  <link rel="shortcut icon" href="" />
  <style>
label{
  outline: 0;
}
.header-dropdown .checkbox {
    padding-left: 0px !important;
    top: -5px !important;
    position: absolute !important;
    left: 32px !important;
}
.header-dropdown{
  list-style: none;
}
  .dropdown-toggle::after {
    display: inline-block;
    width: 0;
    height: 0;
    margin-left: 0.255em;
    vertical-align: 0.255em;
    content: none !important;
    border-top: 0.3em solid;
    border-right: 0.3em solid transparent;
    border-bottom: 0;
    border-left: 0.3em solid transparent;
}
  .initials {
  position: relative;
  top: 25px; /* 25% of parent */
  font-size: 50px; /* 50% of parent */
  line-height: 50px; /* 50% of parent */
  color: #fff;
  font-family: "Courier New", monospace;
  font-weight: bold;
}

.dropdown-menu{
  position: absolute;
    top: 0px;
    left: 0px;
    transform: translate3d(84px, 22px, 0px) !important;
    will-change: transform;
}
  .letter{
    width: 60px;
    height: 60px;
    background: #03a9f3;
    border-radius: 50%;
    color: #fff;
    text-align: center;
    line-height: 60px;
    font-size: 2rem;
    margin: 0 auto;
  }
  .btn-circle {
    width: 30px;
    height: 30px;
    text-align: center;
    padding: 6px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 15px;
}
.stepwizard-step p {
    margin-top: 0px;
    color:#666;
}
.stepwizard-row {
    display: table-row;
}
.stepwizard {
       display: table;
    width: 67%;
    position: relative;
    margin: 0 auto;
}
.stepwizard-step button[disabled] {
    /*opacity: 1 !important;
    filter: alpha(opacity=100) !important;*/
}
.stepwizard .btn.disabled, .stepwizard .btn[disabled], .stepwizard fieldset[disabled] .btn {
    opacity:1 !important;
    color:#bbb;
}
.stepwizard-row:before {
    top: 20px;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 80%;
    height: 2px;
    background-color: #21282c;
    z-index: 0;
    left: 120px;
}
.stepwizard-step {
    display: table-cell;
    text-align: center;
    position: relative;
}
.stepwizard .btn.disabled, .stepwizard .btn[disabled], .stepwizard fieldset[disabled] .btn {
    opacity: 1 !important;
    color: #bbb;
}
.btn.disabled, .btn.disabled:hover, .btn.disabled:focus, .btn.disabled.focus, .btn.disabled:active, .btn.disabled.active, .btn:disabled, .btn:disabled:hover, .btn:disabled:focus, .btn:disabled.focus, .btn:disabled:active, .btn:disabled.active, .btn[disabled], .btn[disabled]:hover, .btn[disabled]:focus, .btn[disabled].focus, .btn[disabled]:active, .btn[disabled].active, fieldset[disabled] .btn, fieldset[disabled] .btn:hover, fieldset[disabled] .btn:focus, fieldset[disabled] .btn.focus, fieldset[disabled] .btn:active, fieldset[disabled] .btn.active, .navbar .navbar-nav>a.btn.disabled, .navbar .navbar-nav>a.btn.disabled:hover, .navbar .navbar-nav>a.btn.disabled:focus, .navbar .navbar-nav>a.btn.disabled.focus, .navbar .navbar-nav>a.btn.disabled:active, .navbar .navbar-nav>a.btn.disabled.active, .navbar .navbar-nav>a.btn:disabled, .navbar .navbar-nav>a.btn:disabled:hover, .navbar .navbar-nav>a.btn:disabled:focus, .navbar .navbar-nav>a.btn:disabled.focus, .navbar .navbar-nav>a.btn:disabled:active, .navbar .navbar-nav>a.btn:disabled.active, .navbar .navbar-nav>a.btn[disabled], .navbar .navbar-nav>a.btn[disabled]:hover, .navbar .navbar-nav>a.btn[disabled]:focus, .navbar .navbar-nav>a.btn[disabled].focus, .navbar .navbar-nav>a.btn[disabled]:active, .navbar .navbar-nav>a.btn[disabled].active, fieldset[disabled] .navbar .navbar-nav>a.btn, fieldset[disabled] .navbar .navbar-nav>a.btn:hover, fieldset[disabled] .navbar .navbar-nav>a.btn:focus, fieldset[disabled] .navbar .navbar-nav>a.btn.focus, fieldset[disabled] .navbar .navbar-nav>a.btn:active, fieldset[disabled] .navbar .navbar-nav>a.btn.active {
      background-color: #e9ecef;
    border-color: #727373;
    color: #ffff;
}


.btn-success.disabled, .btn-success.disabled:hover, .btn-success.disabled:focus, .btn-success.disabled.focus, .btn-success.disabled:active, .btn-success.disabled.active, .btn-success:disabled, .btn-success:disabled:hover, .btn-success:disabled:focus, .btn-success:disabled.focus, .btn-success:disabled:active, .btn-success:disabled.active, .btn-success[disabled], .btn-success[disabled]:hover, .btn-success[disabled]:focus, .btn-success[disabled].focus, .btn-success[disabled]:active, .btn-success[disabled].active, fieldset[disabled] .btn-success, fieldset[disabled] .btn-success:hover, fieldset[disabled] .btn-success:focus, fieldset[disabled] .btn-success.focus, fieldset[disabled] .btn-success:active, fieldset[disabled] .btn-success.active {
  background-color: #434a54;
    border-color: #495057;

}
.wrapper{
  text-align:center;
  margin:50px auto;
}

.tabs{
  margin-top:50px;
  font-size:15px;
  padding:0px;
  list-style:none;
  background:#fff;
  box-shadow:0px 5px 20px rgba(0,0,0,0.1);
  display:inline-block;
  border-radius:50px;
  position:relative;
}

.tabs a{
  text-decoration:none;
  color: #777;
  text-transform:uppercase;
  padding:10px 20px;
  display:inline-block;
  position:relative;
  z-index:1;
  transition-duration:0.6s;
}

.tabs a.active{
  color:#fff;
}

.tabs a i{
  margin-right:5px;
}

.tabs .selector{
  height:100%;
  display:inline-block;
  position:absolute;
  left:0px;
  top:0px;
  z-index:1;
  border-radius:50px;
  transition-duration:0.6s;
  transition-timing-function: cubic-bezier(0.68, -0.55, 0.265, 1.55);
  
  background: #05abe0;
  background: -moz-linear-gradient(45deg, #05abe0 0%, #8200f4 100%);
  background: -webkit-linear-gradient(45deg, #05abe0 0%,#8200f4 100%);
  background: linear-gradient(45deg, #05abe0 0%,#8200f4 100%);
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#05abe0', endColorstr='#8200f4',GradientType=1 );
}
  .tab-content-basic .card{
    border-radius: 4px;
    background: #fff;
    box-shadow: 0 6px 10px rgba(0,0,0,.08), 0 0 6px rgba(0,0,0,.05);
    transition: .3s transform cubic-bezier(.155,1.105,.295,1.12),.3s box-shadow,.3s -webkit-transform cubic-bezier(.155,1.105,.295,1.12);
    padding: 10px 10px 10px 10px;
    cursor: pointer;
}

 .tab-content-basic .card:hover{
     transform: scale(1.05);
  box-shadow: 0 10px 20px rgba(0,0,0,.12), 0 4px 8px rgba(0,0,0,.06);
}

 .tab-content-basic .card h3{
  font-weight: 600;
}

 .tab-content-basic .card img{
  position: absolute;
  top: 20px;
  right: 15px;
  max-height: 120px;
}

 .tab-content-basic .card-1{
  background-image: url(https://ionicframework.com/img/getting-started/ionic-native-card.png);
      background-repeat: no-repeat;
    background-position: right;
    background-size: 80px;
}

 .tab-content-basic .card-2{
   background-image: url(https://ionicframework.com/img/getting-started/components-card.png);
      background-repeat: no-repeat;
    background-position: right;
    background-size: 80px;
}

 .tab-content-basic .card-3{
   background-image: url(https://ionicframework.com/img/getting-started/theming-card.png);
      background-repeat: no-repeat;
    background-position: right;
    background-size: 80px;
}

@media(max-width: 990px){
   .tab-content-basic .card{
    margin: 20px;
  }
} 


  </style>

</head>
<body class="sidebar-mini">
  <div class="container-scroller">
    <!-- partial:../../partials/_navbar.html -->
    <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
      <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
        <a class="navbar-brand brand-logo" href="">LOGO</a>
        <a class="navbar-brand brand-logo-mini" href=""><img src="../../images/logo-mini.svg" alt="logo"/></a>
      </div>
      <div class="navbar-menu-wrapper d-flex align-items-center">
        <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
          <span class="icon-menu"></span>
        </button>
        <ul class="navbar-nav">
          <li class="nav-item dropdown d-none d-lg-flex">
           <a class="nav-link dropdown-toggle nav-btn" id="actionDropdown" href="#" data-toggle="dropdown" style="
    color: black;   border: 1px solid #e9ecef;    border-radius: 6px;">
             <span class="btn" style=" color: black;">+ Create new</span>
            </a>
            <div class="dropdown-menu navbar-dropdown dropdown-left" aria-labelledby="actionDropdown">
              <a class="dropdown-item" href="#">
                <i class="icon-user text-primary"></i>
                User Account
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="#">
                <i class="icon-user-following text-warning"></i>
                Admin User
              </a>

            </div>
          </li>
        </ul>
        <ul class="navbar-nav navbar-nav-right">
          <li class="nav-item dropdown d-none d-lg-flex">
            <a class="nav-link dropdown-toggle" id="languageDropdown" href="#" data-toggle="dropdown">
              <i class="flag-icon flag-icon-gb"></i>
              English
            </a>
            <div class="dropdown-menu navbar-dropdown" aria-labelledby="languageDropdown">
              <a class="dropdown-item font-weight-medium" href="#">
                <i class="flag-icon flag-icon-fr"></i>
                French
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item font-weight-medium" href="#">
                <i class="flag-icon flag-icon-es"></i>
                Espanol
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item font-weight-medium" href="#">
                <i class="flag-icon flag-icon-lt"></i>
                Latin
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item font-weight-medium" href="#">
                <i class="flag-icon flag-icon-ae"></i>
                Arabic
              </a>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
              <i class="icon-bell mx-0"></i>
              <span class="count"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
              <a class="dropdown-item">
                <p class="mb-0 font-weight-normal float-left">You have 4 new notifications
                </p>
                <span class="badge badge-pill badge-warning float-right">View all</span>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-success">
                    <i class="icon-info mx-0"></i>
                  </div>
                </div>
                <div class="preview-item-content">
                  <h6 class="preview-subject font-weight-medium">Application Error</h6>
                  <p class="font-weight-light small-text">
                    Just now
                  </p>
                </div>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-warning">
                    <i class="icon-speech mx-0"></i>
                  </div>
                </div>
                <div class="preview-item-content">
                  <h6 class="preview-subject font-weight-medium">Settings</h6>
                  <p class="font-weight-light small-text">
                    Private message
                  </p>
                </div>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                  <div class="preview-icon bg-info">
                    <i class="icon-envelope mx-0"></i>
                  </div>
                </div>
                <div class="preview-item-content">
                  <h6 class="preview-subject font-weight-medium">New user registration</h6>
                  <p class="font-weight-light small-text">
                    2 days ago
                  </p>
                </div>
              </a>
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link count-indicator dropdown-toggle" id="messageDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
              <i class="icon-envelope mx-0"></i>
              <span class="count"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="messageDropdown">
              <div class="dropdown-item">
                <p class="mb-0 font-weight-normal float-left">You have 7 unread mails
                </p>
                <span class="badge badge-info badge-pill float-right">View all</span>
              </div>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                    <img src="../../images/faces/face4.jpg" alt="image" class="profile-pic">
                </div>
                <div class="preview-item-content flex-grow">
                  <h6 class="preview-subject ellipsis font-weight-medium">David Grey
                    <span class="float-right font-weight-light small-text">1 Minutes ago</span>
                  </h6>
                  <p class="font-weight-light small-text">
                    The meeting is cancelled
                  </p>
                </div>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                    <img src="../../images/faces/face2.jpg" alt="image" class="profile-pic">
                </div>
                <div class="preview-item-content flex-grow">
                  <h6 class="preview-subject ellipsis font-weight-medium">Tim Cook
                    <span class="float-right font-weight-light small-text">15 Minutes ago</span>
                  </h6>
                  <p class="font-weight-light small-text">
                    New product launch
                  </p>
                </div>
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item preview-item">
                <div class="preview-thumbnail">
                    <img src="../../images/faces/face3.jpg" alt="image" class="profile-pic">
                </div>
                <div class="preview-item-content flex-grow">
                  <h6 class="preview-subject ellipsis font-weight-medium"> Johnson
                    <span class="float-right font-weight-light small-text">18 Minutes ago</span>
                  </h6>
                  <p class="font-weight-light small-text">
                    Upcoming board meeting
                  </p>
                </div>
              </a>
            </div>
          </li>
          <li class="nav-item nav-settings d-none d-lg-block">
            <a class="nav-link" href="#">
              <i class="icon-grid"></i>
            </a>
          </li>
        </ul>
        <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
          <span class="icon-menu"></span>
        </button>
      </div>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <div class="row row-offcanvas row-offcanvas-right">

        <div id="right-sidebar" class="settings-panel">
          <i class="settings-close mdi mdi-close"></i>
          <ul class="nav nav-tabs" id="setting-panel" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="todo-tab" data-toggle="tab" href="#todo-section" role="tab" aria-controls="todo-section" aria-expanded="true">TO DO LIST</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="chats-tab" data-toggle="tab" href="#chats-section" role="tab" aria-controls="chats-section">CHATS</a>
            </li>
          </ul>
          <div class="tab-content" id="setting-content">
            <div class="tab-pane fade show active scroll-wrapper" id="todo-section" role="tabpanel" aria-labelledby="todo-section">
              <div class="add-items d-flex px-3 mb-0">
                <form class="form w-100">
                  <div class="form-group d-flex">
                    <input type="text" class="form-control todo-list-input" placeholder="Add To-do">
                    <button type="submit" class="add btn btn-primary todo-list-add-btn" id="add-task">Add</button>
                  </div>
                </form>
              </div>
              <div class="list-wrapper px-3">
                <ul class="d-flex flex-column-reverse todo-list">
                  <li>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input class="checkbox" type="checkbox">
                        Team review meeting at 3.00 PM
                      </label>
                    </div>
                    <i class="remove mdi mdi-close-circle-outline"></i>
                  </li>
                  <li>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input class="checkbox" type="checkbox">
                        Prepare for presentation
                      </label>
                    </div>
                    <i class="remove mdi mdi-close-circle-outline"></i>
                  </li>
                  <li>
                    <div class="form-check">
                      <label class="form-check-label">
                        <input class="checkbox" type="checkbox">
                        Resolve all the low priority tickets due today
                      </label>
                    </div>
                    <i class="remove mdi mdi-close-circle-outline"></i>
                  </li>
                  <li class="completed">
                    <div class="form-check">
                      <label class="form-check-label">
                        <input class="checkbox" type="checkbox" checked>
                        Schedule meeting for next week
                      </label>
                    </div>
                    <i class="remove mdi mdi-close-circle-outline"></i>
                  </li>
                  <li class="completed">
                    <div class="form-check">
                      <label class="form-check-label">
                        <input class="checkbox" type="checkbox" checked>
                        Project review
                      </label>
                    </div>
                    <i class="remove mdi mdi-close-circle-outline"></i>
                  </li>
                </ul>
              </div>
              <div class="events py-4 border-bottom px-3">
                <div class="wrapper d-flex mb-2">
                  <i class="mdi mdi-circle-outline text-primary mr-2"></i>
                  <span>Feb 11 2018</span>
                </div>
                <p class="mb-0 font-weight-thin text-gray">Creating component page</p>
                <p class="text-gray mb-0">build a js based app</p>
              </div>
              <div class="events pt-4 px-3">
                <div class="wrapper d-flex mb-2">
                  <i class="mdi mdi-circle-outline text-primary mr-2"></i>
                  <span>Feb 7 2018</span>
                </div>
                <p class="mb-0 font-weight-thin text-gray">Meeting with Alisa</p>
                <p class="text-gray mb-0 ">Call Sarah Graves</p>
              </div>
            </div>
            <!-- To do section tab ends -->
            <div class="tab-pane fade" id="chats-section" role="tabpanel" aria-labelledby="chats-section">
              <div class="d-flex align-items-center justify-content-between border-bottom">
                <p class="settings-heading border-top-0 mb-3 pl-3 pt-0 border-bottom-0 pb-0">Friends</p>
                <small class="settings-heading border-top-0 mb-3 pt-0 border-bottom-0 pb-0 pr-3 font-weight-normal">See All</small>
              </div>
              <ul class="chat-list">
                <li class="list active">
                  <div class="profile"><img src="../../images/faces/face1.jpg" alt="image"><span class="online"></span></div>
                  <div class="info">
                    <p>Thomas Douglas</p>
                    <p>Available</p>
                  </div>
                  <small class="text-muted my-auto">19 min</small>
                </li>
                <li class="list">
                  <div class="profile"><img src="../../images/faces/face2.jpg" alt="image"><span class="offline"></span></div>
                  <div class="info">
                    <div class="wrapper d-flex">
                      <p>Catherine</p>
                    </div>
                    <p>Away</p>
                  </div>
                  <div class="badge badge-success badge-pill my-auto mx-2">4</div>
                  <small class="text-muted my-auto">23 min</small>
                </li>
                <li class="list">
                  <div class="profile"><img src="../../images/faces/face3.jpg" alt="image"><span class="online"></span></div>
                  <div class="info">
                    <p>Daniel Russell</p>
                    <p>Available</p>
                  </div>
                  <small class="text-muted my-auto">14 min</small>
                </li>
                <li class="list">
                  <div class="profile"><img src="../../images/faces/face4.jpg" alt="image"><span class="offline"></span></div>
                  <div class="info">
                    <p>James Richardson</p>
                    <p>Away</p>
                  </div>
                  <small class="text-muted my-auto">2 min</small>
                </li>
                <li class="list">
                  <div class="profile"><img src="../../images/faces/face5.jpg" alt="image"><span class="online"></span></div>
                  <div class="info">
                    <p>Madeline Kennedy</p>
                    <p>Available</p>
                  </div>
                  <small class="text-muted my-auto">5 min</small>
                </li>
                <li class="list">
                  <div class="profile"><img src="../../images/faces/face6.jpg" alt="image"><span class="online"></span></div>
                  <div class="info">
                    <p>Sarah Graves</p>
                    <p>Available</p>
                  </div>
                  <small class="text-muted my-auto">47 min</small>
                </li>
              </ul>
            </div>
            <!-- chat tab ends -->
          </div>
        </div>
        <!-- partial -->
        <!-- partial:../../partials/_sidebar.html -->
        <nav class="sidebar sidebar-offcanvas" id="sidebar">
          <ul class="nav">
            <li class="nav-item nav-profile">
              <div class="nav-link">
                  <div class="profile-name">
                  <p class="name">
                    VIVEKNADK.K
                  </p>
                  <p class="designation">
                    Super Admin
                  </p>
                </div>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="">
                <i class="icon-grid menu-icon"></i>
                <span class="menu-title">Dashboard</span>
               
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="">
                <i class="icon-user menu-icon"></i>
                <span class="menu-title">Employees</span>
              </a>
            </li>
            <li class="nav-item d-none d-lg-block">
              <a class="nav-link" data-toggle="collapse" href="#sidebar-layouts" aria-expanded="false" aria-controls="sidebar-layouts">
                 <i class="icon-check menu-icon"></i>
                <span class="menu-title">Pay roll</span>
               
              </a>
              <div class="collapse" id="sidebar-layouts">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item"> <a class="nav-link" href="">Overview</a></li>
                  <li class="nav-item"> <a class="nav-link" href="">Payruns</a></li>
                  <li class="nav-item"> <a class="nav-link" href="">Others</a></li>
                </ul>
              </div>
            </li>
            <li class="nav-item">
              <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
              <i class="mdi mdi-settings"></i>
                <span class="menu-title">Settings</span>
              </a>
              <div class="collapse" id="ui-basic">
                <ul class="nav flex-column sub-menu">
                  <li class="nav-item"> <a class="nav-link" href="">General settings</a></li>
                  <li class="nav-item"> <a class="nav-link" href="">Payroll Settings</a></li>
                  <li class="nav-item"> <a class="nav-link" href="">Reports</a></li>
                </ul>
                </div>

            <li class="nav-item">
              <a class="nav-link" href="">
                <i class="icon-diamond menu-icon"></i>
                <span class="menu-title">Full screen</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="">
                <i class="icon-bell menu-icon"></i>
                <span class="menu-title">Logout</span>
              </a>
            </li>

          </ul>
        </nav>
        <!-- partial -->
        <div class="content-wrapper">

          <div class="row">          
        
                <div class="col-sm-9">
                
                    <div class="row">
              
                        <div class="col-lg-3 text-center">
                             <div class="profile-image">  <div class="chart easy-pie-chart-1" data-percent="100"> <span><img src="../../images/user.png" class="rounded-circle"> </span> </div> </div>
                        </div>
                      <div class="col-sm-6 emp-details">
                          <ul class="header-dropdown">
                      <li><i class="fa fa-edit" data-toggle="modal" data-target="#exampleModal-4" style="margin-top:6%;"></i></li>
                  </ul>
                        <h3 class="text-center" style=" margin-top: 20px; margin-bottom: 20px;">${emp.firstname}</h3>
   
                        <div class="row">
                           <div class="col-lg-4 p-0 details">
                             <h6 class="empdetails text-mute"><i class="icon-location-pin" style="margin-right:10px;"></i>Location</h6>
                             <span>Hyderabad</span>
                            </div> 
                            <div class="col-lg-4 p-0 details">
                              <h6 class="empdetails text-mute"><i class="icon-briefcase"  style="margin-right:10px;"></i>Employee Id</h6>
                              <span>${emp.empid}</span>
                            </div> 
                            <div class="col-lg-4 p-0 details">
                              <h6 class="empdetails text-mute"><i class="fa fa-mortar-board" style="margin-right:10px;"></i>Role</h6>
                              <span>${emp.role}</span>
                            </div> 
                        </div>
                      </div>
                    </div>

                <div class="empinfo">
                  <h4 class="headings">Employee Information</h4>
                  <div class="row">
                    <div class="col-lg-4">
                      <h6  class="text-mute">Joined On</h6>
                      <p>23, August, 2018</p>
                    
                    </div>

                      <div class="col-lg-4">
                      <h6  class="text-mute">HR Contact</h6>
                      <p>Langdon Brennan</p>
                    </div>

                   <div class="col-lg-4">
                      <h6  class="text-mute">Employee Type</h6>
                      <p>Full Time</p>
                      
                    </div>
                  </div>
                </div>
                  <div class="empinfo">

                  <h4 class="headings">Basic Information</h4>
                  <div class="row">
                    <div class="col-lg-3">
                      <h6  class="text-mute">First Name</h6>
                      <p>${emp.firstname}</p>
                      
                      
                    </div>

                     <div class="col-lg-3">
                      <h6  class="text-mute">Last Name</h6>
                      <p>${emp.lastname}</p>
                    
                      
                    </div>
                   <div class="col-lg-3">
                      <h6  class="text-mute">Gender</h6>
                      <p>${emp.gender}</p>                 
                      
                    </div>

                    <div class="col-lg-3">
                      <h6  class="text-mute">Date of Birth</h6>
                      <p>28 sep 1986</p> 
                    </div>

                    <div class="col-lg-3">
                      <h6  class="text-mute">Blood Group</h6>
                      <p>O +ve</p> 
                    </div>

                  </div>
                </div>


                  <div class="empinfo">

                  <h4 class="headings">Personal Information</h4>

                     
                  <div class="row">

                    <div class="col-lg-3">
                      <h6  class="text-mute">Email</h6>
                      <p>${emp.mail}</p>
                      
                      
                    </div>

                     <div class="col-lg-3">
                      <h6  class="text-mute">Phone</h6>
                      <p>${emp.contact}</p>
                    
                      
                    </div>
                   <div class="col-lg-3">
                      <h6  class="text-mute">Address</h6>
                      <p>Hyderabad</p>
                     
                      
                    </div>
                           <div class="col-lg-3">
                      <h6  class="text-mute">Country</h6>
                      <p>India</p>
                     
                      
                    </div>

                  </div>
                  <hr>
                       
       <div class="row">
                    <div class="col-lg-3">
                      <h6  class="text-mute">PAN Number</h6>
                      <p>DOIA75765</p>
                      
                      
                    </div>

                     <div class="col-lg-3">
                      <h6  class="text-mute">PF Number</h6>
                      <p>BLR/1921442/074/0954785</p>
                    
                      
                    </div>
                   <div class="col-lg-3">
                      <h6  class="text-mute">UAN Number</h6>
                      <p>125478245845.</p>
                     
                      
                    </div>
                           <div class="col-lg-3">
                      <h6  class="text-mute">ESI Nummber</h6>
                      <p>566688975765</p>
                     
                      
                    </div>
                  </div>
              <hr>
                     
       <div class="row">
                    <div class="col-lg-3">
                      <h6  class="text-mute">LinkedIn</h6>
                      <p>http://linkedin.com/vivek</p>
                      
                      
                    </div>

                     <div class="col-lg-3">
                      <h6  class="text-mute">Facebook</h6>
                      <p>http://facebook.com/vivek</p>
                    
                      
                    </div>
                   <div class="col-lg-3">
                      <h6  class="text-mute">Twitter</h6>
                      <p>http://twitter.com/vivek</p>
                     
                      
                    </div>
                           <div class="col-lg-3">
                      <h6  class="text-mute">Skype</h6>
                      <p>viveknadh</p>
                     
                      
                    </div>
                  </div>
                </div>

       <div class="empinfo">
                  <h4 class="headings">Payroll </h4>
                  <div class="row">
                    <div class="col-lg-3">
                      <h6  class="text-mute">Yearly CTC</h6>
                      <p>500000</p>                     
                      
                    </div>
                    <div class="col-lg-3">
                      <h6  class="text-mute">Payrun Type</h6>
                      <p>Monthly</p>                     
                      
                    </div>
                    <div class="col-lg-3">
                      <h6  class="text-mute">Pay Template</h6>
                      <p>Template Name</p>                     
                      
                    </div>
                       <div class="col-lg-3">
                      <h6  class="text-mute">Date of Releaving</h6>
                      <p>16 Dec 2018</p>                     
                      
                    </div>
                      <div class="col-lg-3">
                      <h6  class="text-mute">TDS</h6>
                      <p><a href="profileSettings.html">Details</p>                     
                      
                    </div>

                  </div>
                </div>

                  </div>
                   
             
      
                  <div class="col-sm-3">
                   <ul class="cnctdetials" style="margin-top: 35px;">
                    <li><i class="icon-envelope-letter" style="margin-right:10px;"></i><a href="mailto:nadh.vivek@gmail.com">${emp.mail}</a></li>
                       <li><i class="icon-phone" style="margin-right:10px;"></i><span class="text-mute">work:</span><span>&nbsp ${emp.contact}<span></li>
                      <li><i class="icon-phone" style="margin-right:10px;"></i><span class="text-mute">Bday:</span><span>&nbsp ${emp.startdate}<span></li>
                   </ul>
                         
             <div class="col-sm-3 emplist empinfo">
            <div class="card">
                <div class="card-body">
                  <h6 class="card-title"></h6>
                  <div class="w-75 mx-auto">
                    <div class="d-flex justify-content-between text-center">
                      <div class="wrapper">
                     
                        <small class="text-muted">Total Payroll</small>
                           <h4>$2256</h4>
                      </div>
                 
                    </div>
                    <div id="dashboard-donut-chart" style="height:250px"><svg height="250" version="1.1" width="265"  style="overflow: hidden; position: relative; left: -0.5px; top: -0.234375px;"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><path fill="none" stroke="#03a9f3" d="M132.4845,201.66666666666669A76.66666666666667,76.66666666666667,0,0,0,205.07857492337823,149.65518330486972" stroke-width="2" opacity="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); opacity: 0;"></path></svg></div>
                  </div>
               </div>
             </div></div>
              
     

                           <div class="body activities empinfo" style="margin-top:30px;">
                           
        <div class="card">
                <div class="card-body">
                  <small class="text-muted text-center">Organization chart</small>
<p class="help-description">
      <div class="text-center mt40">
        <div>
          <strong class="default-color"> Reports to </strong>
          <div class="text-center">
<a href="/employees/3000014574" id="ember3983" class="inline-block ember-view">
  <div id="ember3984" class="asize-64 pt15 ember-view"><div class="avatar-circle">
        <div id="ember3985" class="initialsAvatar avatarColor-1 ember-view">  N
</div>
</div>
</div>
</a>          </div>
          <div class="semi-bold text-muted">
            Naveen
          </div>
          <p class="text-muted">
           Project Manager
          </p>
        </div>
        <div class="profile-info-line"></div>
      <div class="vertical-align btn-special btn-rounded btn-sm default-color">
          <strong>Stuart Errol</strong>
      </div>
      <div class="profile-info-line"></div>
        <div class="semi-bold default-color"> Direct Reports </div>
        <div class="direct-reports">
              <div class="direct-avatar-wrap">
<a href="" id="ember4002" class="ember-view">
  <div id="ember4003" class="size-50 direct-avatar ember-view"><div class="avatar-circle">
        <div id="ember4004" class="initialsAvatar avatarColor-15 ember-view">  D
</div>

</div>
</div>
</a>                <div class="popover1 bottom">
                  <div class="popover-content">
                    <div class="link-color disabled semi-bold">Dreda Mikki</div>
                      <p class="text-muted">Manager &nbsp; | &nbsp; Client Relations</p>
                    <div>
                      <i class="icon-email"></i>&nbsp; dmikki@gmail.com <br>
                   </div>
                  </div>
                </div>
              </div>
       </div>
    </div>

</p>
               </div>
             </div>
                    </div>
                      </div>
                </div>
      
       
        </div>
        </div>
        </div><!--content-wrapper-->
                  <div class="modal fade" id="exampleModal-4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel-4" aria-hidden="true">
                    <div class="modal-dialog" role="document" id="modal-width" style="max-width: 80% !important;">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel-4">Edit Information</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                        <div>
                <div>
                  
                  <ul class="nav nav-tabs tab-basic" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true"> <i class="mdi mdi-account-outline"></i>Basic</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Personal</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Organization</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="contact-tab" data-toggle="tab" href="#payrun" role="tab" aria-controls="contact" aria-selected="false">Payroll</a>
                    </li>

                  </ul>
                    
                    <form action="emp2" method="action">
                  <div class="tab-content tab-content-basic">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                       <div class="empinfo" style="margin-top:0px !important">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="wfirstName2"> First Name :
                                                        <span class="danger">*</span>
                                                    </label>
                                                    <input type="text" class="form-control required" id="wfirstName2" name="firstname" value="${emp.firstname}" aria-required="true"> </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="wlastName2"> Last Name :
                                                        <span class="danger">*</span>
                                                    </label>
                                                    <input type="text" class="form-control required" id="wlastName2" name="lastname" value="${emp.lastname}" aria-required="true"> </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="wemailAddress2"> Date of Birth :
                                                        <span class="danger">*</span>
                                                    </label>
                                                    <input type="text" name="dob" class="form-control required" id = "datepicker-1" placeholder="dd/mm/yyyy"aria-required="true"> </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="wphoneNumber2">Gender :</label>
                                                    <select class="form-control" value="${emp.gender}">
                                                      <option>Male</option>
                                                      <option>Female</option>
                                                    </select>
                                            </div>
                                        </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="wlastName2"> Blood Group :
                                                        <span class="danger">*</span>
                                                    </label>
                                                    <input type="text" class="form-control required" id="wlastName2" name="bloodgroup" aria-required="true"> </div>
                                            </div>
                               
                    </div>
<!--      <button class="btn btn-info btn-sm mt-3 mb-2 cmm-btn">Save</button>-->
                  </div> </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <div class="row">
                           <div class="col-lg-4">
              <div class="empinfo" style="margin-top:0px !important">
                  <h4 class="headings">Communication</h4>
                
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="wfirstName2">Email :
                                                        <span class="danger">*</span>
                                                    </label>
                                                    <input type="text" class="form-control required" id="wfirstName2" name="mail" value="${emp.mail}" aria-required="true"> </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="wlastName2">Phone Number :
                                                        <span class="danger">*</span>
                                                    </label>
                                                    <input type="text" class="form-control required" id="wlastName2" name="contact" value="${emp.contact}" aria-required="true"> </div>
                                            </div>
                                          <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="wlastName2">Address :
                                                        <span class="danger">*</span>
                                                    </label>
                                                    <input type="text" class="form-control required" id="wlastName2" name="address" aria-required="true"> </div>
                                            </div>
                                  

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="wlastName2">Country:
                                                        <span class="danger">*</span>
                                                    </label>
                                                        <select class="form-control" name="country">
                                                      <option>India</option>
                                                      <option>Australia</option>
                                                      <option>USA</option>
                                                    </select> </div>
                                            </div>
                                    
                                      </div>
                           </div>

                          <div class="col-lg-4">
                               <div class="empinfo" style="margin-top:0px !important">
                  <h4 class="headings">Identification</h4>
                
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="wfirstName2">PAN Number :
                                                        <span class="danger">*</span>
                                                    </label>
                                                    <input type="text" name="pannumber" class="form-control required" id="wfirstName2"  aria-required="true"> </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="wlastName2">PF Number:
                                                        <span class="danger">*</span>
                                                    </label>
                                                    <input type="text"  class="form-control required" id="wlastName2" name="pfnumber" aria-required="true"> </div>
                                            </div>
                                  
                                                         <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="wlastName2">DL Number:
                                                        <span class="danger">*</span>
                                                    </label>
                                                    <input type="text" class="form-control required" id="wlastName2" name="dlnumber" aria-required="true"> </div>
                                            </div>                        

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="wlastName2">ESI Number:
                                                        <span class="danger">*</span>
                                                    </label>
                                                    <input type="text" class="form-control required" id="wlastName2" name="esinumber" aria-required="true"> </div>
                                            </div>
                                    
                                      </div>
                           </div>

                           <div class="col-lg-4">
   <div class="empinfo" style="margin-top:0px !important">
                  <h4 class="headings">Social</h4>
       
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="wfirstName2">Facebook :
                                                       
                                                    </label>
                                                    <div class="row">
                                                      <div class="col-lg-5 text-muted">http://facebook.com/</div>
                                                      <div class="col-lg-7"> <input type="text" class="form-control required" id="wlastName2" name="fbid" aria-required="true"> </div>
                                                    </div></div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="wlastName2">LinkedIn :
                                                     
                                                    </label>
                                                    <div class="row">
                                                      <div class="col-lg-5 text-muted">http://linkedin.com/</div>
                                                      <div class="col-lg-7"> <input type="text" class="form-control required" id="wlastName2" name="linkedinid" aria-required="true"> </div>
                                                    </div>
                                                   </div>
                                            </div>
                                  
                                                         <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="wlastName2">Twitter :
                                                     
                                                    </label>
                                                       <div class="row">
                                                      <div class="col-lg-5 text-muted">http://twitter.com/</div>
                                                      <div class="col-lg-7"> <input type="text" class="form-control required" id="wlastName2" name="twitterid" aria-required="true"> </div>
                                                    </div></div>
                                            </div>                        

                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="wlastName2">Skype :
                                                     
                                                    </label>
                                                    <input type="text" class="form-control required" id="wlastName2" name="skypeid" aria-required="true"> </div>
                                            </div>
                                    
                                      </div>
                           </div>
                         </div> 
<!--        <button class="btn btn-info btn-sm mt-3 mb-2 cmm-btn">Save</button>-->
                    </div>
                    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                              <div class="empinfo" style="margin-top:0px !important">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="wfirstName2"> Employee ID :
                                                        <span class="danger">*</span>
                                                    </label>
                                                    <input type="text" class="form-control required" id="wfirstName2" name="empid" value="${emp.empid}" aria-required="true"> </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="wlastName2"> Job Type:
                                                        <span class="danger">*</span>
                                                    </label>
                                                    <input type="text" class="form-control required" id="wlastName2" name="jobtype" aria-required="true"> </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="wfirstName2"> Job Title :
                                                        <span class="danger">*</span>
                                                    </label>
                                                    <input type="text" class="form-control required" id="wfirstName2" name="jobtitle" aria-required="true"> </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="wlastName2"> Team :
                                                        <span class="danger">*</span>
                                                    </label>
                                                    <input type="text" class="form-control required" id="wlastName2" name="team" aria-required="true"> </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="wemailAddress2"> Department :
                                                        <span class="danger">*</span>
                                                    </label>
                                                    <input type="text" class="form-control required" id="wemailAddress2" name="department" aria-required="true"> </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="wphoneNumber2">HR :</label>
                                                    <input type="text" name="hr" class="form-control" id="wphoneNumber2"> </div>
                                        </div>
                                             <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="wphoneNumber2">Start date :</label>
                                                    <input type="text" name="startdate" value="${emp.startdate}" class="form-control required" id = "datepicker-2" placeholder="dd/mm/yyyy"aria-required="true"> 
                                        </div>                                       
                               
                    </div> </div>
<!--     <button class="btn btn-info btn-sm mt-3 mb-2 cmm-btn">Save</button>-->
                    </div></div>
                    <div class="tab-pane fade" id="payrun" role="tabpanel" aria-labelledby="contact-tab">
                                              <div class="empinfo" style="margin-top:0px !important">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="wfirstName2">CTC :
                                                        <span class="danger">*</span>
                                                    </label>
                                                    <input type="text" class="form-control required" id="wfirstName2" name="ctc" value="${emp.ctc}" aria-required="true"> </div>
                                            </div>
                                                                  <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="wfirstName2"> Payrun Type :
                                                        <span class="danger">*</span>
                                                    </label>
                                                     <select class="form-control" name="payruntype">
                                                      <option>Monthly</option>
                                                      <option>Quarterly</option>
                                                      <option>Weekly</option>
                                                    </select>  </div>
                                            </div>
                                      
                                        </div>
                                                                                <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="wfirstName2">Payrun Template :
                                                        <span class="danger">*</span>
                                                    </label>
                                                    <input type="text" class="form-control required" id="wfirstName2" name="paytemplate" aria-required="true"> </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label for="wfirstName2">Date of Releaving :
                                                        <span class="danger">*</span>
                                                    </label>
                                                    <input type="text" class="form-control required" id="wfirstName2" name="relievingdate" aria-required="true"> </div>
                                            </div>

   
                                      
                                        </div>

                                      <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                             <button class="btn btn-info btn-sm mt-3 mb-2 cmm-btn"><a href="profileSettings.html">TDS</button></div>
                                            </div>

                                        </div>
                                                   
                          </div>
                        <button type="submit" class="btn btn-info btn-sm mt-3 mb-2 cmm-btn">Submit</button>

                    </div>
                  </div>
                </div>
              </div>
</form>
                        </div>
               
                      </div>
                    </div>

 <button type="submit" class="btn btn-info btn-sm mt-3 mb-2 cmm-btn">Submit</button>                  </div>
  </div>
   <!-- plugins:js -->

  <script src="../../node_modules/jquery/dist/jquery.min.js"></script>
  <script src="../../node_modules/popper.js/dist/umd/popper.min.js"></script>
  <script src="../../node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
  <script src="../../node_modules/perfect-scrollbar/dist/js/perfect-scrollbar.jquery.min.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <script src="../../node_modules/jquery-bar-rating/dist/jquery.barrating.min.js"></script>
  <script src="../../node_modules/chart.js/dist/Chart.min.js"></script>
  <script src="../../node_modules/raphael/raphael.min.js"></script>
  <script src="../../node_modules/morris.js/morris.min.js"></script>

  <script src="../../node_modules/jquery-sparkline/jquery.sparkline.min.js"></script>  
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="../../js/off-canvas.js"></script>
  <script src="../../js/hoverable-collapse.js"></script>
  <script src="../../js/misc.js"></script>
  <script src="../../js/settings.js"></script>
  <script src="../../js/todolist.js"></script>
  <script src="../../js/easypiechart.bundle.js"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="../../js/dashboard.js"></script>

  <script src="../../js/morris.js"></script>
  <script src="../../js/jquery-ui.js"></script>
  <!-- End custom js for this page-->

 <script>
$(function() {
    $( "#datepicker-1, #datepicker-2" ).datepicker();
});

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
</script>
</body>

</html>

